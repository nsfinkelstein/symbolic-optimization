from final import *
from psimplex import *
from itertools import chain, product
from scipy.optimize import linprog

import scipy.stats as st
import numpy as np
import dill as pickle
import sympy as sy
import z3


# def test_empty():
#     p = np.array([[z3.Real('p{}{}'.format(a, y)) for y in '01'] for a in '01'])
#     subspace = z3.simplify(z3.And(*{p.sum() == 1}, *{p >= 0 for p in chain(*p)}))
#     assert not empty(subspace)

#     condition = z3.Not(-1 * p[1, 0] + -1 * p[1, 1] <= 0)
#     assert not empty(condition)


#     assert not empty(z3.And(subspace, condition))


def test_rank():
    x = z3.Real('x')
    y = z3.Real('y')
    z = z3.Real('z')
    a = np.array([
        [x, 1],
        [y, 1],
        [z, 1],
    ])
    b = np.array([0, 1, 2])
    a, b = z3ify(a, b)
    t = rank_partition(a, b, 1, x + y + z <= 1, ['x', 'y', 'z'])
