from collections import namedtuple
from itertools import chain, product, combinations
from functools import reduce

import z3
import json
import copy
import sympy
import treelib
import numpy as np

simplify = sympy.simplify
simplify = z3.simplify

Node = namedtuple('Node', 'tableau subspace status')

def pivot(nonbasis, basis, a, b, c, nu, e, l):
    new_a, new_b, new_c = (np.ones_like(x, dtype=object) * z3.RealVal(0) for x in (a, b, c))

    # new bases
    new_basis = basis.copy()
    new_basis[e] = basis[l]
    new_basis.pop(l)

    new_nonbasis = nonbasis.copy()
    new_nonbasis[l] = nonbasis[e]
    new_nonbasis.pop(e)

    new_b[new_basis[e]] = b[basis[l]] / a[basis[l], nonbasis[e]]

    for j in set(nonbasis) - {e}:
        new_a[new_basis[e], new_nonbasis[j]] = a[basis[l], nonbasis[j]] / a[basis[l], nonbasis[e]]
    new_a[new_basis[e], new_nonbasis[l]] = 1 / a[basis[l], nonbasis[e]]

    for i in set(basis) - {l}:
        new_b[new_basis[i]] = b[basis[i]] - a[basis[i], nonbasis[e]] * new_b[new_basis[e]]
        for j in set(nonbasis) - {e}:
            new_a[new_basis[i], new_nonbasis[j]] = a[basis[i], nonbasis[j]] - a[basis[i], nonbasis[e]] * new_a[new_basis[e], new_nonbasis[j]]
        new_a[new_basis[i], new_nonbasis[l]] = -a[basis[i], nonbasis[e]] * new_a[new_basis[e], new_nonbasis[l]]

    new_nu = nu + c[nonbasis[e]] * new_b[new_basis[e]]
    for j in set(nonbasis) - {e}:
        new_c[new_nonbasis[j]] = c[nonbasis[j]] - c[nonbasis[e]] * new_a[new_basis[e], new_nonbasis[j]]
    new_c[new_nonbasis[l]] = -c[nonbasis[e]] * new_a[new_basis[e], new_nonbasis[l]]

    return (
        new_nonbasis,
        new_basis,
        new_a,
        np.array([simplify(i) if isinstance(i, z3.ExprRef) else i for i in new_b], dtype=object),
        np.array([simplify(i) if isinstance(i, z3.ExprRef) else i for i in new_c], dtype=object),
        simplify(new_nu) if isinstance(i, z3.ExprRef) else new_nu,
    )


def find_enterspace(c, nonbasis, e):
    return z3.And(
        c[nonbasis[e]] > 0, *{
        c[nonbasis[k]] <= 0
        for k in range(e) if k in nonbasis
    })


def find_leavespace(a, b, e, l, basis, nonbasis):
    return z3.And(*{
        (b[basis[l]] / a[basis[l], nonbasis[e]]) < (b[basis[k]] / a[basis[k], nonbasis[e]])
        for k in basis if k < l and empty(a[basis[k], nonbasis[e]] <= 0)
    }, *{
        (b[basis[l]] / a[basis[l], nonbasis[e]]) <= (b[basis[k]] / a[basis[k], nonbasis[e]])
        for k in basis if k > l and empty(a[basis[k], nonbasis[e]] <= 0)
    })


def parametric_simplex(tableau, subspace, root_name='root--'):
    """ each initial point is assumed to have its basic solution be feasible """
    tree = treelib.Tree()
    data = Node(tableau, subspace, 'incomplete')
    tree.create_node(root_name, root_name, data=data)

    while True:
        incomplete_leaves = list(n for n in tree.leaves() if n.data.status == 'incomplete')
        if not incomplete_leaves: break
        print('START LOOP...', [n.tag for n in incomplete_leaves])

        for leaf in incomplete_leaves:
            parent = leaf.identifier
            tableau, subspace, _ = tree[leaf.identifier].data

            nonbasis, basis, a, b, c, nu = tableau
            enterspaces = None

            for e in nonbasis:
                enterspace = find_enterspace(c, nonbasis, e)
                if empty(z3.And(enterspace, subspace)):
                    continue

                enterspaces = [enterspace] if enterspaces is None else enterspaces + [enterspace]
                infinite_ratios = 0
                for l in basis:
                    if empty(a[basis[l], nonbasis[e]] > 0):
                        infinite_ratios += 1
                        continue

                    leavespace = find_leavespace(a, b, e, l, basis, nonbasis)
                    new_subspace = z3.And(subspace, enterspace, leavespace)

                    if not empty(new_subspace):
                        print('\nadding node after pivot of', e, l, 'to parent', parent)
                        new_tableau = pivot(*tableau, e, l)
                        data = Node(copy.deepcopy(new_tableau), copy.deepcopy(new_subspace), 'incomplete')
                        name = parent + str(e) + ':' + str(l) + '--'
                        tree.create_node(name, name, parent, data)

                if infinite_ratios == len(basis):  # doesn't depend on params
                    data = Node(tableau, z3.And(enterspace, subspace), 'unbounded')
                    name = parent + str(e) + ':unbounded'
                    tree.create_node(name, name, parent, data)

            complete_space = subspace if enterspaces is None else z3.And(subspace, *{z3.Not(e) for e in enterspaces})
            if not empty(complete_space):
                data = Node(copy.deepcopy(tableau), copy.deepcopy(complete_space), 'complete')
                name = parent + 'complete'
                print(name, data.tableau[1])
                tree.create_node(name, name, parent, data)

        # test
        # cs = [c.data.subspace for c in tree.children(parent)]
        # childspaces = z3.Or(*cs)
        # assert empty(z3.And(subspace, z3.Not(childspaces)))
        # assert empty(z3.And(z3.Not(subspace), childspaces))
        # for c1, c2 in combinations(cs, r=2):
        #     assert empty(z3.And(c1, c2))

        print("END LOOP")
        tree.show()

    return tree


def init_psimplex(a, b, c, nu, subspace):
    """ represents opt cx st Ax <= b """

    # space where initial basic solution is feasible
    feasible_subspace = z3.And(subspace, *{bc >= 0 for bc in b})
    basis = {j: i for i, j in enumerate(range(a.shape[0]))}
    nonbasis = {i + a.shape[0]: i for i in range(c.size)}
    tableau = (nonbasis, basis, a, b, c, nu)
    init_feasible = Node(tableau, feasible_subspace, 'incomplete')

    mothertree = treelib.Tree()
    motherroot = Node(tableau, subspace, 'incomplete')
    mothertree.create_node('motherroot', 'motherroot', data=motherroot)

    if not empty(feasible_subspace):
        mothertree.create_node('init_feasible', 'init_feasible', 'motherroot', data=init_feasible)
        output = parametric_simplex(tableau, feasible_subspace, 'init_feasible')
        mothertree.merge('init_feasible', output)

    # create auxiliary PLP
    b_aux = b.copy()
    c_aux = np.zeros(c.size + 1, dtype=object)
    basis_aux = {j: i for i, j in enumerate(range(a.shape[0]))}
    nonbasis_aux = {i + a.shape[0]: i for i in range(c_aux.size)}
    x0_var = a.shape[0] + a.shape[1]
    c_aux[nonbasis_aux[x0_var]] = -1
    a_aux = np.hstack((a.copy(), -np.array([[z3.RealVal(1)]] * a.shape[0], dtype=object), ))
    tableau_aux = (nonbasis_aux, basis_aux, a_aux, b_aux, c_aux, nu)

    for k in basis_aux:
        kmin_subspace = simplify(z3.And(
            subspace,
            b_aux[basis_aux[k]] < 0,
            *{b_aux[basis_aux[k]] < b_aux[basis_aux[j]] for j in basis_aux if j < k},
            *{b_aux[basis_aux[k]] <= b_aux[basis_aux[j]] for j in basis_aux if j > k},
        ))

        if empty(kmin_subspace):
            print('Skipping', k, 'is smallest b')
            continue

        kmin_tableau = pivot(*tableau_aux, x0_var, k)
        kmin_node = Node(copy.deepcopy(kmin_tableau), copy.deepcopy(kmin_subspace), 'incomplete')
        kmin_name = 'kmin-{}-'.format(k)
        mothertree.create_node(kmin_name, kmin_name, 'motherroot', kmin_node)

        output = parametric_simplex(kmin_tableau, kmin_subspace, kmin_name)
        mothertree.merge(kmin_name, output)

        print('If', k, 'is smallest b', kmin_subspace)
        solutions = list(output.leaves())

        for solution in solutions:
            sub = solution.data.subspace
            values = basic_solution(*solution.data.tableau)
            feasible_space = values[x0_var] == z3.RealVal(0)

            infeasible_space = z3.And(sub, z3.Not(feasible_space))
            if not empty(infeasible_space):
                # add an infeasible node
                print("INFEASIBLE")
                n = Node(solution.data.tableau, infeasible_space, 'infeasible')
                nn = solution.identifier + 'infeasible'
                mothertree.create_node(nn, nn, solution.identifier, data=n)

            sol_subspace = z3.And(sub, feasible_space)
            if empty(sol_subspace):
                # test infeasible space is whole space
                # assert empty(z3.And(sub, z3.Not(infeasible_space)))
                # assert empty(z3.And(z3.Not(sub), infeasible_space))
                # nothing feasible here
                continue

            # test feasible and infeasible space are whole space
            # assert empty(z3.And(z3.Not(z3.Or(sol_subspace, infeasible_space)), sub))
            # assert empty(z3.And(z3.Or(sol_subspace, infeasible_space), z3.Not(sub)))

            nonbasis_sol, basis_sol, a_sol, b_sol, c_sol, nu_sol = solution.data.tableau

            if x0_var in basis_sol:
                e = min(e for e in nonbasis_sol if empty(a_sol[basis_sol[x0_var], nonbasis_sol[e]] == 0))
                nonbasis_sol, basis_sol, a_sol, b_sol, c_sol, nu_sol = pivot(*solution.data.tableau, e, x0_var)

            nonbasisa = nonbasis_sol.copy()
            nonbasisa.pop(x0_var)
            for i in nonbasisa:
                if nonbasisa[i] > nonbasis_sol[x0_var]:
                    nonbasisa[i] -= 1

            basisa = basis_sol.copy()
            sa = np.hstack((a_sol[:, :nonbasis_sol[x0_var]], a_sol[:, nonbasis_sol[x0_var] + 1:]))
            ba = b_sol

            nua = nu
            ca = np.zeros_like(c, dtype=object)
            for i in nonbasis:
                if i in nonbasisa:
                    # use same coefficients from original cost function
                    ca[nonbasisa[i]] += c[nonbasis[i]]
                elif i in basisa:
                    # substitute in equation for basis variable
                    nua += c[nonbasis[i]] * b[basisa[i]]
                    for j in nonbasisa:
                        ca[nonbasisa[j]] += c[nonbasis[i]] * (-sa[basisa[i], nonbasisa[j]])

            tableaua = (nonbasisa, basisa, sa, ba, ca, nua)
            tree = parametric_simplex(tableaua, sol_subspace, solution.identifier)
            mothertree.merge(solution.identifier, tree)

    mothertree.show()
    return mothertree


def empty(subspace):
    solver = z3.Solver()
    solver.add(subspace)
    return str(solver.check()) == 'unsat'


def print_tableau(nonbasis, basis, a, b, c, nu):
    print()
    print('nonbasis:', nonbasis)
    print('basis:', basis)
    print('a:', a)
    print('b:', b)
    print('c:', c)
    print('nu:', nu)
    print()


def basic_solution(nonbasis, basis, a, b, c, nu):
    return {**{var: b[basis[var]] for var in basis}, **{var: z3.RealVal(0) for var in nonbasis}}


def objective_value(values, c, nonbasis):
    objective = sum(c[nonbasis[v]] * values[v] for v in nonbasis)
    if isinstance(objective, z3.ArithRef):
        return simplify(objective)
    return objective


def find_node(tree, param_values):
    node = tree.root
    while node.children():

        if len(node.children()) == 1:
            node = node.children()[0]

        else:
            for child in node.children():
                if not empty(z3.And(param_values, child.data.subspace)):
                    node = child
                    break

    return node.data.tableau


def find_basic_solution(tree, param_values):
    node = tree.get_node(tree.root)
    # print(node.identifier, 'children', [d.identifier for d in (tree.children(node.identifier))])
    while len(list(tree.children(node.identifier))) > 0:
        # print(node.identifier, 'children', [d.identifier for d in (tree.children(node.identifier))])
        broke = False
        for child in (tree.children(node.identifier)):
            # print('Checking:', child.identifier)
            if not empty(z3.And(child.data.subspace, param_values)):
                node = child
                broke = True
                break

        if not broke:
            raise ValueError(str(param_values) + '\nnot in any child of\n' + node.identifier)

    # print('Leaf node:', node.identifier)
    # print(z3.simplify(node.data.subspace))
    if node.data.status in ('infeasible', 'unbounded'):
        return ['!!!', node.data.status]
    return basic_solution(*node.data.tableau)


def simplex(tableau):
    nonbasis, basis, a, b, c, nu = tableau
    cs, nus, nonbasiss = c.copy(), nu.copy(), nonbasis.copy()
    while (c > 0).any():
        # print(c)
        e = min(k for k in nonbasis if c[nonbasis[k]] > 0)
        nnv = [k for k in basis if a[basis[k], nonbasis[e]] > 0]

        if len(nnv) == 0:
            return 'unbounded'

        deltas = {k: b[basis[k]] / a[basis[k], nonbasis[e]] for k in nnv}
        smallest = min(deltas.values())
        l = min(k for k in nnv if deltas[k] == smallest)
        # print(nonbasis[e], basis[l])
        # print(e, l)
        nonbasis, basis, a, b, c, nu = pivot(nonbasis, basis, a, b, c, nu, e, l)

        opt = 0
        for i in basis:
            if i in nonbasiss:
                opt += -cs[nonbasiss[i]] * b[basis[i]]
        opt += nus
        # print(opt)

    # print(c)
    # for i in basis:
    #     print(i, b[basis[i]])

    opt = 0
    for i in basis:
        if i in nonbasiss:
            opt += -cs[nonbasiss[i]] * b[basis[i]]
    opt += nus
    return opt


def nodes_in_level(level, tree):
    return [n for n in tree.all_nodes() if tree.depth(n) == level]


def ancestors(nid, tree):
    an = set()
    while tree.parent(nid) is not None:
        parent = tree.parent(nid)
        an.add(parent)
        nid = parent.identifier
    return an


# manipulates in place
def order(basis, *arrays):
    # put everything in order
    rev_basis = {v: k for k, v in basis.items()}
    basis_vars = set(basis.keys())
    basis_idx = set(basis.values())

    for b in sorted(basis_vars):
        if basis[b] != min(basis_idx):
            origin = basis[b]
            dest = min(basis_idx)

            for a in arrays:
                tmp = a[dest]
                a[dest] = a[origin]
                a[origin] = tmp

            basis[rev_basis[min(basis_idx)]] = origin
            basis[b] = dest
            rev_basis = {v: k for k, v in basis.items()}

        basis_idx.remove(min(basis_idx))


def tab_hash(tableau):
    nonbasis, basis, a, b, c, nu = tableau
    order(basis, a, b)
    order(nonbasis, c)
    tabhash = json.dumps(basis, sort_keys=True)
    tabhash += json.dumps(nonbasis, sort_keys=True)
    tabhash += ''.join(str(z3.simplify(x).hash()) for x in
        chain(a.flatten(), b.flatten(), c.flatten(),
              [nu if isinstance(nu, z3.ArithRef) else z3.RealVal(nu)])
    )
    return tabhash


def pass_children(tree, level=None):
    level = [level] if level is not None else range(1, tree.depth() + 1)
    for l in level:
        print('level', l, list(n.identifier for n in nodes_in_level(l, tree)))
        while any(
            len(tree.children(n.identifier)) == 1
            for n in nodes_in_level(l, tree)
        ):
            for node in nodes_in_level(l, tree):
                nid = node.identifier
                if len(tree.children(nid)) == 1:
                    print('one child:', nid)
                    child = tree.children(nid)[0]
                    new_data = Node(
                        child.data.tableau,
                        node.data.subspace,  # must be same subspace
                        child.data.status
                    )
                    tree.update_node(child.identifier, data=new_data, tag=node.tag)
                    tree.link_past_node(nid)
    return tree


def dedup(tree):
    tableaus_seen = dict()
    for level in range(tree.depth() + 1):
        for node in nodes_in_level(level, tree):
            tabhash = tab_hash(node.data.tableau)

            if tabhash not in tableaus_seen:
                tableaus_seen[tabhash] = node
                continue

            dup = tableaus_seen[tabhash]

            if dup.data.status != node.data.status:
                continue

            dup = tableaus_seen[tabhash]
            node_an = ancestors(node.identifier, tree)
            dup_an = ancestors(dup.identifier, tree)

            print(node.identifier, 'node ancestors', [i.identifier for i in node_an])
            print(dup.identifier, 'dup ancestors', [i.identifier for i in dup_an])

            print('merging', node.identifier, 'and', dup.identifier)

            for ancestor in (node_an - dup_an) - {dup}:
                new_data = Node(
                    ancestor.data.tableau,
                    z3.And(ancestor.data.subspace, z3.Not(node.data.subspace)),
                    ancestor.data.status
                )
                tree.update_node(ancestor.identifier, data=new_data)

            for ancestor in dup_an - node_an:
                new_data = Node(
                    ancestor.data.tableau,
                    z3.Or(ancestor.data.subspace, node.data.subspace),
                    ancestor.data.status
                )
                tree.update_node(ancestor.identifier, data=new_data)

            new_data = Node(
                dup.data.tableau,
                z3.Or(dup.data.subspace, node.data.subspace),
                dup.data.status
            )
            tree.update_node(dup.identifier, data=new_data)

            for child in tree.children(node.identifier):
                tree.move_node(child.identifier, dup.identifier)

            parent = tree.parent(node.identifier)
            tree.remove_node(node.identifier)

            # is parent single-child after deletion?
            if len(tree.children(parent.identifier)) == 1:
                print('removing:',  parent.identifier)
                tree.link_past_node(parent.identifier)

    return tree

def compress_tree(tree):
    # drop impossible nodes
    for node in tree.all_nodes():
        if empty(node.data.subspace):
            tree.remove_node(node.identifier)

    # TODO: figure out correct order for this
    # remove parent subspaces from children to simplify SAT check and readability
    for level in range(tree.depth() + 1, 0, -1):
        for node in nodes_in_level(level, tree):
            parent = tree.parent(node.identifier)
            new_data = Node(
                node.data.tableau,
                super_simple(z3.And(*(
                    set(super_simple(node.data.subspace).children()) -
                    set(super_simple(parent.data.subspace).children())
                ))),
                node.data.status
            )
            tree.update_node(node.identifier, data=new_data)

    # rename for clarity
    for level in range(tree.depth() + 1):
        for i, node in enumerate(nodes_in_level(level, tree)):
            if node.data.status != 'complete':
                space = super_simple(node.data.subspace)
                name = ', '.join(str(c) for c in space.children())
            else:
                name = 'otherwise'
            tree.update_node(node.identifier, tag=name)

    # drop childree nodes
    tree = pass_children(tree)

    # # deduplicate repated tableaus
    # tree = dedup(tree)

    return tree


def union_equivalent_leaves(tree):
    leaves = tree.leaves()


def z3ify(*xs):
    res = []
    for x in xs:
        y = x.flatten()
        z = np.zeros_like(x, dtype=object).flatten()
        for i, a in enumerate(y):
            if not isinstance(y[i], z3.ArithRef):
                z[i] = z3.RealVal(y[i])
            else:
                z[i] = z3.simplify(y[i])
        res.append(z.reshape(x.shape))
    return res


def check_partitions(tree):
    for n in tree.all_nodes():
        assert not empty(n.data.subspace)

        cs = [c.data.subspace for c in tree.children(n.identifier)]

        if len(cs) == 0:
            continue

        childspaces = z3.Or(*cs)

        msg = {
            'node': n.identifier,
            'children': [c.identifier for c in tree.children(n.identifier)],
        }
        assert empty(z3.And(n.data.subspace, z3.Not(childspaces))), msg
        assert empty(z3.And(z3.Not(n.data.subspace), childspaces)), msg
        for c1, c2 in combinations(list(tree.children(n.identifier)), r=2):
            assert empty(z3.And(c1.data.subspace, c2.data.subspace)), c1.identifier + '  ' + c2.identifier


def super_simple(exp):
    exp = z3.simplify(exp)

    if not str(exp.decl()) == 'And':
        exp = z3.And(exp)

    conjuncts = set(exp.children())
    while True:
        conjunctions = {s for s in conjuncts if str(s.decl()) == 'And'}

        if not conjunctions:
            break

        for conjunction in conjunctions:
            conjuncts.remove(conjunction)
            conjuncts |= set(conjunction.children())

    for child in conjuncts.copy():
        if empty(z3.And(*(conjuncts - {child}), z3.Not(child))):
            conjuncts.remove(child)

    return z3.And(*conjuncts)
