import re
import z3
import time
import html
import random
import numpy as np
import sympy as sy
import scipy.stats as st

from final import full_algorithm, basic_solution, empty, Node, z3_to_sympy, get_original_tableau
from itertools import chain, combinations
from scipy.optimize import linprog


def super_simple(exp):
    exp = z3.simplify(exp)

    if not str(exp.decl()) == 'And':
        exp = z3.And(exp)

    conjuncts = set(exp.children())
    while True:
        conjunctions = {s for s in conjuncts if str(s.decl()) == 'And'}

        if not conjunctions:
            break

        for conjunction in conjunctions:
            conjuncts.remove(conjunction)
            conjuncts |= set(conjunction.children())

    for child in conjuncts.copy():
        if empty(z3.And(*(conjuncts - {child}), z3.Not(child))):
            conjuncts.remove(child)

    return z3.And(*conjuncts)


def simplify_tree(tree):
    # TODO: figure out correct order for this
    # remove parent subspaces from children to simplify SAT check and readability
    for level in range(tree.depth() + 1, 0, -1):
        for node in nodes_in_level(level, tree):
            parent = tree.parent(node.identifier)
            new_data = Node(
                node.data.tableau,
                super_simple(z3.And(*(
                    set(super_simple(node.data.subspace).children()) -
                    set(super_simple(parent.data.subspace).children())
                ))),
                node.data.status
            )
            tree.update_node(node.identifier, data=new_data)

    # rename for clarity
    z3.set_param(html_mode=True)
    for level in range(tree.depth() + 1):
        for i, node in enumerate(nodes_in_level(level, tree)):
            print(html.unescape(str(node.data.subspace)))
            space = super_simple(node.data.subspace)
            space = re.sub('\s+', ' ', str(space).replace('\n', ' '))

            pattern = '\d+/\d+'
            matches = re.findall(pattern, space)
            print(html.unescape(str( space )))
            for match in matches:
                if '/0' in match:
                    m = 'inf'
                else:
                    m = str(np.round(eval(match), 3))
                space = space.replace(match, m)
            name = html.unescape(str(space).replace('\n', ' '))
            tree.update_node(node.identifier, tag=name + '-' + node.data.status)
    z3.set_param(html_mode=False)

    # drop childree nodes
    tree = pass_children(tree)

    # # deduplicate repated tableaus
    # tree = dedup(tree)

    return tree


def nodes_in_level(level, tree):
    return [n for n in tree.all_nodes() if tree.depth(n) == level]


def pass_children(tree, level=None):
    level = [level] if level is not None else range(1, tree.depth() + 1)
    for l in level:
        print('level', l, list(n.identifier for n in nodes_in_level(l, tree)))
        while any(
            len(tree.children(n.identifier)) == 1
            for n in nodes_in_level(l, tree)
        ):
            for node in nodes_in_level(l, tree):
                nid = node.identifier
                if len(tree.children(nid)) == 1:
                    print('one child:', nid)
                    child = tree.children(nid)[0]
                    new_data = Node(
                        child.data.tableau,
                        node.data.subspace,  # must be same subspace
                        child.data.status
                    )
                    tree.update_node(child.identifier, data=new_data, tag=node.tag + '-' + child.data.status)
                    tree.link_past_node(nid)
    return tree


def check_partitions(tree):
    for n in tree.all_nodes():
        assert not empty(n.data.subspace)

        cs = [c.data.subspace for c in tree.children(n.identifier)]

        if len(cs) == 0:
            continue

        childspaces = z3.Or(*cs)

        msg = {
            'node': n.identifier,
            'children': [c.identifier for c in tree.children(n.identifier)],
        }
        assert empty(z3.And(n.data.subspace, z3.Not(childspaces))), msg
        assert empty(z3.And(z3.Not(n.data.subspace), childspaces)), msg
        for c1, c2 in combinations(list(tree.children(n.identifier)), r=2):
            assert empty(z3.And(c1.data.subspace, c2.data.subspace)), c1.identifier + '  ' + c2.identifier


def find_optimum(tree, param_values):
    node = tree.get_node(tree.root)
    while len(list(tree.children(node.identifier))) > 0:
        broke = False
        for child in (tree.children(node.identifier)):
            if not empty(z3.And(child.data.subspace, param_values)):
                node = child
                broke = True
                break

        if not broke:
            raise ValueError(str(param_values) + '\nnot in any child of\n' + node.identifier)

    if node.data.status != 'complete':
        return node.data.status

    solution = basic_solution(node.data.tableau)
    nonbasis, basis, a, b, c, nu = get_original_tableau(tree, node)
    # print(solution)
    # print(node.identifier)
    # print(nu, c)
    return sum(solution[x] * c[nonbasis[x]] for x in solution if x in nonbasis) + nu


def z3ify(*arrays):
    res = []
    for a in arrays:
        b = np.zeros_like(a, dtype=object).flatten()
        for i, x in enumerate(a.flatten()):
            if not isinstance(x, z3.ArithRef):
                b[i] = z3.RealVal(x)
            else:
                b[i] = x
        res.append(b.reshape(a.shape))
    return res


def z3_sub(pairs, *arrays):
    res = []
    for a in arrays:
        b = np.zeros_like(a, dtype=np.float64).flatten()
        for i, x in enumerate(a.flatten()):
            subbed = z3.substitute(x, *pairs)
            b[i] = np.float64(z3.simplify(subbed).as_decimal(10).replace('?', ''))
        res.append(b.reshape(a.shape))
    return res


def measurement_error():
    params = np.array([[z3.Real('p{}{}'.format(a, y)) for y in '01'] for a in '01'])
    subspace = z3.simplify(z3.And(*{params.sum() == 1}, *{p > 0 for p in chain(*params)}))

    a = -np.array([
        [-1,  0, -1, -1,  0, -1,  1,  0,  0, -1,  0, -1,  0],
        [ 0,  0,  0,  1,  0,  0,  1, -1,  0,  0,  0,  0,  0],
        [ 0, -1,  0, -1, -1,  0, -1,  0, -1,  0, -1,  0, -1],
    ])

    b = np.array([
        params[1, 0] / (params[1, 0] + params[1, 1]),
        (params[0, 0] / (params[0, 0] + params[0, 1]))
            - (params[1, 0] / (params[1, 0] + params[1, 1])),
        params[0, 1] / (params[0, 0] + params[0, 1])
    ])

    c = np.array([
        # q01, q03
        0, 0,
        # q10, q12, q13
        params[1, 0] + params[1, 1],
        params[1, 0] + params[1, 1],
        params[1, 0] + params[1, 1],
        # q20, 21, 22, 23
        params[0, 0] + params[0, 1],
        1,
        params[0, 0] + params[0, 1] - params[1, 0] - params[1, 1],
        params[0, 0] + params[0, 1],
        # q30, 31, 32, 33
        1, 1, 1, 1
    ])

    nu_param = np.array([
        ((params[0, 0] * (params[1, 0] + params[1, 1]))
             / (params[0, 0] + params[0, 1])) - params[1, 0]
    ])

    a, b, c = z3ify(a, b, c)
    start = time.time()
    tree = full_algorithm(a, b, c, subspace, {str(p) for p in params.flatten()})
    print(time.time() - start)
    # return tree
    # check_partitions(tree)
    # tree.show()
    # tree = simplify_tree(tree)
    # tree.show()
    # check_partitions(tree)

    for i in range(10000):
        params2 = np.round(st.dirichlet.rvs(np.random.random(size=4) * np.random.randint(1, 10, size=1)), 3)[0] * 1000
        params2[0] = 1000 - params2[1:].sum()
        params2 = params2.reshape((2, 2)).astype(np.int32)
        pairs =[(p1, z3.RatVal(int(p2), 1000))
                for p1, p2 in zip(params.flatten(), params2.flatten())]

        if np.any(params2 <= 0):
            continue
        print(pairs)

        anum, bnum, cnum, nu = z3_sub(pairs, a, b, c, nu_param)
        res = linprog(-cnum, A_ub=anum, b_ub=bnum, method='simplex', options={'bland': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values) + nu_param[0]
        optimum = z3.substitute(optimum, *pairs)
        optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
        print(optimum, -res.fun + nu[0])
        assert np.allclose(optimum, -res.fun + nu[0], atol=1e-4)


def random_linear_program():
    pass


def example3a_multiparametric():
    params = np.array([z3.Real('p{}'.format(i)) for i in '01'])
    subspace = z3.And(*{p >= z3.RealVal(-10) for p in params}, *{p <= z3.RealVal(10) for p in params})

    a = np.array([
        [params[0], 0.44],
        [0.05, params[1] * 0.10],
        [0.10 , 0.36]
    ])
    b = np.array([24000, 2000, 6000])
    c = np.array([8.10, 10.80])

    a, b, c = z3ify(a, b, c)
    start = time.time()
    known_nonsingular = True
    tree = full_algorithm(a, b, c, subspace, {str(p) for p in params.flatten()}, known_nonsingular)
    print(time.time() - start)
    # check_partitions(tree)
    tree.show()
    # tree = simplify_tree(tree)
    # tree.show()
    # check_partitions(tree)

    for i in range(10000):
        params2 = np.zeros(2)
        params2[0] = np.random.random(1) * 20 - 10
        params2[1] = np.random.random(1) * 20 - 10
        params2 = np.round(params2, 3)
        pairs = [(p1, z3.RealVal(p2))
                for p1, p2 in zip(params.flatten(), params2.flatten())]

        print(pairs)
        anum, bnum, cnum = z3_sub(pairs, a, b, c)
        res = linprog(-cnum, A_ub=anum, b_ub=bnum, method='simplex', options={'bland': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        if isinstance(optimum, str) and optimum == 'infeasible':
            print('infeasible', res.fun)
            assert res.status == 2
        elif isinstance(optimum, str) and optimum == 'unbounded':
            print('unbounded', res.fun)
            assert res.status == 3
        else:
            optimum = z3.substitute(optimum, *pairs)
            optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
            print(-optimum, res.fun)
            assert np.allclose(optimum, -res.fun, atol=1e-4)


def example3b_multiparametric():
    params = np.array([z3.Real('p{}'.format(i)) for i in range(7)])
    subspace = z3.And(
        z3.RealVal(9) <= params[0], params[0] <= z3.RealVal(12),
        z3.RealVal(10) <= params[1], params[1] <= z3.RealVal(13),
        z3.RealVal(0.1) <= params[2], params[2] <= z3.RealVal(0.3),
        z3.RealVal(0) <= params[3], params[3] <= z3.RealVal(3000),
        z3.RealVal(0.2) <= params[4], params[4] <= z3.RealVal(0.5),
        z3.RealVal(2000) <= params[5], params[5] <= z3.RealVal(6000),
        z3.RealVal(4000) <= params[6], params[6] <= z3.RealVal(8000),
    )

    a = np.array([
        [params[2], 0.44],
        [0.05, params[4] * 0.10],
        [0.10, 0.36]
    ])
    b = np.array([24000 + params[3], params[5], params[6]])
    c = np.array([params[0], params[1]])

    a, b, c = z3ify(a, b, c)
    start = time.time()
    tree = full_algorithm(a, b, c, subspace, {str(p) for p in params.flatten()})
    print(time.time() - start)
    check_partitions(tree)
    tree.show()
    # exit()
    # tree = pass_children(tree)
    check_partitions(tree)

    for i in range(10000):
        params2 = np.random.random(7)
        sizes = np.array([3, 3, 0.2, 3000, 0.3, 4000, 4000])
        low = np.array([9, 10, 0.1, 0, 0.2, 2000, 4000])
        params2 = params2 * sizes + low
        params2 = np.round(params2, 3)
        pairs = [(p1, z3.RealVal(p2))
                for p1, p2 in zip(params.flatten(), params2.flatten())]

        print(pairs)
        anum, bnum, cnum = z3_sub(pairs, a, b, c)
        res = linprog(-cnum, A_ub=anum, b_ub=bnum, method='simplex', options={'bland': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        optimum = z3.substitute(optimum, *pairs)
        optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
        print(optimum, res.fun)
        assert np.allclose(optimum, res.fun, atol=1e-4)


def example5_multiparametric():
    params = np.array([z3.Real('p{}'.format(i)) for i in range(3)])
    subspace = z3.And(*{p >= z3.RealVal(-5) for p in params}, *{p <= z3.RealVal(5) for p in params})

    a = np.array([
        [-1, 1],
        [1, -params[2]],
    ])
    b = np.array([params[1], 1])
    c = np.array([params[0], 1])

    a, b, c = z3ify(a, b, c)
    start = time.time()
    tree = full_algorithm(a, b, -c, subspace, {str(p) for p in params.flatten()})
    print(time.time() - start)
    check_partitions(tree)
    tree.show()
    # exit()
    tree = simplify_tree(tree)
    tree.show()
    # return tree

    for i in range(10000):
        params2 = np.random.random(3)
        sizes = np.array([10, 10, 10])
        low = np.array([-5, -5, -5])
        params2 = params2 * sizes + low
        params2 = np.round(params2, 3)
        pairs = [(p1, z3.RealVal(p2))
                for p1, p2 in zip(params.flatten(), params2.flatten())]

        print([(p1, p2.as_decimal(10)) for p1, p2 in pairs])
        anum, bnum, cnum = z3_sub(pairs, a, b, c)
        res = linprog(cnum, A_ub=anum, b_ub=bnum, method='simplex', options={'bland': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        if isinstance(optimum, str) and optimum == 'infeasible':
            print('infeasible')
            assert res.status == 2
        elif isinstance(optimum, str) and optimum == 'unbounded':
            print('unbounded')
            print(anum, bnum, cnum)
            assert res.status == 3
        else:
            optimum = z3.substitute(optimum, *pairs)
            optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
            print(-optimum, res.fun)
            assert np.allclose(-optimum, res.fun, atol=1e-4)


def example6_multiparametric():
    params = np.array([z3.Real('p{}'.format(i)) for i in range(3)])
    subspace = z3.And(
        z3.RealVal(0) <= params[0], params[0] <= z3.RealVal(6),
        z3.RealVal(0) <= params[1], params[1] <= z3.RealVal(1.5),
        z3.RealVal(-50000) <= params[2], params[2] <= z3.RealVal(50000),
    )

    ## put into standard form
    # solve for x5 and x6 using the equality constraints
    x5 = np.array([0.4, 0.06, 0.04, 0.05, -0.6, 0.06]) / 0.6
    x6 = np.array([0, 0.1, 0.01, 0.01, 0, -0.9]) / 0.9

    # x7 appears only in final constraint; can be treated as basic
    a = np.array([
        [1.1, 0.9, 0.9, params[1], 1.1, 0.9],
        [0.5, 0.35, 0.25, 0.25, 0.5, 0.35],
        [0.01, 0.15, 0.15, 0.18, 0.01, 0.15],
        [-6857.6, 364, 2032, -1145, -6857.6, 346]
    ])
    b = np.array([200000, 100000 + params[2], 20000, 20000000])
    c = np.array([2.84, -params[0], -3.33, 1.09, 9.39, 9.51])

    a, b, c, x5, x6 = z3ify(a, b, c, x5, x6)
    # substitute in solutions for x5 and x6 from above
    for i in range(len(c)):
        c[i] = c[i] + c[4] * x5[i]
    for i in range(len(c)):
        c[i] = c[i] + c[5] * x6[i]

    for i in range(len(a)):
        for j in range(len(c)):
            a[i, j] = a[i, j] + a[i, 4] * x5[j]
        for j in range(len(c)):
            a[i, j] = a[i, j] + a[i, 5] * x6[j]

    # now there are only 4 nonbasic variables
    a = np.delete(a, [4, 5], axis=1)
    c = np.delete(c, [4, 5], axis=0)

    start = time.time()
    parallel = False
    tree = full_algorithm(a, b, c, subspace, {str(p) for p in params.flatten()}, parallel=parallel)
    print(time.time() - start)
    tree.show()
    return tree
    # tree = simplify_tree(tree)
    # return tree
    #return tree

    for i in range(10):
        params2 = np.random.random(3)
        sizes = np.array([6, 1.5, 100000])
        low = np.array([0, 0, -50000])
        params2 = params2 * sizes + low
        params2 = np.round(params2, 3)
        params2 = np.array([0, 0.2, 10000])

        pairs = [(p1, z3.RealVal(p2))
                for p1, p2 in zip(params.flatten(), params2.flatten())]

        print([(p1, p2.as_decimal(10)) for p1, p2 in pairs])
        anum, bnum, cnum = z3_sub(pairs, a, b, c)
        res = linprog(-cnum, A_ub=anum, b_ub=bnum, method='revised simplex', options={'presolve': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        if isinstance(optimum, str) and optimum == 'infeasible':
            print('infeasible')
            assert res.status == 2
        elif isinstance(optimum, str) and optimum == 'unbounded':
            print('unbounded')
            assert res.status == 3
        else:
            optimum = z3.substitute(optimum, *pairs)
            optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
            print(-res.fun, optimum)
            assert np.allclose(optimum, -res.fun, atol=1e-4)


def proof_standard_slack_are_same():
    for i in range(1000):
        params = np.random.random(3)
        sizes = np.array([6, 1.5, 100000])
        low = np.array([0, 0, -50000])
        params = params * sizes + low
        params = np.round(params, 3)

        a = np.array([
            [1.1, 0.9, 0.9, params[1], 1.1, 0.9, 0],
            [0.5, 0.35, 0.25, 0.25, 0.5, 0.35, 0],
            [0.01, 0.15, 0.15, 0.18, 0.01, 0.15, 0],
        ])
        b = np.array([200000, 100000 + params[2], 20000])
        c = np.array([2.84, -params[0], -3.33, 1.09, 9.39, 9.51, 0])

        ae = np.array([
            [0.4, 0.06, 0.04, 0.05, -0.6, 0.06, 0],
            [0.0, 0.1, 0.01, 0.01, 0, -0.9, 0],
            [-6857.6, 364, 2032, -1145, -6857.6, 346, 21520]
        ])
        be = np.array([0, 0, 20000000])
        res1 = linprog(-c, a, b, ae, be, method='revised simplex', options={'presolve': True})


        # x7 appears only in final constraint; can be treated as basic
        a = np.array([
            [1.1, 0.9, 0.9, params[1], 1.1, 0.9],
            [0.5, 0.35, 0.25, 0.25, 0.5, 0.35],
            [0.01, 0.15, 0.15, 0.18, 0.01, 0.15],
            [-6857.6, 364, 2032, -1145, -6857.6, 346]
        ])
        b = np.array([200000, 100000 + params[2], 20000, 20000000])
        c = np.array([2.84, -params[0], -3.33, 1.09, 9.39, 9.51])

        ## put into standard form
        # solve for x5 and x6 using the equality constraints
        x5 = np.array([0.4, 0.06, 0.04, 0.05, -0.6, 0.06]) / 0.6
        x6 = np.array([0, 0.1, 0.01, 0.01, 0, -0.9]) / 0.9

        # substitute in solutions for x5 and x6 from above
        c += c[4] * x5
        c += c[5] * x6
        for i in range(len(a)):
            a[i] += a[i, 4] * x5
            a[i] += a[i, 5] * x6

        # now there are only 4 nonbasic variables
        a = np.delete(a, [4, 5], axis=1)
        c = np.delete(c, [4, 5], axis=0)

        res2 = linprog(-c, a, b, method='revised simplex', options={'presolve': True})
        print(res1.fun, res2.fun)
        assert np.allclose(res1.fun, res2.fun)


def big_lp():
    seed = 100
    np.random.seed(seed)
    random.seed(seed)

    x, y = z3.Real('x'), z3.Real('y')
    subspace = z3.And(x > 0, y > 0, x + y == 1)

    m = 10
    n = 10

    # set up linear program (a is somewhat sparse)
    a = np.round(st.uniform.rvs(-1, 2, size=(m, n)) * st.bernoulli.rvs(.5, size=(m, n)), 3)
    b = np.round(st.uniform.rvs(-1, 2, size=m) * st.bernoulli.rvs(.5, size=m), 3)
    c = np.round(st.uniform.rvs(-1, 2, size=n), 3)

    a, b, c = z3ify(a, b, c)
    c[0] = x
    b[0] = y

    start = time.time()
    parallel = False
    tree = full_algorithm(a, b, -c, subspace, {'x', 'y'}, parallel=parallel)
    print(time.time() - start)
    tree.show()

    for _ in range(1000):
        j = np.random.randint(1, 1000)
        k = 1000 - j

        pairs = [
            (x, z3.RatVal(int(j), 1000)),
            (y, z3.RatVal(int(k), 1000)),
        ]
        numa, numb, numc = z3_sub(pairs, a, b, c)
        res = linprog(numc, A_ub=numa, b_ub=numb, method='revised simplex', options={'presolve': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        if isinstance(optimum, str) and optimum == 'infeasible':
            print('infeasible')
            assert res.status == 2
        elif isinstance(optimum, str) and optimum == 'unbounded':
            print('unbounded')
            assert res.status == 3
        else:
            optimum = z3.substitute(optimum, *pairs)
            optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
            print(-res.fun, optimum)
            assert np.allclose(optimum, -res.fun, atol=1e-4)


def big_lp2():
    seed = 1
    np.random.seed(seed)
    random.seed(seed)

    x, y, z = z3.Real('x'), z3.Real('y'), z3.Real('z')
    subspace = z3.And(x > 0, y > 0, z > 0, x + y + z == 1)

    m = 10
    n = 10

    # set up linear program (a is somewhat sparse)
    a = np.round(st.uniform.rvs(-2, 4, size=(m, n)) * st.bernoulli.rvs(.5, size=(m, n)), 3)
    b = np.round(st.uniform.rvs(-2, 4, size=m), 3)
    c = np.round(st.uniform.rvs(-2, 4, size=n) * st.bernoulli.rvs(.5, size=n), 3)

    a, b, c = z3ify(a, b, c)
    c[1] = x
    b[1] = y
    a[0, 0] = z

    start = time.time()
    parallel = False
    tree = full_algorithm(a, b, -c, subspace, {'x', 'y'}, parallel=parallel)
    print(time.time() - start)
    tree.show()
    tree = simplify_tree(tree)
    tree.show()

    for _ in range(1000):
        j = np.random.randint(1, 998)
        k = np.random.randint(1, 999 - j)
        h = 1000 - j - k

        pairs = [
            (x, z3.RatVal(int(j), 1000)),
            (y, z3.RatVal(int(k), 1000)),
            (z, z3.RatVal(int(h), 1000)),
        ]
        numa, numb, numc = z3_sub(pairs, a, b, c)
        res = linprog(numc, A_ub=numa, b_ub=numb, method='revised simplex', options={'presolve': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        if isinstance(optimum, str) and optimum == 'infeasible':
            print('infeasible')
            assert res.status == 2
        elif isinstance(optimum, str) and optimum == 'unbounded':
            print('unbounded')
            assert res.status == 3
        else:
            optimum = z3.substitute(optimum, *pairs)
            optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
            print(-res.fun, optimum)
            assert np.allclose(optimum, -res.fun, atol=1e-4)


def big_lp3():
    seed = 1
    np.random.seed(seed)
    random.seed(seed)

    x, y, z = z3.Real('x'), z3.Real('y'), z3.Real('z')
    subspace = z3.And(x > 0, y > 0, z > 0, x + y + z == 1)

    m = 10
    n = 10

    # set up linear program (a is somewhat sparse)
    a = np.round(st.uniform.rvs(-1, 2, size=(m, n)) * st.bernoulli.rvs(.5, size=(m, n)), 3)
    b = np.round(st.uniform.rvs(-1, 2, size=m), 3)
    c = np.round(st.uniform.rvs(-1, 2, size=n), 3)

    a, b, c = z3ify(a, b, c)
    c[1] = x
    b[1] = y
    a[0, 0] = z

    start = time.time()
    parallel = False
    tree = full_algorithm(a, b, -c, subspace, {'x', 'y', 'z'}, parallel=parallel)
    print(time.time() - start)
    tree.show()
    return tree

    for _ in range(1000):
        j = np.random.randint(1, 998)
        k = np.random.randint(1, 999 - j)
        h = 1000 - j - k

        pairs = [
            (x, z3.RatVal(int(j), 1000)),
            (y, z3.RatVal(int(k), 1000)),
            (z, z3.RatVal(int(h), 1000)),
        ]
        numa, numb, numc = z3_sub(pairs, a, b, c)
        res = linprog(numc, A_ub=numa, b_ub=numb, method='revised simplex', options={'presolve': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        if isinstance(optimum, str) and optimum == 'infeasible':
            print('infeasible')
            assert res.status == 2
        elif isinstance(optimum, str) and optimum == 'unbounded':
            print('unbounded')
            assert res.status == 3
        else:
            optimum = z3.substitute(optimum, *pairs)
            optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
            print(-res.fun, optimum)
            assert np.allclose(optimum, -res.fun, atol=1e-4)


def iv_bounds():
    params = np.array([[[z3.Real('p{}{}{}'.format(y, x, a)) for a in '01'] for x in '01'] for y in '01'])

    subspace = z3.simplify(z3.And(
        *{params[:, :, i].sum() == 1 for i in (0, 1)},
        *{p > 0 for p in params.flatten()}
    ))


    b = -np.array([
        params[0, 1, 0] + params[1, 1, 0] - params[0, 0, 1],
        params[1, 0, 0] - params[0, 1, 1] - params[1, 0, 1],
        -params[0, 1, 0],
        -params[1, 1, 1],
        -params[1, 1, 0],
        -params[1, 0, 1],
        params[1, 0, 1] - params[1, 0, 0],
    ])

    a = -np.array([
        [ 1, -1,  1,  1,  1,  0,  0,  1,  1],
        [-1,  0,  0, -1, -1,  0,  1, -1,  0],
        [-1,  0,  0, -1, -1,  0,  0,  0,  0],
        [ 0,  0, -1,  0,  0,  0, -1,  0, -1],
        [ 0,  0, -1,  0,  0,  0,  0, -1, -1],
        [ 0,  0,  0, -1,  0, -1,  0, -1,  0],
        [ 0,  0,  0,  1,  0,  0, -1,  1,  0],
    ])

    c = np.array([0, 1, -1, -1, -1, 1, 0, -1, -2])

    nu_param = np.array([
        ((params[0, 0] * (params[1, 0] + params[1, 1]))
             / (params[0, 0] + params[0, 1])) - params[1, 0]
    ])

    a, b, c = z3ify(a, b, c)
    start = time.time()
    tree = full_algorithm(a, b, c, subspace, {str(p) for p in params.flatten()})
    print(time.time() - start)
    # return tree
    # check_partitions(tree)
    # tree.show()
    # tree = simplify_tree(tree)
    tree.show()
    # check_partitions(tree)

    for i in range(10000):
        params20 = np.round(st.dirichlet.rvs(np.random.random(size=4) * np.random.randint(1, 10, size=1)), 3)[0] * 1000
        params20[0] = 1000 - params20[1:].sum()
        params20 = params20.reshape((2, 2)).astype(np.int32)

        params21 = np.round(st.dirichlet.rvs(np.random.random(size=4) * np.random.randint(1, 10, size=1)), 3)[0] * 1000
        params21[0] = 1000 - params21[1:].sum()
        params21 = params21.reshape((2, 2)).astype(np.int32)

        params2 = np.stack((params20, params21), axis=2)
        pairs =[(p1, z3.RatVal(int(p2), 1000)) for p1, p2 in zip(params.flatten(), params2.flatten())]
        print(pairs)

        if np.any(params2 <= 0):
            continue

        anum, bnum, cnum, nu = z3_sub(pairs, a, b, c, nu_param)
        res = linprog(-cnum, A_ub=anum, b_ub=bnum, method='simplex', options={'bland': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        print(optimum)
        if isinstance(optimum, str) and optimum == 'infeasible':
            print('infeasible')
            assert res.status == 2
        elif isinstance(optimum, str) and optimum == 'unbounded':
            print('unbounded')
            assert res.status == 3
        else:
            optimum = z3.substitute(optimum, *pairs)
            optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
            print(-res.fun, optimum)
            assert np.allclose(optimum, -res.fun, atol=1e-4)


def measurement_error_equality():
    p = np.array([[z3.Real('p{}{}'.format(a, y)) for y in '01'] for a in '01'])
    subspace = z3.simplify(z3.And(*{p.sum() == 1}, *{x > 0 for x in chain(*p)}))

    # balke thesis page #90
    iva = np.array([
        [1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
        [1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1],
    ])

    a = np.zeros((4, 16), dtype=object)
    for i in range(4):
        a[i] = iva[i * 2] + iva[(i * 2) + 1]

    b = np.array([
        p[0, 0] / (p[0, 0] + p[0, 1]),  # P(Y = 0 | A = 0)
        p[0, 1] / (p[0, 0] + p[0, 1]),  # P(Y = 1 | A = 0)
        p[1, 0] / (p[1, 0] + p[1, 1]),  # P(Y = 0 | A = 1)
        p[1, 1] / (p[1, 0] + p[1, 1]),  # P(Y = 1 | A = 1)
    ])

    a, b = z3ify(a, b)

    # c = np.zeros(a.shape[1], dtype=object)
    # for i in range(len(c)):
    #     c[i] = ((p[0, 0] + p[0, 1]) * a[1, i]) + ((p[1, 0] + p[1, 1]) * a[3, i])

    c = np.zeros(a.shape[1], dtype=object)
    for i in range(len(c)):
        pa = (p[0, 0] + p[0, 1]) * (iva[1, i] + iva[3, i])
        po = (p[1, 0] + p[1, 1]) * (iva[5, i] + iva[7, i])
        c[i] = pa + po

    a, b, c = z3ify(a, b, c)
    # idx = np.arange(16)
    # np.random.shuffle(idx)
    # a = a[:, idx]
    # c = -c

    start = time.time()
    tree = full_algorithm(a[1:], b[1:], c, subspace, {str(p) for p in p.flatten()}, False)
    print(time.time() - start)
    # tree = simplify_tree(tree)
    tree.show()
    return tree

    for i in range(10000):
        params2 = np.round(st.dirichlet.rvs(np.random.random(size=4) * np.random.randint(1, 10, size=1)), 3)[0] * 1000
        params2[0] = 1000 - params2[1:].sum()
        params2 = params2.reshape((2, 2)).astype(np.int32)
        pairs = [(p1, z3.RatVal(int(p2), 1000))
                for p1, p2 in zip(p.flatten(), params2.flatten())]

        if np.any(params2 <= 0):
            continue
        print(pairs)

        anum, bnum, cnum = z3_sub(pairs, a, b, c)
        res = linprog(cnum, A_eq=anum[:3], b_eq=bnum[:3], method='simplex', options={'bland': True})
        # print(res.fun)
        res2 = linprog(-cnum, A_eq=anum[:3], b_eq=bnum[:3], method='simplex', options={'bland': True})
        # print(-res2.fun)
        # print(res.fun - res2.fun)
        # continue

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        optimum = find_optimum(tree, values)
        if isinstance(optimum, str):
            print(optimum, res.status, res.fun)
            assert optimum == 'unbounded' and res.status == 2 or optimum == 'infeasible' and res.status == 3
            continue
        optimum = z3.substitute(optimum, *pairs)
        optimum = float(z3.simplify(optimum).as_decimal(10).replace('?', ''))
        # print(res)
        print(optimum, -res2.fun)
        assert np.allclose(optimum, -res2.fun, atol=1e-4)

        p00 = p[0, 0]
        p01 = p[0, 1]
        p10 = p[1, 0]
        p11 = p[1, 1]
        explicit = (p01/(p00 + p01))*(-1*p00 + -1*p01 + -1*p10 + -1*p11) + (-1*p10 + -1*p11)*(p00/(p00 + p01) + -1*(p10/(p10 + p11)))
        pairs = [(p00, z3.RealVal(params2[0, 0])), (p01, z3.RealVal(params2[0, 1])), (p10, z3.RealVal(params2[1, 0])), (p11, z3.RealVal(params2[1, 1]))]
        e = z3_sub(pairs, np.array([explicit]))[0][0] / 1000
        # print(-e)
        # assert np.allclose(-e, res.fun)


def rank_partition():
    pass


def leaf_solutions(t, param_names):
    for node in t.leaves():
        print('\n')
        print(node.tag, node.identifier)

        if node.data.status == 'complete':
            solution = basic_solution(node.data.tableau)
            nonbasis, basis, a, b, c, nu = get_original_tableau(t, node)
            end = nu
            for x in nonbasis:
                end = z3.simplify(end + (solution[x] * c[nonbasis[x]]))
            print(sy.simplify(z3_to_sympy(np.array([end]), param_names)[0]))
            print(end.hash())
        else:
            print(node.data.status)


def simplify(exp, param_names):
    print(sy.simplify(z3_to_sympy(np.array([exp]), param_names)[0]))
