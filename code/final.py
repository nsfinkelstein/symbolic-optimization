from collections import namedtuple
from itertools import chain, product, combinations
from functools import reduce

import z3
import json
import copy
import sympy
import treelib
import sympy as sy
import numpy as np
import multiprocess as mp


Tableau = namedtuple('Tableau', 'nonbasis basis a b c nu', defaults=[0])
Node = namedtuple('Node', 'tableau subspace status')


def incomplete_leaves(tree, keyword):
    return {n for n in tree.leaves() if n.data.status == keyword}


def recover_from_auxiliary(tableau, original_c, original_nonbasis, x0_var):
    nonbasis_aux, basis_aux, a_aux, b_aux, c_aux, _ = tableau

    # remove aux var column
    a = np.delete(a_aux, nonbasis_aux[x0_var], axis=1)

    # remove from nonbasis set; adjust other var columns accordingly
    nonbasis = nonbasis_aux.copy()
    nonbasis.pop(x0_var)
    for i in nonbasis:
        if nonbasis[i] > nonbasis_aux[x0_var]:
            nonbasis[i] -= 1

    # recover objective function
    c = np.zeros_like(original_c, dtype=object)
    for i in original_nonbasis:
        if i in nonbasis:
            # use same coefficients from original cost function
            c[nonbasis[i]] += original_c[original_nonbasis[i]]
        elif i in basis_aux:
            # substitute in equation for basis variable
            for j in nonbasis:
                c[nonbasis[j]] += original_c[original_nonbasis[i]] * (-a[basis_aux[i], nonbasis[j]])

    return Tableau(nonbasis, basis_aux, a, b_aux, c)


def basic_solution(tableau):
    return {
        **{i: tableau.b[tableau.basis[i]] for i in tableau.basis},
        **{i: 0 for i in tableau.nonbasis},
    }


def child_spaces(tree, node):
    children = tree.children(node.identifier)
    return z3.Or(*{c.data.subspace for c in children})


def pivot(nonbasis, basis, a, b, c, _, e, l):
    new_a, new_b, new_c = (np.ones_like(x, dtype=object) * z3.RealVal(0) for x in (a, b, c))

    # new bases
    new_basis = basis.copy()
    new_basis[e] = basis[l]
    new_basis.pop(l)

    new_nonbasis = nonbasis.copy()
    new_nonbasis[l] = nonbasis[e]
    new_nonbasis.pop(e)

    new_b[new_basis[e]] = z3.simplify(b[basis[l]] / a[basis[l], nonbasis[e]])

    for j in set(nonbasis) - {e}:
        new_a[new_basis[e], new_nonbasis[j]] = z3.simplify(a[basis[l], nonbasis[j]] / a[basis[l], nonbasis[e]])
    new_a[new_basis[e], new_nonbasis[l]] = z3.simplify(1 / a[basis[l], nonbasis[e]])

    for i in set(basis) - {l}:
        new_b[new_basis[i]] = z3.simplify(b[basis[i]] - a[basis[i], nonbasis[e]] * new_b[new_basis[e]])
        for j in set(nonbasis) - {e}:
            new_a[new_basis[i], new_nonbasis[j]] = z3.simplify(a[basis[i], nonbasis[j]] - a[basis[i], nonbasis[e]] * new_a[new_basis[e], new_nonbasis[j]])
        new_a[new_basis[i], new_nonbasis[l]] = z3.simplify(-a[basis[i], nonbasis[e]] * new_a[new_basis[e], new_nonbasis[l]])

    for j in set(nonbasis) - {e}:
        new_c[new_nonbasis[j]] = z3.simplify(c[nonbasis[j]] - c[nonbasis[e]] * new_a[new_basis[e], new_nonbasis[j]])
    new_c[new_nonbasis[l]] = z3.simplify(-c[nonbasis[e]] * new_a[new_basis[e], new_nonbasis[l]])

    return Tableau(
        new_nonbasis,
        new_basis,
        new_a,
        np.array([i if isinstance(i, z3.ExprRef) else i for i in new_b], dtype=object),
        np.array([i if isinstance(i, z3.ExprRef) else i for i in new_c], dtype=object),
    )


def empty(subspace):
    solver = z3.Solver()
    solver.add(subspace)
    return str(solver.check()) == 'unsat'


def z3_to_sympy(z3array, param_names):
    params = {p: sy.symbols(p) for p in param_names}
    new = np.zeros_like(z3array, dtype=object).flatten()
    for i, item in enumerate(z3array.flatten()):
        new[i] = eval(str(item).replace('\n', ''), params)
    return new.reshape(z3array.shape)


def sympy_to_z3(solutions, param_names):
    params = {p: z3.Real(p) for p in param_names}
    new = np.zeros_like(solutions, dtype=object).flatten()
    for i, item in enumerate(solutions.flatten()):
        new[i] = eval(str(item), params)
    return z3ify(new)[0].reshape(solutions.shape)
    # return z3ify(np.array([eval(str(item), params) for item in solutions]))[0]


def solve(system, vs):
    system = set(system)
    backup = copy.deepcopy(system)
    solutions = []
    for v in vs:
        eq = next(eq for eq in system if v in eq.free_symbols)
        system.remove(eq)

        sol = sy.linsolve([eq], v)
        assert len(sol) == 1
        res = list(sol)[0][0]

        solutions = [s.subs(v, res) for s in solutions]
        system = {s.subs(v, res) for s in system}
        backup = {s.subs(v, res) for s in backup}

        solutions.append(res)

    # check there are solutions for all vs
    assert len(solutions) == len(vs)

    return solutions, backup


def make_basic(a, b, c, subspace, param_names):
    tree = treelib.Tree()
    data = Node(None, subspace, 'incomplete')
    tree.create_node('root', 'root', data=data)
    spaces = z3.RealVal(0) == z3.RealVal(1)
    m, n = a.shape
    params = {p: z3.Real(p) for p in param_names}
    for i in range(m, 0, -1):
        # for B in reversed(list(combinations(range(n), i))):
        for B in combinations(range(n), i):
            N = list(j for j in range(n) if j not in B)

            for R in combinations(range(m), i):
                NR = list(j for j in range(m) if j not in R)

                sub_a = np.delete(np.delete(a, NR, axis=0), N, axis=1)
                sub_a = sy.Matrix(z3_to_sympy(sub_a, param_names))
                det = sy.det(sub_a)
                z3_det = eval(str(det), params)

                space = z3.And(subspace, z3_det != z3.RealVal(0))
                if not empty(space):
                    inv = sub_a.inv()
                    inv = np.array(inv)
                    inv = sympy_to_z3(inv, param_names)

                    new_a = inv @ np.delete(np.delete(a, NR, axis=0), B, axis=1)
                    new_b = inv @ np.delete(b, NR)

                    new_c = np.zeros(len(N), dtype=object)
                    for j, k in enumerate(N):
                        new_c[j] = c[k]
                        for h, l in enumerate(B):
                            new_c[j] = new_c[j] + (c[l] * -new_a[h, j])

                    nu = z3.RealVal(0)
                    for h, l in enumerate(B):
                        nu = nu + (c[l] * new_b[h])

                    basis = {k: j for j, k in enumerate(B)}
                    nonbasis = {k: j for j, k in enumerate(N)}
                    tableau = Tableau(nonbasis, basis, *z3ify(new_a, new_b, new_c), z3.simplify(nu))
                    data = Node(tableau, space, 'incomplete')
                    name = 'basis-{}'.format(B)
                    tree.create_node(name, name, 'root', data)

                    spaces = z3.Or(spaces, space)

                    # early stopping condition
                    if empty(z3.And(subspace, z3.Not(spaces))):
                        return tree


def rank_partition(a, b, c, subspace, param_names, known_nonsingular=True):
    # z3 parameterized things, will convert to sympy and back...?
    tree = treelib.Tree()
    basis = {i: i for i in range(a.shape[0])}
    nonbasis = {i + a.shape[0]: i for i in range(a.shape[1])}
    tableau = Tableau(nonbasis, basis, a, b, c)
    data = Node(tableau, subspace, 'incomplete')
    tree.create_node('root', 'root', data=data)

    if known_nonsingular:
        return tree

    visited = set()
    while True:
        to_visit = set(tree.all_nodes()) - visited
        if len(to_visit) == 0:
            break
        visited |= to_visit
        for node in to_visit:
            nonbasis, basis, a, b, c, _ = node.data.tableau
            for i in reversed(range(a.shape[0])):

                if len(a) == 1:  # nothing left to drop
                    continue

                sympy_a = z3_to_sympy(a, param_names)
                string = ' '.join(
                    'v{}'.format(j) for j in range(a.shape[0]) if j != i
                )

                vs = sy.symbols(string)
                if len(a) == 2:  # symbols produces single symbol; must be iterable
                    vs = [vs]

                answers = np.copy(sympy_a[i])
                sympy_a = np.delete(sympy_a, i, axis=0)
                equations = [
                    sum(x * v for x, v in zip(sympy_a[:, j], vs)) - answers[j]
                    for j in range(sympy_a.shape[1])
                ]

                v_vals, equal_set = solve(equations, vs)
                v = sympy_to_z3(np.array(v_vals), param_names)
                equal_set = sympy_to_z3(np.array(equal_set), param_names)

                print('\n\n\n')
                print('row', i)
                print(v)
                print(equal_set)
                # the parameter space for which the solution for v holds
                holds = z3.And(*{e == z3.RealVal(0) for e in equal_set})

                cs = child_spaces(tree, node)
                # irrelevant = sum(q * t for q, t in zip(v, (r for k, r in enumerate(b) if k != i))) <= b[i]
                # space = z3.And(node.data.subspace, irrelevant, holds, z3.Not(cs))
                space = z3.And(node.data.subspace, v @ np.delete(b, i) <= b[i], holds, z3.Not(cs))
                if not empty(space):
                    new_basis = {
                        k: v if v < i else v - 1
                        for k, v in basis.items()
                        if v != i
                    }
                    tableau = Tableau(nonbasis, new_basis, np.delete(a, i, axis=0), np.delete(b, i), c)
                    data = Node(tableau, space, 'incomplete')
                    name = node.tag + '-drop-{}-'.format(i)
                    tree.create_node(name, name, node.identifier, data)

            cs = child_spaces(tree, node)
            space = z3.And(node.data.subspace, z3.Not(cs))
            if len(tree.children(node.identifier)) > 0 and not empty(space):
                data = Node(node.data.tableau, space, status='incomplete')
                name = node.tag + '-remainder-'
                tree.create_node(name, name, node.identifier, data)

    return tree


def initialization(tree, parallel=False, param_names=None):
    original_nodes = tree.leaves()
    x0_var = -1  # something unused and constant
    for node in original_nodes:
        nonbasis, basis, a, b, c, _ = node.data.tableau

        # set up auxiliary linear program
        b_aux = b.copy()
        basis_aux = basis.copy()
        nonbasis_aux = nonbasis.copy()
        c_aux = np.zeros(c.size + 1, dtype=object)
        nonbasis_aux[x0_var] = max(nonbasis.values()) + 1
        c_aux[nonbasis_aux[x0_var]] = -1
        a_aux = np.hstack((a.copy(), -np.array([[z3.RealVal(1)]] * a.shape[0], dtype=object), ))
        tableau_aux = Tableau(nonbasis_aux, basis_aux, a_aux, b_aux, c_aux)

        for k in basis_aux:
            kmin_subspace = z3.And(
                node.data.subspace,
                b[basis[k]] < z3.RealVal(0),
                *{b[basis[k]] < b[basis[j]] for j in basis if j < k},
                *{b[basis[k]] <= b[basis[j]] for j in basis if j > k},
            )

            if not empty(kmin_subspace):
                tableau = pivot(*tableau_aux, x0_var, k)
                name = node.identifier + '-b-min-{}'.format(k)
                data = Node(tableau, z3.simplify(kmin_subspace), 'aux')
                tree.create_node(name, name, node.identifier, data)

    # do this all at once to minimize bottleneck for parallel version
    tree = parametric_simplex(tree, 'aux')

    # convert back from auxiliary linear program
    for node in set(tree.leaves()) - set(original_nodes):
        solution = basic_solution(node.data.tableau)

        # address infeasible space
        infeasible_space = z3.And(node.data.subspace, solution[x0_var] > z3.RealVal(0))
        if not empty(infeasible_space):
            name = node.identifier + '-infeasible'
            data = Node(None, z3.simplify(infeasible_space), 'infeasible')
            tree.create_node(name, name, node.identifier, data)

        nonbasis, basis, a, b, c, _ = node.data.tableau

        og_tableau = get_original_tableau(tree, node)
        if x0_var in basis:
            spaces = infeasible_space
            for e in nonbasis:
                pivotable = a[basis[x0_var], nonbasis[e]] != z3.RealVal(0)
                space = z3.And(node.data.subspace, pivotable, z3.Not(spaces))
                if not empty(space):
                    tableau = pivot(*node.data.tableau, e, x0_var)
                    tableau = recover_from_auxiliary(tableau, og_tableau.c, og_tableau.nonbasis, x0_var)
                    data = Node(tableau, z3.simplify(space), 'incomplete')
                    name = node.identifier + '-{}:{}-recover-'.format(e, x0_var)
                    tree.create_node(name, name, node.identifier, data)
                    spaces = z3.simplify(z3.Or(spaces, space))
        else:
            tableau = recover_from_auxiliary(node.data.tableau, og_tableau.c, og_tableau.nonbasis, x0_var)
            name = node.identifier + '-recover-'
            data = Node(tableau, z3.simplify(z3.And(node.data.subspace, z3.Not(infeasible_space))), 'incomplete')
            tree.create_node(name, name, node.identifier, data)

    for node in original_nodes:
        feasible_subspace = z3.And(node.data.subspace, *{bc >= z3.RealVal(0) for bc in node.data.tableau.b})
        if not empty(feasible_subspace):
            name = node.identifier + '-init-feasible'
            data = Node(node.data.tableau, z3.simplify(feasible_subspace), 'incomplete')
            tree.create_node(name, name, node.identifier, data)

    return tree


def parametric_simplex(tree, keyword='incomplete'):
    while True:
        tree.show()
        incomplete = incomplete_leaves(tree, keyword)
        if len(incomplete) == 0:
            break

        for node in incomplete:
            done = False
            nonbasis, basis, a, b, c, _ = node.data.tableau

            complete_space = z3.And(node.data.subspace, *{c[nonbasis[e]] <= 0 for e in nonbasis})
            if not empty(complete_space):
                name = node.identifier + '-complete'
                data = Node(node.data.tableau, z3.simplify(complete_space), 'complete')
                tree.create_node(name, name, node.identifier, data)

                # early stopping check
                if empty(z3.And(node.data.subspace, z3.Not(complete_space))):
                         continue

            for e in nonbasis:
                if done: break
                enterspace = z3.And(
                    node.data.subspace,
                    c[nonbasis[e]] > z3.RealVal(0),
                    *{c[nonbasis[k]] <= z3.RealVal(0) for k in range(e) if k in nonbasis}
                )

                # early stopping check
                if empty(enterspace):
                    continue

                for l in basis:
                    leavespace = z3.And(
                        a[basis[l], nonbasis[e]] > z3.RealVal(0),
                        *{
                            z3.Or(
                                a[basis[k], nonbasis[e]] <= z3.RealVal(0),
                                (b[basis[l]] / a[basis[l], nonbasis[e]]) < (b[basis[k]] / a[basis[k], nonbasis[e]])
                            )
                            for k in basis if k < l
                        }, *{
                            z3.Or(
                                a[basis[k], nonbasis[e]] <= z3.RealVal(0),
                                (b[basis[l]] / a[basis[l], nonbasis[e]]) <= (b[basis[k]] / a[basis[k], nonbasis[e]])
                            )
                            for k in basis if k > l
                        }
                    )

                    space = z3.simplify(z3.And(enterspace, leavespace))
                    if not empty(space):
                        tableau = pivot(*node.data.tableau, e, l)
                        name = node.identifier + '-{}:{}-'.format(e, l)
                        data = Node(tableau, z3.simplify(space), keyword)
                        tree.create_node(name, name, node.identifier, data)

                        # early stopping check
                        if empty(z3.And(node.data.subspace, z3.Not(child_spaces(tree, node)))):
                            done = True
                            break

                unbounded_space = z3.And(
                    enterspace,
                    *{a[basis[k], nonbasis[e]] <= z3.RealVal(0) for k in basis}
                )
                if not empty(unbounded_space):
                    data = Node(None, z3.simplify(unbounded_space), 'unbounded')
                    name = node.identifier + '-{}:unbounded'.format(e)
                    tree.create_node(name, name, node.identifier, data)

                    # early stopping check
                    if empty(z3.And(node.data.subspace, z3.Not(child_spaces(tree, node)))):
                        break

    return tree


def full_algorithm(a, b, c, subspace, param_names, standard_form=True, parallel=False):

    if standard_form:
        tree = treelib.Tree()
        basis = {i: i for i in range(a.shape[0])}
        nonbasis = {i + a.shape[0]: i for i in range(a.shape[1])}
        tableau = Tableau(nonbasis, basis, a, b, c)
        data = Node(tableau, subspace, 'incomplete')
        tree.create_node('root', 'root', data=data)
    else:
        tree = make_basic(a, b, c, subspace, param_names)

    tree = initialization(tree, parallel, param_names)
    print({i.data.status for i in tree.leaves()})
    if parallel:
        return parallel_parametric_simplex(tree, param_names)
    return parametric_simplex(tree)


def parallel_parametric_simplex(tree, param_names):
    done = mp.Manager().Queue()

    incomplete = incomplete_leaves(tree)

    with mp.Pool(mp.cpu_count()) as pool:
        for node in incomplete:
            # parametric_simplex_iteration(serialize(node.data), node.identifier, done, param_names)
            pool.apply_async(parametric_simplex_iteration, args=[serialize(node.data), node.identifier, done, param_names])

        while len(incomplete_leaves(tree)) != 0:
            name, parent, data = get(done, param_names)
            tree.create_node(name, name, parent, data)
            if data.status == 'incomplete':
                node = tree.get_node(name)
                pool.apply_async(parametric_simplex_iteration, args=[serialize(node.data), node.identifier, done, param_names])

    return tree


def parametric_simplex_iteration(data, parent, done, param_names):
    data = deserialize(data, param_names)
    nonbasis, basis, a, b, c = data.tableau

    enterspaces = z3.RealVal(0) == z3.RealVal(1)
    for e in nonbasis:
        enterspace = z3.And(
            data.subspace,
            c[nonbasis[e]] > z3.RealVal(0),
            *{c[nonbasis[k]] <= z3.RealVal(0) for k in range(e) if k in nonbasis}
        )

        if empty(enterspace):
            continue
        enterspaces = z3.Or(enterspaces, enterspace)

        unbounded_space = z3.And(
            enterspace,
            *{a[basis[k], nonbasis[e]] <= z3.RealVal(0) for k in basis}
        )

        if not empty(unbounded_space):
            child_data = Node(None, unbounded_space, 'unbounded')
            name = parent + '-{}:unbounded'.format(e)
            put(done, name, parent, child_data)

        for l in basis:
            leavespace = z3.And(a[basis[l], nonbasis[e]] > z3.RealVal(0),
                *{
                    z3.Or(
                        a[basis[k], nonbasis[e]] <= z3.RealVal(0),
                        (b[basis[l]] / a[basis[l], nonbasis[e]]) < (b[basis[k]] / a[basis[k], nonbasis[e]])
                    )
                    for k in basis if k < l
                }, *{
                    z3.Or(
                        a[basis[k], nonbasis[e]] <= z3.RealVal(0),
                        (b[basis[l]] / a[basis[l], nonbasis[e]]) <= (b[basis[k]] / a[basis[k], nonbasis[e]])
                    )
                    for k in basis if k > l
                }
            )

            space = z3.And(enterspace, leavespace)
            if not empty(space):
                tableau = pivot(*data.tableau, e, l)
                name = parent + '-{}:{}-'.format(e, l)
                child_data = Node(tableau, space, 'incomplete')
                put(done, name, parent, child_data)

    complete_space = z3.And(data.subspace, z3.Not(enterspaces))
    if not empty(complete_space):
        name = parent + '-complete'
        child_data = Node(data.tableau, complete_space, 'complete')
        put(done, name, parent, child_data)


def z3serialize(expression):
    solver = z3.Solver()
    solver.add(expression == expression)
    return solver.sexpr()


def z3deserialize(expression):
    parsed_conditions = z3.parse_smt2_string(expression)
    return parsed_conditions[0].children()[0]


def serialize_arrays(*arrays):
    res = []
    for a in arrays:
        b = np.zeros_like(a, dtype=object).flatten()
        for i, x in enumerate(a.flatten()):
            b[i] = z3serialize(z3.simplify(x))
        b = b.reshape(a.shape)
        res.append(b)
    return res


def deserialize_arrays(param_names, *arrays):
    res = []
    for a in arrays:
        b = np.zeros_like(a, dtype=object).flatten()
        for i, x in enumerate(a.flatten()):
            b[i] = z3deserialize(x)
        b = b.reshape(a.shape)
        res.append(b)
    return z3ify(*res)


def serialize(data):
    nonbasis, basis, a, b, c = data.tableau
    a, b, c = serialize_arrays(a, b, c)
    tableau = Tableau(nonbasis, basis, a, b, c)
    return Node(tableau, z3serialize(z3.simplify(data.subspace)), data.status)


def deserialize(data, param_names):
    space = z3deserialize(data.subspace)
    nonbasis, basis, a, b, c = data.tableau
    a, b, c = deserialize_arrays(param_names, a, b, c)
    tableau = Tableau(nonbasis, basis, a, b, c)
    return Node(tableau, space, data.status)


def put(done, name, parent, data):
    data = serialize(data)
    done.put((name, parent, data))


def get(done, param_names):
    name, parent, data = done.get()
    data = deserialize(data, param_names)
    return name, parent, data


def z3ify(*arrays):
    res = []
    for a in arrays:
        b = np.zeros_like(a, dtype=object).flatten()
        for i, x in enumerate(a.flatten()):
            if not isinstance(x, z3.ArithRef):
                b[i] = z3.RealVal(x)
            else:
                b[i] = z3.simplify(x)
        res.append(b.reshape(a.shape))
    return res


def get_original_tableau(tree, node):
    path = iter(next(p for p in tree.paths_to_leaves() if node.identifier in p))
    node = next(path)
    og_tableau = tree.get_node(node).data.tableau
    while og_tableau is None:
        node = next(path)
        og_tableau = tree.get_node(node).data.tableau

    return og_tableau
