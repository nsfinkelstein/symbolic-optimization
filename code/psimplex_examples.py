from psimplex import *
from itertools import chain, product
from scipy.optimize import linprog

import scipy.stats as st
import dill as pickle
import numpy as np
import random
import sympy
import time
import z3


def balke_iv_formulation(params):
    b = -np.array([
        params[0, 1, 0] + params[1, 1, 0] - params[0, 0, 1],
        params[1, 0, 0] - params[0, 1, 1] - params[1, 0, 1],
        -params[0, 1, 0],
        -params[1, 1, 1],
        -params[1, 1, 0],
        -params[1, 0, 1],
        params[1, 0, 1] - params[1, 0, 0],
    ])

    a = -np.array([
        [ 1, -1,  1,  1,  1,  0,  0,  1,  1],
        [-1,  0,  0, -1, -1,  0,  1, -1,  0],
        [-1,  0,  0, -1, -1,  0,  0,  0,  0],
        [ 0,  0, -1,  0,  0,  0, -1,  0, -1],
        [ 0,  0, -1,  0,  0,  0,  0, -1, -1],
        [ 0,  0,  0, -1,  0, -1,  0, -1,  0],
        [ 0,  0,  0,  1,  0,  0, -1,  1,  0],
    ])

    c = np.array([0, 1, -1, -1, -1, 1, 0, -1, -2])
    nu = params[1, 1, 0] + params[1, 1, 1] - params[1, 0, 0]
    nonbasis = {i + 7: i for i in range(9)}
    basis = {i: i for i in range(7)}

    return nonbasis, basis, a, b, c, nu


def balke_iv_bounds(tree1=None, tree2=None):
    params = np.array([[[z3.Real('p{}{}.{}'.format(y, x, a)) for a in '01'] for x in '01'] for y in '01'])
    nonbasis, basis, a, b, c, nu = balke_iv_formulation(params)

    subspace = z3.simplify(z3.And(
        *{params[:, :, i].sum() == 1 for i in (0, 1)},
        *{p > 0 for p in params.flatten()}
    ))

    # # how to run for numeric example
    # subspace = z3.And(
    #         params[0, 0, 0] == 0.7,
    #         params[0, 1, 0] == 0.09,
    #         params[1, 0, 0] == 0.1,
    #         params[1, 1, 0] == 0.11,
    #         params[0, 0, 1] == 0.09,
    #         params[0, 1, 1] == 0.1,
    #         params[1, 0, 1] == 0.3,
    #         params[1, 1, 1] == 0.51,
    #     )

    # minus c for minimizing
    a, b, c = z3ify(a, b, c)
    if tree1 is None or tree2 is None:
        tree = init_psimplex(a, b, -c, nu, subspace)  # overflow error
        tree1 = copy.deepcopy(tree)
        tree2 = compress_tree(tree)
        return tree1, tree2

    c1 = c
    nu1 = nu


    # return tree1, tree2

    # m1 = print_leaf_values(tree1, c, nonbasis, nu)
    # m2 = print_leaf_values(tree2, c, nonbasis, nu)

    tree1.show()
    tree2.show()

    for i in range(5000):
        params20 = np.round(st.dirichlet.rvs(np.random.random(size=4) * np.random.randint(1, 10, size=1)), 3)[0] * 1000
        params20[0] = 1000 - params20[1:].sum()
        params20 = params20.reshape((2, 2)).astype(np.int32)

        params21 = np.round(st.dirichlet.rvs(np.random.random(size=4) * np.random.randint(1, 10, size=1)), 3)[0] * 1000
        params21[0] = 1000 - params21[1:].sum()
        params21 = params21.reshape((2, 2)).astype(np.int32)

        params2 = np.stack((params20, params21), axis=2)
        pairs =[(p1, z3.RatVal(int(p2), 1000)) for p1, p2 in zip(params.flatten(), params2.flatten())]
        print(pairs)

        if np.any(params2 <= 0):
            continue

        nonbasis, basis, a, b, c, nu = balke_iv_formulation(params2 / 1000)
        res = linprog(c, A_ub=a, b_ub=b)
        print(res)

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        basic1 = find_basic_solution(tree1, values)
        basic2 = find_basic_solution(tree2, values)


        if '!!!' in basic1:
            print('infeasible / unbounded')
            print('UNSUCCESSFUL')

            print(basic1[1])
            o1 = objective_value(basic1[1], c1, nonbasis)
            o1 = z3.substitute(o1, *pairs)
            oz1 = float(z3.simplify(o1).as_decimal(10).replace('?', ''))

            print(oz1, res.fun)
            assert res.status != 0
            assert basic1 == basic2

            continue

        o1 = objective_value(basic1, c1, nonbasis)
        print(basic1, basic2)
        print(z3.simplify(o1 + nu1))
        o1 = z3.substitute(o1, *pairs)
        oz1 = float(z3.simplify(o1).as_decimal(10).replace('?', ''))

        o2 = objective_value(basic2, c1, nonbasis)
        print(z3.simplify(o2 + nu1))
        o2 = z3.substitute(o2, *pairs)
        oz2 = float(z3.simplify(o2).as_decimal(10).replace('?', ''))

        assert oz1 == oz2, str(basic1) + '\n' +  str(basic2)

        print(res.fun, oz1)
        assert np.allclose(oz1, res.fun, atol=1e-4)



    # # example
    # param_values = z3.And(
    #     params[0, 0, 0] == 0.7,
    #     params[0, 1, 0] == 0.09,
    #     params[1, 0, 0] == 0.1,
    #     params[1, 1, 0] == 0.11,
    #     params[0, 0, 1] == 0.09,
    #     params[0, 1, 1] == 0.1,
    #     params[1, 0, 1] == 0.3,
    #     params[1, 1, 1] == 0.51,
    # )

    # b = find_basic_solution(tree, param_values)
    # objective = z3.simplify(objective_value(b, c, nonbasis) + nu)
    # print(objective)
    # pairs = [(pair.children()[0], pair.children()[1]) for pair in param_values.children()]
    # # print(z3.simplify(z3.substitute(nu, *pairs)))
    # print(z3.simplify(z3.substitute(objective, *pairs)))


def print_leaf_values(tree, c, nonbasis, nu):
    msg = {}
    for n in tree.all_nodes():

        if len(list(tree.children(n.identifier))) != 0:
            pass

        basic = basic_solution(*n.data.tableau)
        msg[n.identifier] = {
            'subspace': z3.simplify(n.data.subspace),
            'objective': z3.simplify(objective_value(basic, c, nonbasis) + nu),
        }

    res = {}
    for n, d in msg.items():
        sol = str(d['objective'])
        if sol in res:
            res[sol] = z3.simplify(z3.Or(res[sol], d['subspace']))
        else:
            res[sol] = d['subspace']

    return res


def test():
    from scipy.optimize import linprog
    params = np.zeros((2, 2, 2))
    params[0, 0, 0] = 0.7
    params[0, 1, 0] = 0.09
    params[1, 0, 0] = 0.1
    params[1, 1, 0] = 0.11
    params[0, 0, 1] = 0.09
    params[0, 1, 1] = 0.1
    params[1, 0, 1] = 0.3
    params[1, 1, 1] = 0.51

    b = -np.array([
        params[0, 1, 0] + params[1, 1, 0] - params[0, 0, 1],
        params[1, 0, 0] - params[0, 1, 1] - params[1, 0, 1],
        -params[0, 1, 0],
        -params[1, 1, 1],
        -params[1, 1, 0],
        -params[1, 0, 1],
        params[1, 0, 1] - params[1, 0, 0],
    ])

    a = -np.array([
        [ 1, -1,  1,  1,  1,  0,  0,  1,  1],
        [-1,  0,  0, -1, -1,  0,  1, -1,  0],
        [-1,  0,  0, -1, -1,  0,  0,  0,  0],
        [ 0,  0, -1,  0,  0,  0, -1,  0, -1],
        [ 0,  0, -1,  0,  0,  0,  0, -1, -1],
        [ 0,  0,  0, -1,  0, -1,  0, -1,  0],
        [ 0,  0,  0,  1,  0,  0, -1,  1,  0],
    ])

    c = np.array([0, 1, -1, -1, -1, 1, 0, -1, -2])

    nu = params[1, 1, 0] + params[1, 1, 1] - params[1, 0, 0]
    # print(nu)
    sol = linprog(
        np.concatenate((np.zeros(7), c)),
        A_eq=np.concatenate((np.eye(7), a), axis=1), b_eq=b,
        method='simplex', callback=lambda x: print(x.fun)
    )
    # print(sol.status)
    print(sol.fun + nu)
    print(sol.x)
    basis = {i: i for i in range(7)}
    nonbasis = {i + 7: i for i in range(9)}

    # minus c for minimizing, as-is for maximizing
    tableau = (nonbasis, basis, a, b, -c, nu)
    print('simplex', simplex(tableau))


def frontdoor_formulation(m, c):
    # a = np.array([
    #     [1, 1, 1, 1, 1, 1, 1, 1],
    #     [0, c[0], 1 - c[0], 1, 0, 0, 0, 0],
    #     [1, 1 - c[0], c[0], 0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 1, 1 - c[1], c[1], 0],
    #     # [0, 0, 0, 0, 0, c[1], 1 - c[1], 1],
    # ])
    # b = np.array([
    #     1,
    #     m[0, 1],
    #     m[0, 0],
    #     m[1, 0],
    #     # m[1, 1],
    # ])
    # c1 = np.array([
    #     0, c[0], 1 - c[0], 1,
    #     0, c[0], 1 - c[0], 1,
    # ])
    # c2 = np.array([
    #     0, c[1], 1 - c[1], 1,
    #     0, c[1], 1 - c[1], 1,
    # ])
    # c = -c2 + c1

    # inequality
    a = -np.array([
        # x1 x2 x5 x6
        [1, 1, 1, 1],
        [1 - c[0], c[0], 0, 0],
        [0, 0, 1 - c[1], c[1]],
    ])
    b = np.array([
        1,
        m[0],
        m[1],
    ])
    nu = ((1 - c[1]) - (1 - c[0])) * c[0]
    cost = np.array([
        ((1 - c[1]) - ((1 - c[0]) * c[0])),
        (c[1] - c[0] - (((1 - c[1]) - (1 - c[0])) * (1 - c[0]))),
        c[1] - c[0],
        (1 - c[1] - (c[0]))
    ])

    # x1 x2 x5 x6
    a = np.array([
        # [1, 1, 1, 1],
        [1 - c[0], c[0], 0, 0],
        [c[0], 1 - c[0], 0, 0],
        [0, 0, 1 - c[1], c[1]],
        [0, 0, c[1], 1 - c[1]],
        # [1, 0, 1, 0],
    ])
    b = np.array([
        # 1,
        m[0, 0],
        m[0, 1],
        m[1, 0],
        m[1, 1],
        # 0
    ])
    nu = 0
    cost = np.array([
        c[1] - c[0],
        (1 - c[1]) - (1 - c[0]),
        c[1] - c[0],
        (1 - c[1]) - (1 - c[0]),
    ])

    # x1 x2 x5 x6 monotonicity
    a = np.array([
        # [1, 1],
        [c[0], 0],
        [1 - c[0], 0],
        [0, c[1]],
        # [0, 1 - c[1]],
        # [1, 0, 1, 0],
    ])
    b = np.array([
        # 1,
        m[0, 0],
        m[0, 1],
        m[1, 0],
        # m[1, 1],
        # 0
    ])
    nu = 0
    cost = np.array([
        # c[1] - c[0],
        (1 - c[1]) - (1 - c[0]),
        # c[1] - c[0],
        (1 - c[1]) - (1 - c[0]),
    ])

    nonbasis = {i + a.shape[0]: i for i in range(a.shape[1])}
    basis = {i: i for i in range(a.shape[0])}
    return nonbasis, basis, a, b, cost, nu

def front_door():
    m = np.array([[z3.Real('p{}{}'.format(a, y)) for y in '01'] for a in '01'])
    c = np.array([z3.Real('p{}|{}'.format(0, a)) for a in '01'])
    # m = np.array([z3.Real('p{}{}'.format(a, 0)) for a in '01'])
    c = np.array([z3.Real('p{}|{}'.format(0, a)) for a in '01'])
    positivity = z3.And(
        z3.And(*{p > 0 for p in chain(*m)}),
        z3.And(*{p > 0 for p in c}),
    )
    normalize = z3.And(*{m.flatten()[:-1].sum() < 1, c[0] < 1, c[1] < 1})
    subspace = z3.simplify(z3.And(positivity, normalize))
    print(subspace)
    # stop
    nonbasis, basis, a, b, c, nu = frontdoor_formulation(m, c)
    a, b, c = z3ify(a, b, c)
    print(a, b, c)
    tree = init_psimplex(a, b, -c, nu, subspace)
    print(tree)
    # stop
    tree1 = copy.deepcopy(tree)
    # m1 = print_leaf_values(tree, c, nonbasis, nu)

    tree2 = compress_tree(tree)
    print('COMPRESSED')
    tree.show()
    print(print_leaf_values(tree, c, nonbasis, nu))
    # print(tree1)
    # print(tree2)
    return tree1, tree2
    # exit()


def measurement_error_formulation(params):
    a = -np.array([
        [-1,  0, -1, -1,  0, -1,  1,  0,  0, -1,  0, -1,  0],
        [ 0,  0,  0,  1,  0,  0,  1, -1,  0,  0,  0,  0,  0],
        # [ 0, -1,  0, -1, -1,  0, -1,  0, -1,  0, -1,  0, -1],
    ])

    b = np.array([
        params[1, 0] / (params[1, 0] + params[1, 1]),
        (params[0, 0] / (params[0, 0] + params[0, 1]))
            - (params[1, 0] / (params[1, 0] + params[1, 1])),
        # params[0, 1] / (params[0, 0] + params[0, 1])
    ])

    c = np.array([
        # q01, q03
        0, 0,
        # q10, q12, q13
        params[1, 0] + params[1, 1],
        params[1, 0] + params[1, 1],
        params[1, 0] + params[1, 1],
        # q20, 21, 22, 23
        params[0, 0] + params[0, 1],
        1,
        params[0, 0] + params[0, 1] - params[1, 0] - params[1, 1],
        params[0, 0] + params[0, 1],
        # q30, 31, 32, 33
        1, 1, 1, 1
    ])

    nu = ((params[0, 0] * (params[1, 0] + params[1, 1]))
             / (params[0, 0] + params[0, 1])) - params[1, 0]

    # track which variable is at which row
    basis = {i: i for i in range(a.shape[0])}
    nonbasis = {i + a.shape[0]: i for i in range(a.shape[1])}

    return nonbasis, basis, a, b, c, nu


def measurement_error(tree1=None, tree2=None):
    params = np.array([[z3.Real('p{}{}'.format(a, y)) for y in '01'] for a in '01'])
    subspace = z3.simplify(z3.And(*{params.sum() == 1}, *{p > 0 for p in chain(*params)}))

    nonbasis, basis, a, b, c, nu = measurement_error_formulation(params)
    nu1 = nu
    a, b, c = z3ify(a, b, c)
    print(a, b, c)
    if tree1 == None:
        tree = init_psimplex(a, b, -c, nu, subspace)
        tree1 = copy.deepcopy(tree)
        # m1 = print_leaf_values(tree, c, nonbasis, nu)

        tree2 = compress_tree(tree)
        print('COMPRESSED')
        tree.show()
        print(print_leaf_values(tree, c, nonbasis, nu))
        # print(tree1)
        # print(tree2)
        return tree1, tree2
        # exit()

    # m2 = print_leaf_values(tree1, c, nonbasis, nu)
    # assert m1 == m2, '\n' + str(list(m1.keys())) + '\n' + str(list(m2.keys()))

    for i in range(10000):
        params2 = np.round(st.dirichlet.rvs(np.random.random(size=4) * np.random.randint(1, 10, size=1)), 3)[0] * 1000
        params2[0] = 1000 - params2[1:].sum()
        params2 = params2.reshape((2, 2)).astype(np.int32)
        pairs =[(p1, z3.RatVal(int(p2), 1000))
                for p1, p2 in zip(params.flatten(), params2.flatten())]
        if np.any(params2 <= 0):
            continue
        print(pairs)

        nonbasis, basis, a, b, c, nu = measurement_error_formulation(params2 / 1000)
        res = linprog(c, A_ub=a, b_ub=b, method='simplex', options={'bland': True})

        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        basic1 = find_basic_solution(tree1, values)
        basic2 = find_basic_solution(tree2, values)
        assert basic1 == basic2, str(basic1) + str(basic2)
        basic = basic2
        o = objective_value(basic, c, nonbasis) + nu1
        o = z3.substitute(o, *pairs)
        oz = float(z3.simplify(o).as_decimal(10).replace('?', ''))
        print(res)
        print(res.fun + nu, oz)
        assert np.allclose(oz, res.fun + nu, atol=1e-4)


def large_program():
    pass

# def easier_iv_bounds():
#     params = np.array([[[z3.Real('p{}{}.{}'.format(y, x, a)) for a in '01'] for x in '01'] for y in '01'])
#     subspace = z3.simplify(z3.And(*set.union(
#         {params[:, :, 0].sum() == 1},
#         {params[:, :, 1].sum() == 1},
#         {p >= 0 for p in params.flatten()}
#     )))

#     b = np.array([
#         params[0, 0, 1],
#         params[0, 1, 0],
#         params[0, 1, 1] - params[0, 0, 0] + params[0, 0, 1],
#         params[1, 1, 1],
#         params[0, 0, 0] - params[0, 0, 1],
#         params[0, 0, 0] - params[0, 0, 1] + params[1, 0, 0] - params[0, 1, 1],
#         params[1, 1, 0] - params[1, 1, 1],
#     ])

#     a = -np.array([
#         [-1, -1, -1,  0,  0,  0,  0,  0,  0],
#         [ 0, -1,  0, -1, -1,  0,  0,  0,  0],
#         [ 0, -1, -1, -1, -1,  1,  0,  0,  0],
#         [ 0,  0,  0,  0,  0, -1, -1, -1,  0],
#         [ 0, -1, -1,  0,  0, -1,  0,  0,  0],
#         [ 0,  1,  1,  1,  1, -1, -1,  0, -1],
#         [ 0,  0, -1,  0,  0,  1,  1,  0,  0],
#     ])

#     c = np.array([ 1,  0,  1,  0, -1,  1,  0,  0,  0],)
#     c += a[3]
#     c -= a[5]
#     c -= a[2]
#     c -= a[1]
#     nu = b[3] - b[5] - b[2] - b[1]

#     basis = {i: i for i in range(7)}
#     nonbasis = {i + 7: i for i in range(9)}

#     tableau = (nonbasis, basis, a, b, c, nu)


def big_lp(tree=None):
    seed = 100
    np.random.seed(seed)
    random.seed(seed)

    m = 10
    n = 10

    # set up linear program (a is somewhat sparse)
    a = np.round(st.uniform.rvs(-1, 2, size=(m, n)) * st.bernoulli.rvs(.5, size=(m, n)), 3)
    b = np.round(st.uniform.rvs(-1, 2, size=m) * st.bernoulli.rvs(.5, size=m), 3)
    c = np.round(st.uniform.rvs(-1, 2, size=n), 3)
    c[0] = 1
    b[0] = 1
    a, b, c = a.astype(object), b.astype(object), c.astype(object)

    basis = {i: i for i in range(m)}
    nonbasis = {i + m: i for i in range(n)}

    def parameterize(b, c, x, y):
        newb, newc = np.copy(b), np.copy(c)

        newb[0] *= x
        # newb[1] *= x * y
        newc[0] *= y
        # newc[1] *= x * y

        return newb, newc

    # three params - one in both b and c, one only in b, and one only in c
    x, y = z3.Real('x'), z3.Real('y')
    subspace = z3.And(x > 0, y > 0, x + y == 1)
    paramb, paramc = parameterize(b, c, x, y)
    if tree is None:
        tree = init_psimplex(*z3ify(a, paramb, -paramc), 0, subspace)
        t1 = copy.deepcopy(tree)
        t2 = compress_tree(tree)
        t2.show()
        return t1, t2

    for _ in range(1000):
        j = np.random.randint(1, 1000)
        k = 1000 - j

        numb, numc = parameterize(b, c, j/1000, k/1000)
        start = time.time()
        res = linprog(numc, A_ub=a, b_ub=numb, method='interior-point', options={'bland': True})
        # res = simplex((nonbasis, basis, a, b, c, np.zeros(1)))
        print('linprog:', time.time() - start)
        # print(res)
        # continue

        pairs = {
            (x, z3.RatVal(int(j), 1000)),
            (y, z3.RatVal(int(k), 1000)),
        }
        # print(pairs)

        start = time.time()
        values = z3.And(*[p1 == p2 for p1, p2 in pairs])
        basic = find_basic_solution(tree, values)

        if '!!!' in basic:
            if basic[1] == 'infeasible':
                assert res.status == 2
            elif basic[1] == 'unbounded':
                assert res.status == 3
            print('parametric simplex:', time.time() - start)
            continue

        o = objective_value(basic, z3ify(paramc)[0], nonbasis)
        o = z3.substitute(o, *pairs)
        o = float(z3.simplify(o).as_decimal(10).replace('?', ''))
        print('parametric simplex:', time.time() - start)

        # print(o, res.fun)
        # assert np.allclose(o, res)
        assert np.allclose(o, res.fun)


if __name__ == "__main__":
    # test()
    balke_iv_bounds()
    # easier_iv_bounds()
    # measurement_error()
    # front_door()
    # big_lp()
    # pass
