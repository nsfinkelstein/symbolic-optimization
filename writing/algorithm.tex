\documentclass[main.tex]{subfiles}

\begin{document}

\section{The Parametric Simplex Algorithm}
\label{sec:algorithm}

In this section, we introduce the parametric simplex algorithm. Given a PLP,
denoted $\mathcal L_\phi$, and a setting of the parameters $\phi^*$, it is
possible to obtain a numerical LP by plugging $\phi^*$ into the parameterized
$A_\phi$, $b_\phi$ and $c_\phi$ of $\mathcal L_\phi$. We represent this
numerical LP as $\mathcal L_\phi(\phi^*)$

As mentioned above, the parametric simplex algorithm will construct a tree,
denoted $\mathcal T$, for a PLP input $\mathcal L_\phi$. Each node in the tree
is to be associated with three pieces of information. (i) A tableau,
representing the state of the problem corresponding to that node. (ii) A
subspace $\mathcal F'$ of the full parameter space $\mathcal F$. For a node a
depth $d$, $\mathcal F'$ represents the space of parameters for which the
tableau (i) would be achieved after $d$ iterations of the numerical simplex
algorithm applied to $\mathcal L_\phi(\phi^*)$ for all $\phi^* \in \mathcal F'$.
(iii) A status, indicating whether the tableau represents an ``incomplete'' or
``optimal'' solution for all $\phi^* \in \mathcal F'$, or representing that
$\mathcal L_\phi(\phi^*)$ is ``unbounded'' or ``infeasible'' for all $\phi^* \in
\mathcal F'$.

The idea behind constructing $\mathcal T$ is straightforward. As seen in \S
\ref{sec:prelim}, at each iteration of the simplex algorithm, the algorithm
must choose entering and leaving variables, or decide that the algorithm should
terminate. To build the subtree beginning at any leaf of $\mathcal T$ marked as
``incomplete'', we partition the space $\mathcal F'$ associated with that node
such that the simplex procedure would make the same decision for all $\phi^*$ in
each partition set, and not make the same decision for any two partition sets.
Then, for each partition set that is not empy, we ``enact'' that decision --
either by pivoting the tableau, or by marking the problem as complete -- and
create a child of the original node, with the updated tableau, status, and
parameter subspace. At the conclusion of the algorithm, the leaves of $\mathcal
T$ will partition $\mathcal F$, and their tableaus will encode parametric
solutions to $\mathcal L_\phi$ for their associated parameter subspaces, or
encode that $\mathcal L_\phi$ is infeasible or unbounded over those parameter
subspaces.

Parameter sets over which the optimal solution has the same parametric form have
been called \textit{critical regions} \cite{charitopoulos2017plp} in past works
on PLPs. The leaves of $\mathcal T$ contain all the information necessary to
trivally construct such critical regions. The critical region corresponding to
each parametric solution is simply the union of parameter subspaces of all leaf
nodes with that solution. A useful feature of the parametric simplex algorithm
is that the tree it constructs can be used post facto as a decision tree for
finding the parametric solution corresponding to a particular $\phi^*$. In many
cases, this will be more efficient than sequentially checking whether $\phi^*$
is in each of the critical regions.

Our algorithm can address any PLP in which each parameterized coefficient is a
rational function of $\phi$.

\begin{definition}[Rational Parametric Linear Programs]
  \label{def:rational-plp}
  A Rational Parametric Linear Program is a linear program in which any
  coefficient or constant in the objective function or constraints can be
  expressed as a rational function of some set of parameters $\phi$.
\end{definition}

This restriction is required by our use of the NLSat algorithm to evaluate
whether parameter spaces specified by polynomial constraints are empty. For the
same reason, we require that the parameter space under consideration, $\mathcal
F$, be expressable as a system of polynomial constraints. Unlike the majority of
existing methods, we do not place an restrictions on the size of $\phi$, the
number of coefficients that can be parameterized, or where in the program they
can occur. We also do not assume that any matrices are invertible everywhere in
the parameter space, unlike e.g. \cite{charitopoulos2017plp} (which also
implicitly requires rational PLPs). 

In the remainder of this section, we describe the parameteric simplex algorithm
in detail. Throughout, we assume that all PLPs considered are rational, and the
parameter spaces $\mathcal F$ can be expressed as polynomial constraints. In \S
\ref{sec:nonsingular} we begin by discussing how PLPs in non-standard form can
be encoded as tableaus, even if relevant matrices are singular for certain
parameter values in $\mathcal F$. In \S \ref{sec:init} we discuss the problem of
initializing a PLP in tableau form, and provide an algorithm for doing so. In \S
\ref{sec:parametric-simplex} we show how to solve a PLP, given an initial
feasible solution. In \S \ref{sec:full} we combine these pieces to yield an
algorithm for solving PLPs. Finally, in \S \ref{sec:parallel}, we discuss
features of the algorithm that make it amenable to parallelization, and discuss
one approach to parallelizing it.  Throughout, the algorithms can be expedited
through the inclusion of early stopping conditions to avoid unnecessary
iterations of various loops. Such conditions are omitted from the presentation
for clarity, but are included in the software implementation.

\subsection{PLPs in non-standard form}
\label{sec:nonsingular}

As discussed above, when an LP is in standard form, we can put it into slack
form, and the slack variables naturally become the basic variables. The same
holds for PLPs. We saw that when an LP includes equality constraints, it becomes
necessary to solve a system of linear equations, which entails inverting a
matrix, to put the LP into tableau form. Because the relevant matrix may be
singular for some parameter values, care must be taken to avoid associating a
subspace of the parameterspace $\mathcal F'$ with a tableau form that involves
inverting a matrix that is not invertible everywhere in $\mathcal F'$.

Suppose we have a PLP with equality and inequality constraints, which can be
expressed as

\begin{align}
  \label{eq:full-plp}
\max c_\phi^Tx \text{~~subject~to~~} A_\phi x \le b_\phi, A'_\phi x = b'_\phi ~~ x \ge 0,
\end{align}

\noindent and suppose the matrices $A_\phi$ and $A'_\phi$ have $m$ and $m'$ rows
respectively. Then using the inequality constraints, we can introduce $m$ slack
variables $s$ to the inequality constraints and make them basic, to obtain $s =
b_\phi - A_\phi x$. We can then use the equality constraints to make up to $m'$
additional variables in $x$ basic. In regions of $\mathcal F$ in which the
matrix is $A'_\phi$ is less than full row-rank, we will only be able to use the
equality constraints to solve for fewer than $m'$ of the variables.

Suppose $B$ represents the indices of a subset of $x$ that we would like to use
the equality constraints to solve for, and $A'_{\phi,B}$ represent the submatrix
obtained by collating only those columns. Similarly, let $N$ represent the
complement of $B$. Then we can rewrite the equality constraints as

\begin{align}
  A'_{\phi, B}x_B + A'_{\phi, N}x_N = b_\phi.
\end{align}

The variables $x_B$ can then be expressed simply as

\begin{align}
  x_B = A'^{-1}_{\phi, B}b_\phi - A'^{-1}_{\phi, B}A'_{\phi, N}x_N.
\end{align}

Different matrices $A'^{-1}_{\phi, B}$ may be invertible for different value of
$\phi^* \in \mathcal F$. Our goal, then, should be to associate a tableau form
corresponding to each basic set $B$ with a parameter subspace $\mathcal F'$ for
which the corresponding matrix $A'^{-1}_{\phi, B}$ is invertible. In addition,
we need to address the possibility that for some $\phi^* \in \mathcal F$, it is
possible that no such matrices will be invertible, i.e. that the matrix
$A'_{\phi}$ is not full row-rank. In this case, we must consider smaller basic
sets, and fewer rows. We denote the submatrix of $A'_\phi$ made up of the
elements in rows $R$ and columns $B$ by $A'_{\phi, R, B}$.

Once the variables $x_B$ are made basic, they must be substituted into the
inequality constraints and the objective function. Once that has been done,
the linear program is in tableau form, with the variables $s$ and $x_B$ serving
as basic variables. Algorithm \ref{alg:tabularize} describes a procedure for
converting any program of the form of Problem (\ref{eq:full-plp}) into tableau
form.

\begin{algorithm}[h!]
	\caption{Tabularize}
  \label{alg:tabularize}
  \include{tabularize}
\end{algorithm}

The algorithm procedes as follows. The for loop on line 3 determines the size of
the basic set under consideration. It is important to begin with the largest
possible number of variables that can be made basic using the equality
constraints, which is $m'$, the number of rows in $A'_\phi$. This is because at
each subsequent iteration, we exclude portions of the parameter space $\mathcal
F$ for which we have already found a basic set. In the loop beginning on line 4,
we iterate over each possible basic set of that size. In the loop beginning in
line 5, we iterate over each set of rows of the matrix $A'_\phi$ that has the
same size as the basis, i.e. for which it would be possible to obtain a square
matrix $A'_{\phi, R, B}$. If there are regions of $\mathcal F$ for which we must
consider a basic set of size less than $m'$, it follows that $A'_{\phi}$ has
less than full row-rank in those regions, and certain equality constraints are
redundant. By searching for sets of rows of size $R$ for which it is possible to
obtain a nonsingular square matrix $A'_{\phi, R, B}$, we effectively look to
drop redundant equality constraints.

In line 6, we define the parameter subspace for which the matrix under
consideration is, in fact, invertible, such that we can solve for the basic
variables as described above. Importantly, we subtract out the parameter
subspaces already associated with a tableau form. This ensures that each portion
of the parameter space is associated with a tableau form in which the maximum
number of variables are made basic, and therefore in which all the information
contained in the original constraints is also contained in the tableau form.

In line 7, we check whether the relevant space is empty. In lines 8 and 9, we
calculate the technology matrix and ``right hand side'' corresponding to the
basic set $x_B$. Note that this entails taking the inverse of a parameterized
matrix, which we do symbolically using the Gaussian elimination procedure. In
line 10, we substitute the nonbasic expression for each of the newly basic
variables into the objective function and the inequality constraints. In line
11, we specify that all the original variables, less the basic set, are
nonbasic. In line 12, we specify that the basic set for the full problem,
including the inequality constraints, is made up of the basic set under
consideration $B$, in addition to one slack variable for each of the original
inequality constraints, which are indexed after the original variables.

In line 13, we explicitly create the tableau form. The $A$ matrix is made up of
stacking the nonbasic columns from the $A_\phi$ matrix of the original
inequalities (with the basic expressions for $x_B$ substituted in) on top of the
newly calculated $\hat A_\phi$ matrix representing the basic form of $X_B$. The
$b_\phi$ vector is similarly constructed, and the coefficients corresponding
to $x_B$ -- which are all zero after the substitution on line 10 -- are dropped
from the cost vector $c_\phi$.

Finally, in lines 14 and 15, we expand the tree with a new node corresponding to
a tableau form that is equivalent to the original problem over its associated
subspace, and update the ``spaces'' variable, that keeps track of the portions
of $\mathcal F$ that already have tableaus.

We note that although this algorithm has been expressed in terms of constructing
a tree to be passed to later procedures, with very slight modification it can be
used as a pre-processing step for any algorithm that handles PLPs with
parameterized ``left hand sides''. To our knowledge, all such algorithms have
assumed that the $A'_\phi$ matrix is invertible for all parameter values $\phi$
in $\mathcal F$.

\begin{proposition}
  \label{prop:tabularize}
  The output of Algorithm \ref{alg:tabularize} is a two-level tree, in which the
  subspaces associated with the nodes on the second level partition $\mathcal
  F$, and the tableau associated with each node on the second level is
  equivalent to $\mathcal L_\phi(\phi^*)$ for any $\phi^*$ in its associated
  subspace.
\end{proposition}

By ``equivalent'', we mean that the same space of the original $x$ values if
feasible, and that the objective value is the same in the tableau form as in the
original form.

\subsection{Initializing the parametric simplex algorithm}
\label{sec:init}

As discussed in \S \ref{sec:prelim}, it is possible to obtain a tableau form
for which the basic solution is not feasible. In particular, if some elements of
the vector $b_\phi$ are negative for permitted parameter values, the initial
basic solution will assign some basic variables negative values, violating the
positivity constraints. The parameteric simplex algorithm, due to its grounding
on the simplex algorithm, assumes that the basic solution at each iteration
is feasible. It will therefore be necessary to ``initialize'' the algorithm,
i.e. to find tableaus that are equivalent to the original PLP in a parameter
subspace, such that each tableau has a basic feasible solution in that subspace.

In the parametric setting, this can be achieved by continuing to build out the
tree we introduced in the previous subsection. At a high level, we follow the
strategy for initializing the numerical simplex algorithm described in
\cite{clrs}, keeping track of the points at which the operations potentially
depend on the values of the parameters. This entails finding the parameter
subspaces which correspond to each possible auxiliary linear program -- and the
subspace for which no auxiliary program is needed -- solving those programs, and
then creating tableaus for the original program with the corresponding
solutions. This procedure is summarized in Algorithm \ref{alg:initialization}.

\begin{algorithm}[h!]
	\caption{Initialization}
  \label{alg:initialization}
  \include{initialization}
\end{algorithm}

In line 1, we enter a loop over all the leaves of the tree -- the output of
Algorithm \ref{alg:tabularize} -- in which each node corresponds to a parameter
subspace, and a tableau equivalent to the original PLP in that subspace. In
lines 2 - 4, we set up the the tableau for the auxiliary linear program that
will be used to find an initial feasible solution. The auxiliary variable is
given index $0$, and the appropriate changes are made to the technolgy matrix
and cost function, as described in \S \ref{sec:prelim}. Recall that in the
simplex algorithm, it was necessary to pivot the auxiliary variable into the
basis, with the basic variable corresponding to the smallest value of $b$
leaving the basis. In the parameteric setting, this variable can vary with the
parameters. In lines 5 - 10, we find the parameter spaces in which each leaving
variable would be chosen. In line 11, we use the parametric simplex algorithm --
to be described in \S \ref{sec:parametric-simplex} -- to solve each of these
auxiliary PLPs over their corresponding parameter spaces.

In lines 12 - 34, we convert back from the solution to the auxiliary PLP into a
tableau that again represents the original PLP. First, in lines 13 - 16, we
observe that wherever in the parameter space the auxiliary random variable is
not set equal to $0$ in the parametric solution to the auxiliary PLP, the
original PLP is infeasible. Next, in lines 17 - 22, if the auxiliary random
variable is not basic in the solution to the auxiliary PLP, it is
straightforward to restore the original PLP -- the portions of $A_\phi$ and
$c_\phi$ corresponding to the auxiliary variable are simply removed, and it is
taken out of the nonbasic set. If, by contrast, it is in the basis, it must be
pivoted out of the basis before these steps can be taken. However, it cannot be
pivoted with an entering variable that has a coefficient of $0$ in the row of
the technology matrix define the auxiliary variable. In lines 24 - 34, we ensure
that no such pivot is made. This entails further subdividing the parameter space
associated with the node that solves the auxiliary linear program into regions
wherein each pivot performed is known to be permitted.

Finally, in lines 35 - 37, we identify the parameter space in which the tableau
under consideration already has a basic feasible solution, such that no
auxiliary PLP is needed.

\begin{proposition}
  \label{prop:initialization}
  In the output of Algorithm \ref{alg:initialization}, the parameter subspace
  corresponding to each non-leaf node is partitioned by those corresponding to
  its children. Each leaf node either (i) contains a tableau that is equivalent
  to the original PLP and in which the basic solution is feasible in the
  subspace corresponding to that node, or (ii) contains the status
  ``infeasible'', in which case the original PLP is infeasible for any parameter
  in that node's parameter subspace.
\end{proposition}

\subsection{The Parametric Simplex Algorithm}
\label{sec:parametric-simplex}

\begin{algorithm}[h!]
	\caption{parametric simplex}
  \label{alg:parametric-simplex}
  \include{parametric-simplex}
\end{algorithm}

We now introduce a routine for continuing to construct the tree when each leaf
node can be assumed to have a tabluea in which the basic solution if feasible
for the parameter subspace corresponding to that node. As above, our strategy
will be to follow the simplex algorithm, branching out whenever the operations
prescribed by the algorithm depend on the values of the parameters. In this
implementation, we use Bland's pivot rule -- whenever multiple entering or
leaving variables are possible, we select the variable with the smallest index.
Although this rule has been shown to be slower than others in practice
\cite{avis1978blandslow}, it has been proven to prevent cycling
\cite{bland1977bland}. This property is especially important in PLPs, because if
the simplex algorithm were to cycle for the numerical optimization problem
corresponding to any parameter in the permitted space, the parametric-simplex
algorithm would not terminate. Nevertheless, the algorithm can also accomodate
other variable selection rules, which may be more efficient for certain PLPs.
The parameteric simplex Algorithm is provided in \ref{alg:parametric-simplex}.

Lines 1 - 3 specify that the algorithm continues to build out the tree so long
as any node does not represent a ``completed'' program, i.e. does not specify
that the problem the optimal solution, or that the problem is infeasible or
unbounded. For each nonbasic variable (line 6), we specify the parameter
subspace that would cause our pivot rule of choice to select it as the entering
variable in line 7. If the coefficients corresponding to that random variable in
the expression of every basic variable is nonpositive, then it is possible to
increase that variable arbitrarily, leading to an unbounded objective. Line 10
defines the parameter subspace in which this is the case. Otherwise, for each
basic variable (line 14), we define the parameter subspace for which our pivot
rule would select it, given the choice of entering variable. We then make the
relevant pivot (line 18), and add the new tableau to the tree, to be addressed
in the next iteration of the while loop. Finally, we specify that for parameters
for which the objective function decreases with an increase in any nonbasic
variable (line 21), the program is complete.

\begin{proposition}
  \label{prop:parametric-simplex}
  In the output of Algorithm \ref{alg:parametric-simplex}, the parameter subspace
  corresponding to each non-leaf node is partitioned by those corresponding to
  its children. Each leaf node either (i) contains a tableau corresponding to an
  optimal solution to the original PLP for any parameter in that node's
  parameter subspace, (ii) correctly characterizes the original PLP as unbounded
  or infeasible in its parameter subspace.
\end{proposition}

\subsection{Full algorithm}
\label{sec:full}

These algorithms can be compiled to solve any PLP of the form of
(\ref{eq:full-plp}), as shown in Algorithm \ref{alg:full-algorithm}. We again
emphasize that we need not make any of the nonsingularity assumptions that
restrict the set of PLPs that previous methods for providing parametric
solutions can address.

\begin{algorithm}[h!]
	\caption{full algorithm}
  \label{alg:full-algorithm}
  \include{full-algorithm}
\end{algorithm}

\begin{theorem}
  \label{thm:full-algorithm}
  For any rational PLP of the form given in Equation \ref{eq:full-plp},
  each leaf in the output of Algorithm \ref{alg:full-algorithm} either provides
  a correct parametric solution for the PLP for its parameter subspace, or
  correctly characterizes the PLP as unbounded or infeasible in its parameter
  subspace.
\end{theorem}


In some settings, we may be interested in partitioning the parameter space into
so-called critical regions, such that the parametric optimum of the PLP has the
same form within each critical region. In this case, for each parametric optimum
that appears as a solution to a tableau in a leaf node of the output, the
critical region for that solution is made up of the union of parameter spaces
corresponding to all the leaf nodes with that solution. Because the parameter
subspaces associated with the leaf node already form a partition of $\mathcal
F$, any such set of unions would form a partition as well.

Alternatively, we may be interested in the output as a decision tree that can be
used to obtain an answer to a particular problem more quickly than solving the
corresponding numerical linear program. In this case, making use of the
structure of the output is preferable to enumerating critical regions, as the
optimum can be obtained with less computation. We can compress the tree by
removing all nodes with exaclty one child, and linking the parent of each such
node directly to its child. No information is lost by this procedure because any
node with one child is associated with the same parameter space as its child. It
is also of course possible to optimize the decision tree, or in fact to
recompute a better decision tree from an enumeration of critical regions.
Depending on the size of the problem, constructing a better decision tree,
according to some criterion, may be quite slow. We have not investigated the
tradeoffs between constructing such a tree and directly making use of the output
of the parametric simplex algorithm as a decision tree.

\subsection{Parallelization}
\label{sec:parallel}

At each stage of the algorithm described above, all the information required to
continue construction of the tree starting at any of its leaf nodes is contained
within that leaf node. This means that at any stage of the algorithm, the work
of constructing the remainder of the tree may be easily parallelized.
This is in contrast to the algorithm developed in \cite{charitopoulos2017plp},
which is bottlenecked by using symbolic softward to obtain a solution to a large
system of equations.

\begin{algorithm}
  \caption{parallel parametric simplex}
  \label{alg:parallel-parametric-simplex}
  \include{parallel-parametric-simplex}
\end{algorithm}

In this section, we present one approach to parallelizing the algorithm. In
particular, we develop a drop-in parallelized replacement for the parameter
simplex algorithm. The parallelization strategy we adopt is to separate out the
task of constructing all the children of each node into a separate routine,
called a parametric simplex iteration. As new leaf nodes are formed, they are
placed onto a queue. The main process pulls nodes off the queue and adds them to
the tree. Any nodes that are incomplete are again submitted to the worker pool.
This process continues until the tree has been completed.

This is one of many possible strategies, presented here mostly for the purposes
of illustrating the ease with which thie algorithm can be parallelized. More
inovlved parallelization approaches may lead to better use of computational
resources, for example by parallelizing the tabularization and initialization
procedures or reducing the overhead of communicaton between processes. For
simplicity of exposition we do not explore such strategies here.

\end{document}