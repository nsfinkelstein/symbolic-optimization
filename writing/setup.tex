\documentclass[main.tex]{subfiles}

\begin{document}

\section{Preliminaries}
\label{sec:prelim}

\subsubsection*{The Simplex tableau algorithm}

In this section, we briefly review the Simplex algorithm. The simplex algorithm
operates on a tableau, constructed by selecting ``basic'' variables that are
defined in terms of the ``nonbasic'' variables. We let $\mathcal B$ and
$\mathcal N$ denote the set of indices of basic and nonbasic variables
respectively, and we let $z$ denote the value of the objective function. Then
all the information needed to solve the linear program can be expressed as

\begin{align}
  \label{eq:tableau}
  z &= \sum_{j \in \mathcal N} c_{j} x_j,\\
  \nonumber
  x_i &= b_{i} - \sum_{j \in \mathcal N} a_{ij} x_j ~~~~~\text{~for~} i \in {\mathcal B}.
\end{align}

The pieces of this representation -- $\mathcal N$, $\mathcal B$, $A$, $b$, $c$
-- are collectively known as a tableau. Assuming we are given a LP in standard
form, we can introduce slack variables $s$ to form an LP in which the feasible
region of the variables is described by a system of linear equations,

\begin{align}
  \label{eq:pplp}
  \text{optimize~~} c^Tx \text{~~subject to~~} A x + s = b \text{~~and~~} x \ge 0, s \ge 0.
\end{align}

In this system, it is easy to make the slack variables $s$ basic and the
original variables $x$ nonbasic, as $s = b - Ax$. If the original problem has
equality rather than or in addition to inequality constraints, we must solve for
a subset of the original variables to make them basic. This entails inverting a
submatrix of $A_\phi$. As we describe in \S \ref{sec:nonsingular}, when the
technology matrix is parameterized, we must take care to keep track of the
parameter values for which the relevant submatrices are singular.

The centeral operation permitted by the tableau form is the Pivot operation, in
which one nonbasic variable enters the basis, and one basic variable leaves the
basis. Conceptually, this corresponds to solving for the entering variable in
the equation for the leaving variable, and substituting the result everywhere
else the entering variable appears in the system. The details of the Pivot
operation are provided in Algorithm \ref{alg:pivot}. In the context of the
Simplex algorithm, a pivot corresponds to ``moving'' to an adjacent vertex.

\begin{algorithm}[h!]
	\caption{Pivot}
  \label{alg:pivot}
  \include{pivot}
\end{algorithm}

Once the PLP is in tableau form, as in (\ref{eq:tableau}), a ``basic solution''
is one in which all nonbasic variables are set to $0$, and the basic variable
$x_i$ takes the value $b_i$. In some cases, some $b_i$ will be negative, and the
basic solution will be infeasible, due to the constraint $x \ge 0$. In such
cases it will be necessary to find an initial feasible solution, which can be
done by forming an auxiliary linear program as described in \cite{clrs}. The key
idea is to introduce an auxiliary random variable $x_0$, and amend the tableau as
follows:

\begin{align*}
  \label{eq:tableau}
  z &= - x_0\\
  x_i &= b_{i} - \sum_{j \in \mathcal N} a_{ij} x_j - x_0 ~~~~~\text{~for~} i \in {\mathcal B}.
\end{align*}

If $x_0$ is pivoted into the basis and the variable corresponding to the
smallest value of $b_i$ is pivoted out, the resulting basic solution will be
feasible for the auxiliary LP, and the simplex algorithm can then be run on the
auxiliary LP. If the objective can be equal to $0$, then all the constraints in
the original LP must be satisfied at the optimal solution to the auxiliary LP,
in which case it represents a feasible solution for the original LP. Otherwise,
it must be impossible to satisfy all the original constraints, and the original
LP is infeasible.

Once we have obtained a feasible solution for the original LP, we can procede
with the core of the simplex algorithm, which is reproduced in Algorithm
\ref{alg:simplex}. At each iteration of the simplex algorithm, it selects an
entering and leaving variable according to a user-specified rule. Any pivot with
an entering variable $x_j$ corresponding to a positive value of $c_j$ is
guaranteed to weakly increase the objective by adding non-negative values to it.
Once an entering variable is selected, the choice of leaving variables is
restricted to those for which the basic solution after the pivot remains
feasible, i.e. all values of $b$ remain positive. For a more detailed treatment
of this algorithm, including a proof of correctness, see Ch 29 in \cite{clrs}.
We make use of Bland's rule for selecting the entering and leaving variables in
Algorithm \ref{alg:simplex}. Bland's rule is discussed in more detail in \S
\ref{sec:parametric-simplex}.

\begin{algorithm}[h!]
	\caption{Simplex}
  \label{alg:simplex}
  \include{simplex}
\end{algorithm}

\subsubsection*{Nonlinear system satisfiability}

Suppose $g(\phi)$ and $f(\phi)$ are polynomials in $\phi$. Constraints of the
form $g(\phi) = f(\phi)$, $g(\phi) \le f(\phi)$ and $g(\phi) < f(\phi)$ are
called \textit{polynomial constraints}. As we will see in \S
\ref{sec:algorithm}, we will often need to evaluate whether there are any values
of $\phi$ that satisfy a system of polynomial constraints. This question can be
answered exactly using any number of algorithms from the field of algebraic
geometry. We make use of the NLSat algorithm introduced in
\cite{javanovic2012nlsat}, and implemented in the z3 software package
\cite{demoura2007z3}.

\subsubsection*{Parametric matrix inversion}

When presented with a LP that includes equality constraints, we will need to
calculate inverses of parameterized matrices to put it into tableau form. To do
so, we use the Gaussian elimination method, as implemented in the sympy software
package \cite{meurer2017sympy}.

\end{document}
