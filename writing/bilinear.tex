\documentclass[main.tex]{subfiles}

\begin{document}

\section{
  Hidden Variable Bayesian Networks as Multilinear systems
}

Directed acyclic graphs (DAGs) are often used to characterize causal or
statistical models. In this case, each vertex in the graph corresponds to a
variable. The variables can be partitioned into observed variables, $\bf X$, and
unobserved variables $\bf U$. The full collection of variables is denoted $\bf V
= X \cup U$. The parents of a variable $V$ in the DAG $\mathcal G$, denoted
$pa_{\mathcal G}(V)$, are the collection of random variables $V'$ such that
there exists an edge $V' \rightarrow V$ in $\mathcal G$. A probability
distribution $P$ is said to be Markov to a DAG $\mathcal G$ if

\begin{align}
  \label{eq:markov}
  P({\bf V}) = \prod_{V \in \bf V} P(V \mid pa_{\mathcal G}(V)).
\end{align}

The distribution over observed variables, $P({\bf X})$, is called the observed
data distribution, or the observed data law. It would be very useful to be able
to list symbolically the conditions on $P({\bf X})$ such that there exists a
distrubtion $P({\bf V})$ markov to a hidden variable DAG $\mathcal G$ with

\begin{align}
  \label{eq:marginal}
  P({\bf X}) = \sum_{U \in \bf U}P({\bf X, U}).
\end{align}

These conditions would allow us reject the \textit{causal} model associated with
$\mathcal G$ based \textit{only} on the observed data distribution.

Suppose that each variable $X \in \bf X$ is discrete-valued. Then it is known
that without loss of generality, each variable in $\bf U$ can also be assumed to
be discrete-valued, with a cardinality given by a function of the cardinalities
of the observed variables and the structure of the graph. For discrete observed
data, then, we can describe the full distribution $P({\bf X, U})$ with a finite
number of variables.

Consider the directed acyclic graph

\begin{figure}[h!]
\centering
\begin{tikzpicture}[>=stealth, node distance=1.0cm]
\tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
\tikzstyle{edge} = [->, blue, very thick]
\node[vertex, circle] (a) {$A$};
\node[vertex, circle] (b) [right of=a] {$B$};
\node[vertex, circle] (c) [right of=b] {$C$};
\node[vertex, gray, circle] (u) [above of=b,yshift=-.3cm] {$U$};
\draw[edge] (b) to (a);
\draw[edge] (b) to (c);
\draw[edge, red] (u) to (a);
\draw[edge, red] (u) to (c);    
\end{tikzpicture}
\end{figure}

in which it is known ahead of time that all variables, including the unobserved
$U$, are binary valued. For notational convenience, we let the two binary values
be denoted by, for example, $a$ and $\bar a$. Also for notational convenience,
we let $P(A = a \mid B = b, U = u) = \phi_{a \mid b u}$, and likewise for all
other factors that appear in equation (\ref{eq:markov}). Then according to
equations (\ref{eq:markov}) and (\ref{eq:marginal}), the following must hold

\begin{align}
  P(a, b, c) = P(b)\phi_{a\mid b u}\phi_{c \mid b u}\phi_u +
               P(b)\phi_{a\mid b \bar u}\phi_{c \mid b \bar u}\phi_{\bar u}.
\end{align}

Dividing both sides by $P(b)$, which appears in all terms on the right hand
side, and can be deduced simply from the observed data distribution, yields

\begin{align}
   P(a, c \mid b) = \phi_{a\mid b u}\phi_{c \mid b u}\phi_u +
               \phi_{a\mid b \bar u}\phi_{c \mid b \bar u}\phi_{\bar u}.
\end{align}

In addition, we are constrained by the rules of probability. All these variables
must be greater than $0$, and probability distributions (including conditional
distributions) must sum to one.

For completeness, we produce the full system of equations implied by this model.


\begin{align*}
   P(a, c \mid b) = \phi_{a\mid b u}\phi_{c \mid b u}\phi_u +
               \phi_{a\mid b \bar u}\phi_{c \mid b \bar u}\phi_{\bar u} \\
   P(\bar a, c \mid b) = \phi_{\bar a\mid b u}\phi_{c \mid b u}\phi_u +
               \phi_{\bar a\mid b \bar u}\phi_{c \mid b \bar u}\phi_{\bar u} \\
   P(a, \bar c \mid b) = \phi_{a\mid b u}\phi_{\bar c \mid b u}\phi_u +
               \phi_{a\mid b \bar u}\phi_{\bar c \mid b \bar u}\phi_{\bar u} \\
   P(a, c \mid \bar b) = \phi_{a\mid \bar b u}\phi_{c \mid \bar b u}\phi_u +
               \phi_{a\mid \bar b \bar u}\phi_{c \mid \bar b \bar u}\phi_{\bar u} \\
   P(\bar a, \bar c \mid b) = \phi_{\bar a\mid b u}\phi_{\bar c \mid b u}\phi_u +
               \phi_{\bar a\mid b \bar u}\phi_{\bar c \mid b \bar u}\phi_{\bar u} \\
   P(\bar a, c \mid \bar b) = \phi_{\bar a\mid \bar b u}\phi_{c \mid \bar b u}\phi_u +
               \phi_{\bar a\mid \bar b \bar u}\phi_{c \mid \bar b \bar u}\phi_{\bar u} \\
   P(a, \bar c \mid \bar b) = \phi_{a\mid \bar b u}\phi_{\bar c \mid \bar b u}\phi_u +
               \phi_{a\mid \bar b \bar u}\phi_{\bar c \mid \bar b \bar u}\phi_{\bar u} \\
   P(\bar a, \bar c \mid \bar b) = \phi_{\bar a\mid \bar b u}\phi_{\bar c \mid \bar b u}\phi_u +
               \phi_{\bar a\mid \bar b \bar u}\phi_{\bar c \mid \bar b \bar u}\phi_{\bar u} \\
  1 = \phi_{a \mid b u} + \phi_{\bar a \mid b u}\\
  1 = \phi_{a \mid b \bar u} + \phi_{\bar a \mid b \bar u}\\
  1 = \phi_{a \mid \bar b u} + \phi_{\bar a \mid \bar b u}\\
  1 = \phi_{a \mid \bar b \bar u} + \phi_{\bar a \mid \bar b \bar u}\\
  1 = \phi_{c \mid b u} + \phi_{\bar c \mid b u}\\
  1 = \phi_{c \mid b \bar u} + \phi_{\bar c \mid b \bar u}\\
  1 = \phi_{c \mid \bar b u} + \phi_{\bar c \mid \bar b u}\\
  1 = \phi_{c \mid \bar b \bar u} + \phi_{\bar c \mid \bar b \bar u}\\
  1 = \phi_u + \phi_{\bar u}\\
  \phi \ge 0
\end{align*}

We now make several observations about this system.

First, we note that the coefficient on every monomial is always $1$. Second, we
note that the system of equation is linear in the following sets.

\begin{enumerate}
\item $\{\phi_u, \phi_{\bar u}\}$
\item $\{
  \phi_{a \mid b u},
  \phi_{\bar a \mid b u},
  \phi_{a \mid \bar b u},
  \phi_{\bar a \mid \bar  b u},
  \phi_{a \mid b \bar u},
  \phi_{\bar a \mid b\bar  u},
  \phi_{a \mid \bar b \bar u},
  \phi_{\bar a \mid \bar b \bar u}
  \}$
\item $\{
  \phi_{c \mid b u},
  \phi_{\bar c \mid b u},
  \phi_{c \mid \bar b u},
  \phi_{\bar c \mid \bar  b u},
  \phi_{c \mid b \bar u},
  \phi_{\bar c \mid b\bar  u},
  \phi_{c \mid \bar b \bar u},
  \phi_{\bar c \mid \bar b \bar u}
  \}$
\end{enumerate}

Third, we note that each thruple in the cross-space of these sets appears as a
monomial in the polynomial portion of the system of equations. Fourth, the
number of terms in each polynomial equation is equl to the cardinality of the
unobserved variable(s). Fifth, no variable has a power higher than $1$ in any
monomial. Some general form of these properties will hold for all systems of
equations that represent hidden variable DAGs. There may be additional
properties I am missing.

We are interested in \textbf{two questions}.

\textbf{First}, if the observed data distribution, as represented by the
conditionals $P(a, c \mid b)$ on the left hand side of the polynomial equations,
is \textit{known}, what is the most efficient way to discover whether or not the
system has a solution? If it is through the theory of elimination involving
grobner bases, is it possible to show that due to the special from of the system
of equations the elimination process will require no numerical optimization
methods? (For example, $x^2 = 4$ can be solved without recorse to numerical
optimization, but $x^5 + x^3 = 64$ cannot be).

\textbf{Second}, suppose that we find there is a way to use elimination theory
with grobner bases, and without numerical optimization techniques to find
solutions, to answer the first question. Is it possible to solve a
\textit{parametric} form of the same question, where the observed data
distribution is not known? The idea would be similar to our parametric simplex
algorithm - we would find a parametric grobner basis, partitioning the possible
space of observed data distributions whenever the step taken in a grobner-basis
searching algorithm depends on the value of the parameters (in this case, these
are the parameters of the observed data distribution). Then we would also
need a parametric form of the elimination algorithm.

I suspect all this may be possible, because only the constants are
parameterized, which I believe simplifies the problem. There are other important
causal questions about hidden variable DAGs that are closely related and could
be answered using any method that could answer either or both of these
questions. Finding sharp bounds on various objectives may be slightly more
involved.

\end{document}
