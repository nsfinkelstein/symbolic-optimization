\documentclass[main.tex]{subfiles}

\begin{document}

\paragraph{Proof of Proposition \ref{prop:tabularize}}
\begin{proof}
  First, we show that the subspaces associated with the nodes on the second
  level of the tree partition $\mathcal F$. To see this, we first observe that
  by construction they are non-overlapping, as any parameter subspace previously
  assigned to a node is excluded from the parameter subspace constructed to be
  assigned to a new node in line 6 of the algorithm. Next, we observe that the
  full space must be covered, because at the iteration of the loop on line 3 in
  which $i = 1$, the matrix $A'_{\phi, R, B}$ is a single number. At least one
  such number must be invertible unless $A'_\phi$ is a matrix of zeros, which is
  prevented by assumption.

  Next, we show that the tableaus in each of the leaf nodes are equivalent to
  the original PLP for their corresponding parameter spaces, excepting, as
  usual, the non-negativity constraints. To see this, we first observe that the
  matrix $A'_{\phi, R, B}$ is by construction not inverted anywhere it is
  singular, such that lines 8 - 10 make $x_B$ basic. Next, we observe that --
  because we check for the largest possible basic sets first, and exclude
  parameter subspaces that are already associated with a tableau from being
  associated with later tableaus and smaller basic sets -- each node makes basic
  as many variables as possible in its corresponding parameter subspace.
  Therefore no constraints are lost in converting to tableau form, as in all
  cases where rows of $A'_\phi$ are not used, they represent redundant
  constraints.
\end{proof}

\paragraph{Proof of Proposition \ref{prop:initialization}}
\begin{proof}
  To prove that the subspaces associated with each non-leaf node are partitioned
  by the subspaces associated by their children, we first examine nodes that are
  in the input tree. We add children these nodes in lines 10 and 37. To see that
  these children have disjoint associated parameter subspace, we note that
  subspace constructed in line 35 has $b_{\phi, i} \ge 0$ for all $i$, whereas
  the subspaces constructed in line 6 has $b_{\phi, k} < 0$ for at least one
  $k$. To see that these latter subspaces are mutually disjoint, we simply note
  that they each correspond to the region in which $b_{\phi, k}$ is smaller than
  all lexigraphically earlier values of $b_\phi$, and at least as small as all
  subsequent values.
  
  By Proposition \ref{prop:parametric-simplex}, this property obtains for all
  nodes added by the parametric simplex algorithm in line 11. Finally, we
  examine the children added to each node output as a leaf node by the
  parametric simplex. These nodes have tableaus that represent the optima of the
  auxiliary linear programs for their corresponding parameter spaces. Because
  the objective function is to maximize a non-negative value, the objective
  cannot be unbounded. In the subspace in which the optimum in non-zero,
  constructed in line 14, the original PLP is infeasible, as no setting of the
  original variables can make the equations hold. If the auxiliary variable is
  nonbasic, the space on which the original problem is feasible is represented
  by the node added in line 22. This subspace of this node is by construction
  the complement of that representing the subspace on which the original problem
  is feasible.
  
  If the auxiliary variable is basic, it must be pivoted out of the basis. The
  nonbasic variable that enters the basis must have a non-zero coefficient in
  the equation for the auxiliary variables. In the for loop on line 25, by
  construction each node created in that loop is associated with a parameter
  space that does not overlap with either the infeasible set or any node
  previously constructed in the loop, as their associated parameter spaces are
  excluded in line 26. To see that the sets collectively cover the whole space
  in which the parent node is feasible, we note that the only portion of this
  space that is not covered would have to have the auxiliary random variable set
  identically to zero (i.e. not given in terms of either the parameters or the
  variables of the system). However, by construction the auxiliary random
  variable appears in every constraint in the system of equations, and therefore
  cannot be exactly identified unless there are as many constraints as variables
  in the auxiliary program, which would mean that the original program would
  have more equations than variables and therefore be infeasible. Such programs
  are excluded by assumption.
  
  We have left to prove that each leaf node contains a tableau that is
  equivalent to the original PLP and in which the basic solution is feasible in
  the subspace corresponding to that node, or contains the status
  ``infeasible'', in which case the original PLP is infeasible for any parameter
  in that node's subspace. Our strategy here will simply be to show that for any
  parameter in the parameter subspace corresponding to any of the leaves, the
  tableau will have undergone exactly the same pivot operations and matrix
  manipulations as it would have undergone in the numeric version of the simplex
  initialization algorithm as presented in \cite{clrs}. 

  To this end, we first observe that the construction of the auxiliary linear
  program in lines 2 - 4 do not depend on the parameter values. Next, we observe
  for any parameter value corresponding to $b_{\phi, k}$ being the lexigraphical
  first element in $b_{\phi}$ to achieve the minimum of all values in $b_\phi$,
  the basic variable $k$ exists the basis and the auxiliary variable enters,
  leading to a tableau with a basic feasible solution for any such parameter
  value. By Proposition \ref{prop:parametric-simplex}, the parametric simplex
  algorithm on line 11 also performs the same operations on the input PLP as
  would be performed on a numeric PLP for every parameter in the space. Next, we
  observe that for any parameter in the space for which the auxiliary random
  variable is non-zero in the optimal solution to the auxiliary program, the
  corresponding numerical LP would be marked as infeasible, as in line 16. If
  the auxiliary variable is nonbasic, the corresponding column is removed from
  the $A$ matrix, and $c$ is restored as in lines 18 - 22. Otherwise, the
  auxiliary random variable is pivoted out of the basis before the auxiliary
  random variable removed and the objective restored, as in lines 24 - 33.
  Finally, we observe that no pivot operations or matrix manipulations are
  needed when the initial basic solution is feasible, corresponding to
  parameters in the space defined in line 37.
\end{proof}

\begin{proof}
\end{proof}

\paragraph{Proof of Proposition \ref{prop:parametric-simplex}}
\begin{proof}
  We first consider children of each node for which the algorithm performs a
  pivot. The subspaces constructed in line 16 identify the regions of the
  parameter space for which the entering and leaving variables $e$ and $\ell$
  would be selected according to Bland's rule. Because Bland's rule uniquely
  identifies entering and leaving variables for any numerical LP, these spaces
  are non-overlapping. For each entering variable, line 10 constructs the space
  for which no leaving variable would be selected by Bland's rule, corresponding
  to an unbounded program. Therefore the full space $\mathcal E_e$ is covered by
  non-overlapping subpsaces associated with children of the node. Finally, if no
  variable is selected as the entering variable, the linear program must be
  complete. The parameter space associated with this determination, defined on
  line 21, is by construction the complement of the union of spaces for which
  some variable is chosen to enter the basis. Therefore the children of each
  node partition its subspace, and for any given parameter, value, the path from
  the root to the corresponding leaf corresponds to exactly the tableaus that
  would arise from an application of the standard simplex algorithm to the
  corresponding numerical linear program.
\end{proof}

\paragraph{Proof of Theorem \ref{thm:full-algorithm}}
\begin{proof}
  Follows directly from Propositions \ref{prop:tabularize},
  \ref{prop:initialization}, and \ref{prop:parametric-simplex}.
\end{proof}
\end{document}
%   We recall the associativity property of the pivot routine described in
%   equation (\ref{eq:pivot-associative}). This property tells us that we will get
%   the same result if we first substitute a specific value $\phi^*$ into
%   $\mathcal I_\phi$ and then run any number of pivot operations beginning with
%   the numerical result of that substitution, or if we run exactly the same pivot
%   oeprations (i.e. using the same sequence of entering and leaving variables)
%   starting with $\mathcal I_\phi$, and substitute the specific value $\phi^*$
%   into the result of those operations.

%   Now we show that for any $\phi^*$, $\psi(\mathcal P(\mathcal I_\phi), \phi^*)$
%   will be a solution corresponding to the result of exactly the same pivot
%   operations as $\mathcal S(\chi(\mathcal I_\phi, \phi^*))$. It follows
%   from the construction of the partition of $\mathcal F$ that for any pairing of
%   a subspace and its corresponding state $(\mathcal F_1, s_1)$, the following
%   hold for any $\phi^* \in \mathcal F_1$. If the state $s_1$ is ``unbounded'',
%   the SIMPLEX algorithm would return ``unbounded'' without executing a pivot. If
%   the state is ``optimal'', the SIMPELX algorithm would conclude that the
%   initial feasible solution corresponds to the optimal value of the objective
%   without executing a pivot. Finally, if the state is a new parametric system of
%   equations, that system was constructed by executing a pivot with the same
%   entering and leaving variables as would have been used by the first iteration
%   of the SIMPELX algorithm.
  
%   Next, we note that in the case of the PARAMETRIC-SIMPLEX, if the algorithm is
%   not concluded after a single iteration, the next iteration is equivalent to
%   what the first iteration would have been if it had been called with inputs
%   $(s_1, \mathcal F_1)$ . Likewise for the SIMPLEX algorithm with the new value
%   of $(N, B, A, b, c, \nu)$ obtained after the first pivot. Therefore
%   examination of the first iteration suffices.
% \end{proof}


% \subsubsection{The Pivot Routine}

% The simplex algorithm makes use of the Pivot routine, defined in Algorithm
% \ref{alg:pivot}, adapting notation from \cite{CLSR}. In the context of the
% simplex algorithm, a pivo

% We can see that the
% routine has two important properties. First, the operations of the routine
% are the same regardless of the numeric values of the inputs, i.e. there are no
% conditional statements. Second, all of the operations on the input tensors are
% basic binary arithematic operations. It therefore follows from the associative
% property mentioned above that
% {\small
%   \begin{align}
%   \label{eq:pivot-associative}
%   Pivot(N, B, \chi(A_\phi, \phi^*), \chi(b_\phi, \phi^*),
%     \chi(c_\phi, \phi^*), \chi(\nu_\phi, \phi^*), l, e)
%     = \chi(Pivot(N, B, A_\phi, b_\phi, c_\phi, \nu_\phi, l, e), \phi^*),
% \end{align}
% }
% for any input $(N, B, A_\phi, b_\phi, c_\phi, \nu_\phi, l, e)$.

% \begin{algorithm}[h]
% 	\caption{Pivot}
%   \label{alg:pivot}
%   \include{pivot}
% \end{algorithm}

% At a high level, the inputs to the Pivot routine can be thought of as a summary
% of a system of equations whereby the variables in the ``non-basis'' set $N$ are
% defined in terms of the variables in the ``basis'' set $B$, with the inputs $A,
% b, c, \nu$ being used to characterize the system of equations. The inputs $e,
% \ell$ are used to identify an entering variable, $e$ which will enter the basis
% set, and a leaving variable $\ell$, which will leave the basis set. The Pivot
% routine then outputs a new system of equations, such that the new non-basis
% variables, following this exchange of leaving and entering variables, are given
% in terms of the new basis variables.


\end{document}
