\documentclass[main.tex]{subfiles}

\begin{document}

\section{Introduction}

Linear programs (LPs) are an important class of constrained optimization
problems in which the objective function and each of the constraints can be
represented as linear functions of the variables in the system. LPs are
regularly used across a wide range of fields \cite{chvatal1983lp, karloff2008lp,
  gass2003lp, dorfman1987lp}. In some cases, a subset of the coefficients in LPs
may be \textit{unknown} \cite{sahinidis2004uncertain}. Such coefficients can be
represented by parameters, resulting in a parameterized linear program (PLP).
These parameters, which we denoted as $\phi$, are distinct from the
variables of the linear program over which optimization cocurs, which we denote
$x$. We denote the space of possible parameter values by $\mathcal
F$, and make the standard assumption that $x \in \mathbb{R}^+$.

PLPs have been studied in many contexts \cite{bental2000robust,
  birge2011stochastic, yu2019efficient, rountree1982plp}. A parametric solution
to a PLP provides an analytic map $x_\phi: \mathcal F \rightarrow \mathbb{R}^+$
from possible parameter values $\phi$ to the optimal settings of $x$, and
therefore to the optimal objective value. These solutions have many uses. For
the purposes of motivating this work, we focus on three such uses. The first use
is as a method for sensitivity analysis. When practioners are \textit{uncertain}
about coefficients in the system, they may want to know how much the solution to
the optimization problem varies with the coefficients. If they find that it
varies dramatically, they may invest resources in reducing uncertainty. If, by
contrast, they find that it barely varies, they may feel comfortable using their
existing problem formulation. The use of PLPs for sensitivity analysis has been
explored extensively \cite{masse1957sensitive, hadley1963lp, das2017rnn}.

The second use of interest arises when parametric solutions are of interest for
their own sake. One prominent example of this use arises in causal inference. It
has been shown that in some cases, causal targets of inference and modeling
constraints can be expressed linearly, leading to a PLP parameterized by the
parameters of a probability distribution \cite{balke1993ivbounds,
  sachs2020linear, balke1997thesis, finkelstein2020measurement-error,
  sachs2020linear}. In such cases, it is possible to \textit{bound} the target
of inference by minimizing and maximizing the corresponding linear expression
over the linear model. Parametric solutions of such PLPs, corresponding to
expression of these bounds as functionals of probability distributions,
facilitate use of well understood statistical estimation methods, as well as
use of the bounds in fields not equipped to run linear programs.

The final use case we consider is one in which a parametric solution to a linear
program offers a computational advantage. Generally, given some parameters
$\phi^*$, evaluating $x_{\phi}(\phi^*)$ to obtain the optimal solution will be
substantially faster than numerically solving the LP corresponding to
substituting $\phi^*$ into a PLP. It follows that if an PLP must be solved many
times for different parameter values, it may be most efficient to obtain a
parametric solution. Similarly, if there is a need to obtain the optimual
solution quickly after the parameters become known, a parametric solution may be
of use.

While much attention has been paid to PLPs with one or
more parameters in the objective vector $c$ or the so-called
``right hand side'' of the constraints $b$ \cite{gass1955plp, gal1972plp,
  yuf1976plp, borrelli2003plp}, much less
work has been done on PLPs with parameters in the ``left hand side'' of the
constraints, also sometimes refered to as the ``technology matrix'', $A$. Work
on PLPs with a parameterized technology matrix often assumes there is only a
single parameter in the technology matrix, or that only a single row or column
of the technology matrix is parameterized \cite{gal1995lhs}. There are also
methods for producing \textit{approximate} parametric solutions under arbitrary
uncertainty in the technology matrix \cite{li2007plp}.

More recently, \cite{charitopoulos2017plp} proposed an approach that
provides an exact solution under parameterization of all three components of an
LP. We will show that the algorithm proposed in \S \ref{sec:algorithm}
outperforms their approach, while providng more complete information about
regions of $\mathcal F$ for which the PLP is infeasible or has an unbounded
objective. See the introduction to \cite{charitopoulos2017plp} for a more
detailed review of existing PLP methods.

For the sake of clarity, we will begin by assuming a linear program in
standard form, and address the possibility of equality constraints later in \S
\ref{sec:nonsingular}. Allowing a subscript of $\phi$ to indicate that a vector
or matrix is parameterized, PLPs in standard form can be expressed as
%
\begin{align}
  \label{eq:plp}
  \text{optimize~~} c_\phi^Tx \text{~~subject to~~} A_\phi x \le b_\phi \text{~~and~~} x \ge 0.
\end{align}
%
Our approach builds on the Simplex algorithm \cite{dantzig1990simplex}, a
popular approach for solving LPs. The Simplex algorithm takes advantage of the
fact that the feasible region for $x$ defined by linear constraints is a convex
polyhedron, and that the optimum of a linear objective in a convex polyhedron,
if it exists, must occur at a vertex of the polyhedron. The main idea behind the
simplex algorithm is that at each iteration, the algorithm visits a vertex of
the polyhedron of feasible $x$ values that is (i) adjacent to the vertex it is
presently visiting, and (ii) at which the objective is greater. It may also
decide at each iteration that the optimum has been obtained, or that the
objective is unbounded over the polyhedron.

In broad strokes, our proposed algorithm keeps track of the conditions on $\phi$
under which the simplex algorithm would take each of these possible decisions.
We formalize this idea as the construction of a tree, where the root represents
the original PLP. At each iteration of the construction of this tree, the
algorithm visit each leaf node, which represents the state of problem after a
number of iterations corresponding to its depth. For each possible decision that
the simplex algorithm would made for some permitted values of $\phi$ at that
iteration, we add a child to the node representing the state of the problem
after the operations corresponding to that decision as well as the subset of the
parameter space for which all decisions leading to that state would have been
made.

The remainder of this work is organized as follows. In \S \ref{sec:prelim}, we
review the simplex algorithm in more detail and introduce additional tools used
in our approach. In \S \ref{sec:algorithm}, we introduce the parametric simplex
algorithm, alongside additional routines for initializing the algorithm, and for
handling problems with equality constraints. In \S \ref{sec:applications}, we
apply our algorithm to a number of PLPs, including several previously explored
in the literature. Finally, we conclude in \S \ref{sec:conclusion} with a brief
discussion.

\end{document}
