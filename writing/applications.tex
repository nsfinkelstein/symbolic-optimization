\documentclass[main.tex]{subfiles}

\begin{document}

\section{Applications}
\label{sec:applications}

In this section, we apply our algorithm to several problems from the literature
on providing parametric solutions to PLPs. We focus especially on examples
provided in \cite{charitopoulos2017plp}, as their approach -- like ours --
provides exact solutions to PLPs in which all components of the program are
parameterized, and they report their algorithm's runtime on each example. We
compare the reported runtimes of their procedure to that of our algorithm,
although because we were unable to obtain the source code for their algorithm,
the algorithms will have been run on different hardware. For that reason the
runtimes are not directly comparable. However, as we will see, our algorithm is
faster in all cases. In addition, we use our algorithm to provide parameteric
bounds under a common measurement error model, employing the PLP formulation
described in \cite{finkelstein2020measurement-error}. We illustrate the output
of the algorithm as a decision tree, though as described above, the leaf nodes
of the output of the algorithm are sufficient to trivially construct so called
``critical regions,'' which can also be recovered from our decision
tree presentation.

As a final check on the correctness of our algorithm, we numerically validated
its output for each of these examples as follows. For each PLP $\mathcal
L_\phi$, we randomly drew 100,000 parameter values $\phi^*$ from $\mathcal F$
and used the numerical LP optimizer implemented in scipy \cite{scipy2020} to
obtain numerical solutions -- or determinations that the LP was infeasible or
unbounded -- to $\mathcal L_\phi(\phi^*)$. We then compared these solutions to
the parametric solutions provided by our algorithm, and found that they
coincided in all cases.

\subsubsection*{Example 1}

We begin with an example introduced in \cite{li2007plp}, in which an
approximate parametric solution was provided, and revisited in
\cite{charitopoulos2017plp}, in which an exact solution was provided. This
example has a single uncertain parameter in each of the objective function, the
``left hand side'' of the constraints and the ``right hand side'' of the
constraints. The parametric linear program is:
%
\begin{align*}
  & &\min_x \phi_1 x_1 + x_2&\\
  & &\text{s.t.~} -x + x_2 + x_3 &= \phi_2\\
  & &x_1 - \phi_3 x_2 + x_4 &= 1\\
  & &x &\ge 0\\
  & &\mathcal F = \{\phi \mid -5 \le \phi_i \le 5 &\text{~ for all ~} i\}.
\end{align*}
%
\begin{sidewaysfigure}
\begin{tikzpicture}
\Tree [.root
  [.{$\phi_2 \ge 0$}
    [.{$\phi_1 \ge 0$} [.\node[draw]{$0$}; ] ]
    [.{$\phi_1 < 0$}
      [.\node[align=center]{$\phi_3 \ge 1$\\$ \phi_1\phi_3 < -1$}; [.\node[draw]{unbounded}; ] ]
      [.\node[align=center]{$\phi_3 < 1$\\$ \phi_1\phi_3 < -1$}; [.\node[draw]{$\frac{\phi_1 \phi_2\phi_3 + \phi_1 + \phi_2 + 1}{1 - \phi_2}$}; ] ]
      [.{$ \phi_1\phi_3 \ge -1$} [.\node[draw]{$\phi_1$}; ] ]
    ]
  ]
  [.{$\phi_2 < 0$}
    [.{$\phi_2 < -1$}
      [.{$\phi_3 > 1$}
        [.{$ \phi_1\phi_3 \ge -1$} [.\node[draw]{$\frac{\phi_1 \phi_2\phi_3 + \phi_1 + \phi_2 + 1}{1 - \phi_2}$}; ] ]
        [.{$ \phi_1\phi_3 < -1$} [.\node[draw]{unbounded}; ] ]
      ]
      [.{$\phi_3 \le 1$} [.\node[draw]{infeasible}; ] ]
    ]
    [.{$\phi_2 \ge -1$}
      [.{$\phi_1 \ge 0$} [.\node[draw]{$-\phi_1\phi_2$}; ] ]
      [.{$\phi_1 < 0$}
        [.{$\phi_1\phi_3 \ge 1$} [.\node[draw]{$\phi_1$}; ] ]
        [.\node[align=center]{$\phi_3 < 1$\\$ \phi_1\phi_3 < 1$}; [.\node[draw]{$\frac{\phi_1 \phi_2\phi_3 + \phi_1 + \phi_2 + 1}{1 - \phi_2}$}; ] ]
        [.\node[align=center]{$\phi_3 \ge 1$\\$ \phi_1\phi_3 < 1$}; [.\node[draw]{unbounded}; ] ]
      ]
    ]
  ]
]
\end{tikzpicture}
\caption{Decision tree representing the solution to Example 1}
\label{fig:example1}
\end{sidewaysfigure}
%
We present the output of our algorithm as a decision tree in Figure
\ref{fig:example1}. In this visualization, each leaf node in the decision tree
is associated with a parametric expression for the optimum, if it exists, for
every parameter that would lead to that leaf in the decision tree. Not captured
in the visualization is the fact that each leaf in the otuput of the algorithm
itself contains full information about the parameter subspace in which it
applies, as well as parameteric expressions for the variable assignments that
lead to the optimum. It can easily be seen that this decision tree implies the
same critical regions as those presented in \cite{charitopoulos2017plp} for the
same problem, with additional information about regions in which the problem is
unbounded or infeasible.

\subsubsection*{Example 2}

Next, we examine a larger problem explored in \cite{charitopoulos2017plp} and
adapted from \cite{edgar2003chem}. The problem represents the optimization of
profits, subject to inequality constraints imposed by the capacity of components
of a thermal cracker, and equality constraints imposed by the nature of
chemical reactions. The variables in the system represent the quantities of
chemicals input and output from the system. The parameters -- introduced in
\cite{charitopoulos2017plp} -- represent uncertainty about the cost of one
chemical input ($\phi_1$), a stoichimetric constant in a chemical reaction
($\phi_2$), and capacity of the thermal cracker ($\phi_3$). Full detail about
the problem setup can be found in \cite{edgar2003chem}. The parameterized linear
program is

\begin{align*}
  &\max_x~~&2.84x_1 - \phi_1x_2 - 3.33x_3 + 1.09x_4 + 9.39x_5 + 9.51x_6&\\
  &\text{s.t.~~} &1.1x_1 + 0.9x_2 + 0.9x_3 + \phi_2x_4 + 1.1x_5 + 0.9x_6 &\le 200,000\\
  & &0.5x_1 + 3.5x_2 + 0.25x_3 + 0.25x_4 + 0.5x_5 + 0.35x_6 &\le 100,000 + \phi_3\\
  & &0.01x_1 + 0.15x_2 + 0.15x_3 + 0.18x_4 + 0.01x_5 + 0.15x_6 &\le 20,000\\
  & &0.4x_1 + 0.06x_2 + 0.04x_3 + 0.05x_4 - 0.6x_5 + 0.06x_6 &= 0\\
  & &0.1x_2 + 0.01x_3 + 0.01x_4 - 0.9x_6 &= 0\\
  & &-6857.6x_1 + 364x_2 + 2032x_3 - 1145x_4 - 6857.6x_5 + 364x_6 + 21,520x_7 &\le 20,000,000\\
  & &x &\ge 0\\
  & &\mathcal F = \{\phi \mid 0 \le \phi_1 \le 6, 0 \le \phi_2 \le 1.5, -50,000 \le \phi_3 \le 50,000 \}.
\end{align*}
%
\begin{sidewaysfigure}
\begin{tikzpicture}
\Tree [.root
  [.{$\phi_3 < -9090.909$} [.\node[draw]{$10.92\phi_3 + 1092000$}; ] ]
  [.{$\phi_3 \ge -9090.909$} 
    [.\node[align=center]
    {
      $\phi_2 < 0.297$\\
      $\phi_3 \le -9090.909 + (0.249 - .455\phi_2)\frac{18181.818}{0.182 -0.009\phi_2}$};
    [.\node[draw]{$\frac{10.92\phi_2\phi_3 + 1092000\phi_2 - 3.242 \phi_3 - 574036.667}{\phi_2 - 0.549}$}; ] ]
    [.{$\phi_2 \ge 0.297$} [.\node[draw]{$992727.273$}; ] ]
    [.\node[align=center]
    {
      $\phi_2 < 0.297$\\
      $\phi_3 > -9090.909 + (0.249 - .455\phi_2)\frac{18181.818}{0.182 -0.009\phi_2}$};
    [.\node[draw]{$\frac{99272.727\phi_2 - 207046.667}{0.009\phi_2 - 0.182}$}; ] ]
  ]
]
\end{tikzpicture}
\caption{Decision tree representing the solution to Example 2}
\label{fig:example2}
\end{sidewaysfigure}

A decision-tree visualization of the output of our algorithm is presented in
Figure \ref{fig:example2}. In this case, the output of our algorithm does not
match the critical regions described in \cite{charitopoulos2017plp} for the same
problem. Without access to the source code used by \cite{charitopoulos2017plp}
to solve this problem we cannot know why this is. However -- as mentioned above
-- we have numerically validated this solution and know it to be correct. We
note that the solution in \cite{charitopoulos2017plp} seems to perform some
aggressive rounding. For example, their solution for the region in which our
algorithm returns the constaint 99272.273 as optimal is 99300, which appears to
simply be rounded to the nearest hundered.

However, there are additional problems with their proposed solution. For
example, certain permitted parameter values that have valid optimal solutions
are excluded from each of the critical regions presented in
\cite{charitopoulos2017plp}. For example, the parameter value $\phi_1 = 0$,
$\phi_2 = .2$, $\phi_3 = 10000$ is not in any of the 4 critical regions
presented in their work, but the corresponding LP can easily numerically be
confirmed to have the valid numerical optimum of $1041358.732$. This is in line
with our parameteric solution.

We speculate that this issue arises because the approach of
\cite{charitopoulos2017plp} relies an algrebraic software to find \textit{all}
solutions to a polynomial system of equations. While this is possible in theory,
many implementations of the relevant algorithms rely on heuristics to find as
many solutions as possible in a reasonable amount of time. Missing solutions to
the polynomial system would lead to issues like the one observed above.

\subsubsection*{Example 3}

In this section, we consider a measurement error problem. We assume that a
variable of interest, $X$, is unobserved, but has an observed proxy $Y$. We
further assume that another variable $A$ is a cause of $X$, and does not affect
the proxy $Y$ other than through $X$. This scenario is depicted graphically in
Figure \ref{fig:iv}. We suppose all random variables are binary, and would like
to bound the probability that the unobserved variables was equal to $1$. Recent
work \cite{finkelstein2020measurement-error, sachs2020linear} has shown that
this problem -- and many related problems involving bounds in measurement error
and causal inference -- may be framed as linear programs, in which the program
is parameterized by the parameters of the observed data distribution.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[>=stealth, node distance=1.0cm]
\tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
\tikzstyle{edge} = [->, blue, very thick]

	\begin{scope}
    \node[vertex, circle] (z) {$A$};
    \node[vertex, gray, circle] (a) [right of=z] {$X$};
    \node[vertex, circle] (y) [right of=a] {$Y$};
    \node[vertex, gray, circle] (h) [above of=a, xshift=0.5cm, yshift=-0cm] {$U$};

    \draw[edge] (z) to (a);
    \draw[edge] (a) to (y);
    \draw[edge, red] (h) to (a);
    \draw[edge, red] (h) to (y);    
  \end{scope}
\end{tikzpicture}
\caption{
  A graphical model representing a measurement error scenario. The unobserved
  variable $X$ has an observed proxy, $Y$, with which it is confounded. The
  cause $A$ affects the observed proxy $Y$ only through $X$. We are interested
  in bounding the marginal distribution of $X$.
}
\label{fig:iv}
\end{figure}

In this program, $x$ represent the parameters of the distribution of the
unobserved variable $U$, which confounds the unobserved variable of interest $X$
with its observed proxy $Y$. Because this distribution is unknown, its
parameters function as the variables in the system. The parameters $\phi_{ij}$
represent the parameters of the joint distribution of the observed variables $A$
and $Y$, with $\phi_{ij} \equiv P(A = i, Y = j)$, and function as the parameters
of the PLP. The constraints obtain because of the relationships between the two
distributions.

The objective represents the probability that $X = 1$. Its maximum and minimum
over the constraints, which represent the measurement error model, form
\textit{bounds} on values it can take that are consistent with he observed data
distribution $\phi$.
% 
\begin{align*}
  \text{optimize~~~}
  &x_{13} + x_{14} + x_{15} + x_{16} +
  (\phi_{00} + \phi_{01})(x_{9} + x_{11} + x_{10} + x_{12})
  + (\phi_{10} + \phi_{11})(x_{5} + x_{7} + x_{6} + x_{8})\\
  \text{subject to~~}
  &\frac{\phi_{00}}{\phi_{00} + \phi_{01}} = x_{1} + x_{2} + x_{5} + x_{6} + x_{9} + x_{11} + x_{13} + x_{15}\\
  &\frac{\phi_{01}}{\phi_{00} + \phi_{01}} = x_{3} + x_{4} + x_{7} + x_{8} + x_{10} + x_{12} + x_{14} + x_{16}\\
  &\frac{\phi_{10}}{\phi_{10} + \phi_{11}} = x_{1} + x_{2} + x_{9} + x_{10} + x_{5} + x_{7} + x_{13} + x_{15}\\
  &\frac{\phi_{11}}{\phi_{10} + \phi_{11}} = x_{3} + x_{4} + x_{11} + x_{12} + x_{6} + x_{8} + x_{14} + x_{16}\\
  &\mathcal F = \{\phi \mid \sum_{ij} \phi_{ij} = 1, \phi_{ij} > 0 \text{~for all~} i, j\}
\end{align*}
%                                                
Recalling that $\phi_{ij} \equiv P(A = i, Y = j)$, we can write the decision
criteria in the tree in terms of marginal and conditional probabilities. The
optimal values are most easily expressed in terms of $\phi$. The results of the
algorithm run on the maximization problem are depicted in Fig. \ref{fig:bounds}.
The output of the algorithm for minimization problem has exactly the same
decision points (and consequnetly the same critical regions), and is not depicted.

\begin{sidewaysfigure}
\begin{tikzpicture}
\Tree [.root
  [.{$P(Y = 1 \mid A = 0) \le P(Y = 1 \mid A = 1)$}
    [.{$P(A = 0) \ge P(A = 1)$}
      [.\node[draw] {$\frac{\phi_{00}^2 + 2\phi_{00}\phi_{01} + \phi_{00}\phi_{10} + \phi_{01}^2 + 2\phi_{01}\phi_{10} + \phi_{01}\phi_{11}}{\phi_{00} + \phi_{01}}$}; ] ]
    [.{$P(A = 0) < P(A = 1)$}
      [.\node[draw]{$\frac{\phi_{00}\phi_{10} + 2\phi_{01}\phi_{10} + \phi_{01}\phi{11} + \phi_{10}^2 + 2\phi_{10}\phi_{11} + \phi_{11}^2}{\phi_{10} + \phi_{11}}$}; ] ]
  ]
  [.{$P(Y = 1 \mid A = 0) > P(Y = 1 \mid A = 1)$}
    [.{$P(A = 0) \ge P(A = 1)$}
      [.\node[draw]{$\frac{\phi_{00}^2 + 2\phi_{00}\phi_{01} + \phi_{00}\phi_{10} + 2\phi_{00}\phi_{11} + \phi_{01}^2 + \phi_{01}\phi_{11}}{\phi_{00} + \phi_{01}}$}; ] ]
    [.{$P(A = 0) < P(A = 1)$} 
      [.\node[draw]{$\frac{\phi_{00}\phi_{10} + 2 \phi_{00}\phi_{11} + \phi_{01}\phi_{11} + \phi_{10}^2 + 2\phi_{10}\phi_{11} + \phi_{11}^2}{\phi_{10} + \phi_{11}}$}; ] ]
  ]
]
\end{tikzpicture}
\caption{Example 3 Maximization problem}
\label{fig:bounds}
\end{sidewaysfigure}

These results are useful in part because they allow for an explicit expression
of the bounds that can be employed in settings where practitioners are not
equipped to run linear programs. In addition, the bounds can now be estimated
using methods for statistical estimation of functionals of the observed data
distribution. Such methods are better understood than methods for estimation of
the results of numerical linear programs. See
\cite{finkelstein2020measurement-error} for more details on how to construct
linear programs for obtaining bounds in the presence of measurement error. More
involved modeling assumptions may involve parameterization of the left hand side
as well as the right hand side and the objective. We posit that obtaining
results that are parametric in the parameters of the modeling assumptions can
help analysts evaluate the sensitivity of the bounds they obtain to their
modeling assumptions.

\subsection{Scalability}

\begin{table}[h]
  \centering
\begin{tabular}{| c | c c |} \hline
  & parametric-simplex & charitopoulos2017plp \\ \hline
  Example 1 & 1.508 & 5.635 \\
  Example 2 & 0.726 & 69.546 \\
  Example 3-max & 1.557 & - \\
  Example 3-min & 1.681 & -\\
  Example 4 & 1.333 & 2.452 \\
  Example 5 & 0.709 & 2.645 \\
  \hline
\end{tabular}
\caption{Time in seconds it took our algorithm (parametric simplex) to run each
  example, with comparison to charitopoulos2017plp when possible.}
\label{tab:timing}
\end{table}

Table \ref{tab:timing} shows the amount of time the algorithm took to run on
each of the examples explored in this work, in addition to two runs for which
the results were not reported. Where relevant, we also included the time
reported by \cite{charitopoulos2017plp}. In addition to examples 1 and 2
described above from \cite{charitopoulos2017plp}, we run two additional programs
explored in \cite{charitopoulos2017plp}, to enable comparison on the 4 problems
found to be most difficult in that work. The two additional programs are
reported in Table \ref{tab:timing} as Examples 4 and 5, and correspond to
Examples 3a and 3b respectively in \cite{charitopoulos2017plp}.

We note that we were unable to obtain source code for the algorithm in
\cite{charitopoulos2017plp}, so the times recorded in Table \ref{tab:timing} for
their algorithm are those reported in \cite{charitopoulos2017plp}, and are
therefore not directly comparable, as the algorithms were run on different
hardware. However, our hardware is a ten year old, middle-of-the-line laptop,
which, in our evaluation, is unlikely to be faster than the hardware used in the
experiments of \cite{charitopoulos2017plp}.

We believe these results show that the approach described in this paper allow
for parametric solutions to larger PLPs than could have been feasible addressed
using existing methods. Parallelizing the implementation as described above, and
taking steps raised in \S \ref{sec:conclusion}, will further expedite the
algorithm.

\end{document}