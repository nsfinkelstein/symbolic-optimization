# Before publication checklist (2023-12-23)

I am close to finishing this project, but don't have time to wrap it up. This readme should serve
basically as a checklist for wrapping things up.

The most up-to-date version of the paper and code are in the `aistats-submission` subdirectory.

### Comparison to [Charitopoulos et al. 2017](https://aiche.onlinelibrary.wiley.com/doi/epdf/10.1002/aic.15755)

In `applications.py`, there are functions called `example1()` and `example6()`. These two functions
return a tuple containing (i) a dataframe compairing our results, Charitopoulos et al.'s  results,
and numerical results for parameter values sampled randomly from the space, (ii) the parameteric
simplex tree returned by our algorithm, and (iii) critical regions extracted from the tree.

The `example1` function compares our results to those of Example 5 of Charitopoulos et al. The
high-level view is that we seem to get identical results. Our algorithm runs a bit faster than
their's. We also get explicit descriptions of the spaces corresponding to unbounded and infeasible
programs.

The `example6` function compares our results to those of the Thermal cracker case study in
Charitopoulos et al. Here, the results are more nuanced.

First, and most importantly, there is a whole critical region that Charitopoulos et al. completely
miss. This region basically corresponds to points that don't fall into any of the regions they list,
and the solution in this region has a form that they are not aware of. This is the main issue to
point out when discussing our advantage

Second, Charitopoulos et al.'s results are heavily rounded, which substantially hurts their
accuracy, and also means that sometimes parameters get mapped to the "wrong" critical regions. To
highlight this issue, we can create two summaries. First, we can count the percentage of programs
corresponding to parameter samples for which their result puts the answer in the "wrong" critical
region. This can be taken to be an estimate of the percent of the region for which the wrong
critical region is chosen, as parameters are uniformly sampled from the region. Second, we can show
the average difference between the truth and their solution, potentially broken down by critical
region.

Third, the problem specification in Charitopoulos et al. specifies that they are minimizing the
objective subject to the constraints, but in fact they are maximizing it.

#### Neat descriptions of critical regions

A major difficulty of this comparison is that in Example 6, the critical regions are very
complicated, and cannot be neatly represented. This makes it difficult to explicitly state the
difference with Charitopoulos et al.'s critical regions. To get around this issue, I think it's fine
to say that just the remaining space corresponds to the missing critical region.

### Double-checking the algorithm accuracy

I believe I had to make a few minor tweaks to the constraints corresponding to certain operations.
The constraints detailed in the paper should be carefully compared to those in the implementation.
When there is conflict, the implementation should be considered the truth, and the paper should be
updated.

### Wrapping up the parameteric simplex code into a package

This should be pretty close to done.

### Using this on an Instrumental Variable example

I would like to include a bounds on P(b) in the graph a -> b -> c, b <-> c, where c in unobserved.
This model is discussed in the measurement error paper with Roy, and I believe the Instrumental
Programs package can produce the corresponding linear program, which includes parameters throughout
the program. Hopefully the critical regions in this case will be simpler.
