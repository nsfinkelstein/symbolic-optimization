\documentclass{article}
\usepackage{amsmath}

\title{Towards a Constrained Optimization Approach to Measurement Error}

\begin{document}
\maketitle

NOTE: one thought is to have c, A, and b all capable of containing symbolic
elements. However, these symbolic elements themselves have constraints, i.e.
cannot take arbitrary values. Then we have a symbolic branching version of the
simplex algorithm, where we branch every time the result of a decision rule
depends on the values of the symbols.

In general this will be faster than vertex enumeration approaches, as the
restrictions on the symbolic c, A, and b will rule out many of the vertices that
would otherwise require costly enumeration.

NOTE: Think more about whether delta moves can be translated from stochastic
matrices to arbitrary linear programs. I am not hopeful for this, especially for
symbolic A.



\subsubsection*{Problem Description}

We consider a general measurement error problem of the following form. We let
$\bf X$ represent unobserved variables of known cardinality, $\bf Y$ represent
observed variables, and $\bf U$ represent unobserved variables of unknown
cardinality. Let $\bf V = X \cup Y \cup U$, and $\mathcal G$ be a directed
acyclic graph with vertices corresponding to $\bf V$.

We are interested in making inference about the $P({\bf X = x})$, which is
never point-identified from observed data. Assuming all variables have finite
cardinality, we pose this problem as a constrained optimization problem. The
objective can be written:

\[\sum_{\bf y, u} P({\bf X = x, Y = y, U = u})}\]

The constraints from observed data can be written:

\[\sum_{\bf x, u}P({\bf X = x, Y, U = u}) = P({\bf Y})\]

Finally, the constraints from the model are more complicated, and in general
will not be linear in the parameters of the full distribution. We note that
partially identified targets, and their constraints, in causal inference and
missing data can be similarly expressed.

Our goal is to characterize problems in the domain of measurement error, causal
inference, and missing data for which sharp bounds can be symbolically obtained
through identification of points that obey the Karush-Kuhn-Tucker first order
condition. A ``problem'' in this setting is comprised of a causal graph and
target of inference.

If the some relevant constraint qualification conditions holds everywhere in the
feasible region of the parameters - which I believe it may for a broad class of
relevant problems - all local optima of the objective function will satisfy the
KKT conditions. In combination with second-order tests, this will suffice to
fully identify the feasible region of the objective.

\subsubsection*{Example}

We consider the model $A \rightarrow X \rightarrow Y$, where $A$, $Y$ are
observed, and we would like to infer the distribution of $X$. For notational
convenience, we define\newline $\phi_{axy} := P(A = a, X = x, Y = y)$ and $\alpha_{ay} := P(A
=a, Y =y)$, so that $\alpha_{ay}$ is known. Then the objective is:

\[P(X = x) = \sum_{a,y} \phi_{axy}\]

Constraints imposed by the observed distribution are:

\[\forall a, y ~~\sum_x \phi_{axy} = \alpha_{ay}\]

Constraints imopsed by probability theory are:

\[\forall a, x, y ~~ 0 \le \phi_{axy} \le 1\]

Finally, the independence constraints imposed by the model can be represented:

\begin{align*}
  \label{eq:a}
  &\forall a, x, y ~~ \frac{\phi_{axy}}{\sum_{a',y'} \phi_{a'xy'}} =
    \frac{\sum_{a'} \phi_{a'xy}}{\sum_{a',y'} \phi_{a'xy'}}\frac{\sum_{y'} \phi_{axy'}}{\sum_{a',y'} \phi_{a'xy'}}\\
  \nonumber
  \implies
  &\forall a, x, y ~~ \phi_{axy}\sum_{a',y'} \phi_{a'xy'} -
  (\sum_{a'} \phi_{a'xy}) (\sum_{y'} \phi_{axy'}) = 0
\end{align*}

Our goal is to find the feasible region of the objective subject to the
constraints. It is not obvious whether this can be accomplished symbolically.

\section{Example: Bounds on Distribution under Measurement Error}

In this section, we use the modified simplex algorithm to derive symbolic bounds
on the distribution of the unobserved variable $X$ under the model in Fig.
\ref{fig:iv}, when $A, X,$ and $Y$ are binary random variables.

\begin{figure}[h]
  \centering
  \begin{tikzpicture}[>=stealth, node distance=1.0cm]
    \tikzstyle{vertex} = [draw, thick, minimum size=4.0mm, inner sep=1pt]
    \tikzstyle{edge} = [->, blue, very thick]

    \node[vertex, circle] (a) {$A$};
    \node[vertex, circle, gray] (x) [right of=a] {$X$};
    \node[vertex, circle] (y) [right of=x] {$Y$};
    \node[vertex, circle, red, opacity=.7] (u) [above of=x, xshift=0.5cm] {$U$};

    \draw[edge] (a) to (x);
    \draw[edge] (x) to (y);
    \draw[edge, red, opacity=.7] (u) to (x);
    \draw[edge, red, opacity=.7] (u) to (y);
  \end{tikzpicture}
  \caption{
    A and Y are observed. X is unobserved but with known cardinality; U is an
    unobserved confounder.
  }
  \label{fig:iv}
\end{figure}

It is known that all constraints implied by the model described by Fig.
\ref{fig:iv} can be expressed linearly on the conditional probabilities $P(X, Y
\mid A)$. By assumption, we observe the distribution $P(A, Y)$.

For notational convenience, we let $\psi_{xy \mid a} = P(X = x, Y = y \mid A
=a)$, and $\phi_{ay} = P(A = a, Y = y)$. The linear program that can be used to
bound $P(X = 0)$ can then be expressed, in slack form, as follows.

\begin{align*}
  \text{objective:~~}&
  (\phi_{00} + \phi_{01})(\psi_{00\mid 0} + \psi_{01\mid 0}) +
  (\phi_{10} + \phi_{11})(\psi_{00\mid 1} + \psi_{01\mid 1})
  \\
  \text{constraints:~~}
  &(\phi_{00} + \phi_{01}) (\psi_{00\mid 0} + \psi_{10\mid 0}) = \phi_{00}\\
  &(\phi_{00} + \phi_{01}) (\psi_{01\mid 0} + \psi_{11\mid 0}) = \phi_{01}\\
  &(\phi_{10} + \phi_{11}) (\psi_{00\mid 1} + \psi_{10\mid 1}) = \phi_{10}\\
  &(\phi_{10} + \phi_{11}) (\psi_{01\mid 1} + \psi_{11\mid 1}) = \phi_{11}\\
  &\psi_{00\mid 0} + \psi_{01\mid 1} + s_1 = 1 \\
  &\psi_{00\mid 1} + \psi_{01\mid 0} + s_2 = 1 \\
  &\psi_{10\mid 0} + \psi_{11\mid 1} + s_3 = 1 \\
  &\psi_{10\mid 1} + \psi_{11\mid 0} + s_4 = 1\\
  &\psi_{xy\mid a} \ge 0, ~~~s_i \ge 0 & \forall x, y, a, i\\
\end{align*}

In this formulation, it is possible to rewrite the LP such that $A$ is not a
function of $\phi$, but in general that will not always be possible. Due to the
fact that $\phi_{ay}$ represents a probability distribution, we know that it
must satisfy the following constraints.

\begin{align*}
  \sum_{ay} \phi_{ay} &= 1 &\\
  \phi_{ay} &\ge 0 &\forall a, y
\end{align*}

To find basic feasible solutions, and the corresponding constraints on $\phi$
for which they are valid, we 


\end{document}