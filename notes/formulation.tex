\documentclass[letterpaper]{article}

\usepackage[margin=1in]{geometry}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{amsmath}       % hyperlinks
\usepackage{amsthm}       % hyperlinks
\usepackage{todo}
\usepackage{amssymb}       % hyperlinks
\usepackage{hyperref}       % hyperlinks
\usepackage{tikz}
\usepackage{tabu}
\usepackage{bbm}
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{subfig}
\usepackage{booktabs}
\usepackage{microtype}      % microtypography

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{algorithm}
\usepackage{algorithmicx}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{corollary}{Corollary}
\newtheorem{proposition}{Proposition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}

\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\title{Problem Formulation}

\begin{document}
\maketitle

We begin with the following system of equations, where $p_{yx.a} = P(Y = y, X =
x \mid A = a)$.

\begin{align}
  p_{00.0} &= q_{00} + q_{01} + q_{10} + q_{11}\\
  p_{01.0} &= q_{20} + q_{22} + q_{30} + q_{32}\\
  p_{10.0} &= q_{02} + q_{03} + q_{12} + q_{13}\\
  p_{11.0} &= q_{21} + q_{23} + q_{31} + q_{33}\\
  p_{00.1} &= q_{00} + q_{01} + q_{20} + q_{21}\\
  p_{01.1} &= q_{10} + q_{12} + q_{30} + q_{32}\\
  p_{10.1} &= q_{02} + q_{03} + q_{22} + q_{23}\\
  p_{11.1} &= q_{11} + q_{13} + q_{31} + q_{33}
\end{align}

We first solve for one of the variables in each of the first four equations.

\begin{align}
  q_{00} &= p_{00.0} - q_{01} - q_{10} - q_{11}\\
  q_{22} &= p_{01.0} - q_{20} - q_{30} - q_{32}\\
  q_{12} &= p_{10.0} - q_{03} - q_{02} - q_{13}\\
  q_{31} &= p_{11.0} - q_{23} - q_{21} - q_{33}
\end{align}

We then substitute these values into the last four equations:

\begin{align}
  p_{00.1} &= p_{00.0} - q_{10} - q_{11} + q_{20} + q_{21}\\
  p_{01.1} &= q_{10} + p_{10.0} - q_{03} - q_{02} - q_{13} + q_{30} + q_{32}\\
  p_{10.1} &= q_{02} + q_{03} + p_{01.0} - q_{20} - q_{30} - q_{32} + q_{23}\\
  p_{11.1} &= q_{11} + q_{13} + p_{11.0} - q_{23} - q_{21}
\end{align}

We now solve for one variable at a time, beginning with $q_{10}$. We substitute
this into the second half of the system, but we wait to substitute it into the
first half.

\begin{align}
  q_{10} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21}\\
  p_{01.1} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21} + p_{10.0} - q_{03} - q_{02} - q_{13} + q_{30} + q_{32}\\
  p_{10.1} &= q_{02} + q_{03} + p_{01.0} - q_{20} - q_{30} - q_{32} + q_{23}\\
  p_{11.1} &= q_{11} + q_{13} + p_{11.0} - q_{23} - q_{21}
\end{align}

Next we solve for $q_{02}$

\begin{align}
  q_{10} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21}\\
  q_{02} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21} + p_{10.0} - q_{03} - p_{01.1} - q_{13} + q_{30} + q_{32}\\
  p_{10.1} &= p_{00.0} - p_{00.1} - q_{11} + q_{21} + p_{10.0} - p_{01.1} - q_{13} + p_{01.0} + q_{23}\\
  p_{11.1} &= q_{11} + q_{13} + p_{11.0} - q_{23} - q_{21}
\end{align}

Next we solve for $q_{23}$

\begin{align}
  q_{10} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21}\\
  q_{02} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21} + p_{10.0} - q_{03} - p_{01.1} - q_{13} + q_{30} + q_{32}\\
  q_{23} &= -p_{00.0} + p_{00.1} + q_{11} -  q_{21} - p_{10.0} + p_{01.1} + q_{13} - p_{01.0} + p_{10.1}\\
  p_{11.1} &= p_{11.0} + p_{00.0} - p_{00.1} +  p_{10.0} - p_{01.1} + p_{01.0} - p_{10.1}
\end{align}

We chose this order of operations because each newly solved-for variable need
only be substituted into the following equation. Note that the final equation is
now a tautology, and we can drop it from the system. We now substitute the
values for these three variables back into the first four equations to obtain
the following system:

\begin{align}
  q_{00} &= p_{00.0} - q_{01} - p_{00.0} + p_{00.1} - q_{20} - q_{21}\\
  q_{22} &= p_{01.0} - q_{20} - q_{30} - q_{32}\\
  q_{12} &= p_{10.0} - p_{00.0} + p_{00.1} + q_{11} - q_{20} - q_{21} - p_{10.0} + p_{01.1} - q_{30} - q_{32}\\
  q_{31} &= p_{11.0} + p_{00.0} - p_{00.1} - q_{11} + p_{10.0} - p_{01.1} - q_{13} + p_{01.0} - p_{10.1} - q_{33}\\
  q_{10} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21}\\
  q_{02} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21} + p_{10.0} - q_{03} - p_{01.1} - q_{13} + q_{30} + q_{32}\\
  q_{23} &= -p_{00.0} + p_{00.1} + q_{11} -  q_{21} - p_{10.0} + p_{01.1} + q_{13} - p_{01.0} + p_{10.1}
\end{align}

Simplifying the constants, we obtain

\begin{align}
  q_{00} &=  p_{00.1} - q_{01}  - q_{20} - q_{21}\\
  q_{22} &= p_{01.0} - q_{20} - q_{30} - q_{32}\\
  q_{12} &= p_{01.1} - p_{00.0} + p_{00.1} + q_{11} - q_{20} - q_{21} - q_{30} - q_{32}\\
  q_{31} &= p_{11.1} - q_{11} - q_{13} - q_{33}\\
  q_{10} &= p_{00.0} - p_{00.1} - q_{11} + q_{20} + q_{21}\\
  q_{02} &= p_{00.0} - p_{00.1} + p_{10.0} - p_{01.1} - q_{11} + q_{20} + q_{21} - q_{03} - q_{13} + q_{30} + q_{32}\\
  q_{23} &=  p_{11.0} - p_{11.1} + q_{11} -  q_{21} + q_{13}.
\end{align}


\end{document}

