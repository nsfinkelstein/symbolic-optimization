\documentclass[twoside]{article}

\usepackage{aistats2024}
\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{dirtree}
\newtheorem{theorem}{Theorem}

% If you use natbib package, activate the following three lines:
\usepackage[round]{natbib}
\renewcommand{\bibname}{References}
\renewcommand{\bibsection}{\subsubsection*{\bibname}}


% If your paper is accepted, change the options for the package
% aistats2022 as follows:
%
%\usepackage[accepted]{aistats2024}
%
% This option will print headings for the title of your paper and
% headings for the authors names, plus a copyright note at the end of
% the first column of the first page.

% If you set papersize explicitly, activate the following three lines:
%\special{papersize = 8.5in, 11in}
%\setlength{\pdfpageheight}{11in}
%\setlength{\pdfpagewidth}{8.5in}

% If you use natbib package, activate the following three lines:
%\usepackage[round]{natbib}
%\renewcommand{\bibname}{References}
%\renewcommand{\bibsection}{\subsubsection*{\bibname}}

% If you use BibTeX in apalike style, activate the following line:
%\bibliographystyle{apalike}

\begin{document}

% If your paper is accepted and the title of your paper is very long,
% the style will print as headings an error message. Use the following
% command to supply a shorter title of your paper so that it can be
% used as headings.
%
%\runningtitle{I use this title instead because the last one was very long}

% If your paper is accepted and the number of authors is large, the
% style will print as headings an error message. Use the following
% command to supply a shorter version of the authors names so that
% they can be used as headings (for example, use only the surnames)
%
%\runningauthor{Surname 1, Surname 2, Surname 3, ...., Surname n}

% Supplementary material: To improve readability, you must use a single-column format for the supplementary material.
\onecolumn
\aistatstitle{
    Supplement
}

\section{PROOF OF THEOREM 1}

The original simplex algorithm has a number of conditional statements, where the conditions are
functions of the coefficients of the programs. Because in our case, the coefficients of the programs
are themselves functions of the parameters, those predicates might evaluate to \texttt{true} for
some parameter values and \texttt{false} for others. Our goal, then, is to partition the space of
parameters such that all relevant predicates evaluate to the same value Boolean value for every
parameter value in each partition. This allows us to produce program manipulations that are not
functions of the coefficients of the program, but are instead associated with a specific parameter
subspace. We then execute the relevant program manipulation ``in'' each parameter subspace, i.e.
we execute the manipulation and associate it with the relevant subspace.

In that sense, we can conceptually think of the operations involved in the parametric simplex
algorithm as transforming each predicate into the corresponding parameter subspace. We can prove
correctness, therefore, by observing that for each value in each parameter subspace, all
conditionals evaluate the same way, and lead to a series of operations identical to the program
manipulations described in the main body of the paper. This framing may clarify the approach of the
proof.

We consider each operation set $\Delta_t$, $\Delta_a$, $\Delta_s$, and $\Delta_r$ in turn. We show
that each set has two properties. First, we show that the parameter subspaces of the operations in
the set form a partition of the parameter space. Second, we show that for each operation $\delta$ in
the set, the simplex algorithm executed on a numerical program $\mathcal L(\phi)$ for any $\phi \in
\delta.\Phi$ would enact the program manipulation $\delta.\gamma$. To avoid reproducing well known
results on the simplex algorithms, we rely on proofs presented in other sources as appropriate.

We begin by considering $\Delta_s$. We reproduce the subspace definitions from the main text for
convenience.
%
\begin{align*}
    &\delta_{\texttt{unbounded}}.\Phi =
    \{
        \phi:
            \exists_{i \in \mathcal N}~ c_{\phi,i} < 0~ \land~ \forall_{j \in \mathcal B}~ A_{\phi, j, i} \le 0
    \}
    \\
    &\delta_{\texttt{optimal}}.\Phi =
    \{
        \phi: \forall_{i \in \mathcal N}~ c_{\phi, i} \ge 0
    \}
    \\
    &\delta_{\texttt{pivot(i, j)}}.\Phi =
    \delta_{\texttt{unbounded}}.\Phi \setminus
    \\
    &\hspace{0.25in}
    \{
        \phi:
            \big(
                c_{\phi, i} < 0 \land \forall_{k < i}~ c_{\phi, k} \ge 0
            \big)
            \land
            \big(
                A_{\phi, i, j} > 0
                \land
                \big(
                    \forall_{k \in \mathcal B}~
                    \frac{b_{\phi, j}}{A_{\phi, i, j}} \le \frac{b_{\phi, k}}{A_{\phi, i, k}}
                    \land
                    \forall_{k > j \in \mathcal B}~
                    \frac{b_{\phi, j}}{A_{\phi, i, j}} < \frac{b_{\phi, k}}{A_{\phi, i, k}}
                \big)
            \big)
    \}
\end{align*}
%
To see that these parameter subspaces form a partition, we first note that any $\phi$ leading to
$c_{\phi,i} \ge 0$ for all $i \in \mathcal N$ will fall only into $\delta_{\texttt{optimal}}.\Phi$.
Next, any $\phi$ that does not meet that condition, so by definition there is at least one value of
$i \in \mathcal N$ for which $c_{\phi, i} < 0$. For any such $i$, if $A_{\phi, j, i} \le 0$ for all
$j \in \mathcal B$, then the corresponding $\phi$ will fall into $\delta_{\texttt{unbounded}}.\Phi$.
Otherwise, it is explicitly excluded from all pivot operations's subspaces. Finally, we observe
that the remainder of the space is covered by the pivot subpsaces. First, to see that the pivot
subspaces are mutually exclusive, observe that each $\phi$ falls into a pivot subspace in which $i$
is strictly the least index for which $c_{\phi, i}$ is negative. Note that there must be some
$A_{\phi, i, j} > 0$, because otherwise $\phi \in \delta_{\texttt{unbounded}}.\Phi$. Then note that
$\phi$ falls into the pivot subspace such that the index $j$ is the smallest index for which the
ratio $\frac{b_{\phi, k}}{A_{\phi, i, k}}$ takes its lowest possible value over $k \in \mathcal B$.
Each $\phi$ must fall into only one subspace, because only one value for each of $i$ and $j$ can be
the lowest value for which corresponding conditions hold.

We now observe that the corresponding program manipulations are correct for every parameter value in
the corresponding subspaces. For all values $\phi \in \delta_{\texttt{unbounded}}.\Phi$, the value
of $x_i$ can be made arbitrarily large without violation of constraints; increasing $x_i$ only
serves to increase other variables as well. Because $c_i < 0$, the objective can be arbitrarily
decreased. For all $\phi \in \delta_{\texttt{optimal}}.\Phi$, optimality follows from Theorem
{29.10} in \cite{clrs}; by construction, $\mathcal L(\phi)$ satisfies the optimality conditions
described therein. Similarly, by construction, $\mathcal L(\phi)$ for each $\phi \in
\delta_{\texttt{pivot(i, j)}}.\Phi$ satisfies the conditions under which Bland's rule
\citep{bland1977bland} requires that $j$ be pivoted out of the basis for $i$.

Next we consider $\Delta_a$. Again, we reproduce the relevant subspaces here.
%
\begin{align*}
    &\delta_{\texttt{feasible}}.\Phi =
    \{
        \phi: \forall_{i}~ b_{\phi,i} \ge 0
    \}
    \\
    &\delta_{\texttt{auxiliary(k)}}.\Phi =
    \{
        \phi: b_{\phi,k} < 0
        \land
        \forall_{j} b_{\phi,k} \le b_{\phi,j}
        \land
        \forall_{j<k} b_{\phi,k} < b_{\phi,j}
    \}
\end{align*}
%
It is easy to see that these subspaces form a partition. We employ a strategy similar to Bland's
rule, in the sense that each $\phi$ that does not lead to $b_{\phi,i} \ge 0$ for all $i$ belongs to
$\delta_{\texttt{auxiliary(k)}}.\Phi$ corresponding to the smallest index $k$ for which $\b_{\phi}$
takes its least value.

By construction, $\mathcal L(\phi)$ has a basic feasible solution for all
$\phi \in \delta_{\texttt{feasible}}.\Phi$. In particular, neither the equality constraints nor the
non-negativity constraints can be violated for any such $\phi$ at the basic solution. Otherwise, the
standard simplex procedure requires that we pivot the auxiliary variable into the basis in favor of
any basic variable corresponding to a negative right hand side. By construction, this requirement is
satisfied for any $\phi$ in any $\delta_{\texttt{auxiliary(k)}}.\Phi$ (the additional constraints
are necessary to ensure that the partition property is maintained).

Next we consider $\Delta_r$.
%
\begin{align*}
    &\delta_{\texttt{infeasible}}.\Phi =
    \{
        \phi: x^{basic}_{\phi, 0}(\mathcal L) > 0
    \}
    \\
    &\delta_{\texttt{recover(k)}}.\Phi =
    \{
        \phi:
        x^{basic}_{\phi, 0}(\mathcal L) = 0  % make sure auxiliary program indicates feasibility
        \land
        A_{\phi,0,k} \neq 0
        \land
        \forall_{j < k} A_{\phi, 0, j} = 0
    \}
\end{align*}
%
It is clear these subspaces form a partition. Again, we partition the space according to the
smallest index in $\mathcal L(\phi)$ that meets a certain condition, in this case $A_{\phi,0,k} \neq
0$. Correctness for all operations follows from Lemma {29.12} in \cite{clrs}. Note that the
tabularization procedure leaves untouched programs rendered \texttt{feasible} by the previous
iteration of the parametric simplex algorithm.

Finally, we consider $\Delta_t$, which is made up of operations corresponding to the following
subspaces:
%
\begin{align*}
    \delta_{\texttt{tabularize(B, R)}}.\Phi =
    \{
        \phi:
            det(A'_{\phi,R,B}) \neq 0
            \land
            \forall_{R \prec R', B \prec B'}
            det(A'_{\phi,R',B'}) = 0
    \}.
\end{align*}
%
To see that these subspaces are mutually exclusive, we note that each subspace includes only the
largest sets $R$ and $B$ for which $det(A'_{\phi,R,B}) \neq 0$, according to the ordering described
in the main paper. To see that these sets space the full parameter space, we note that all matrices
with one row have nonzero determinant.

To show correctness, we observe that the tabularization procedure in the main body of the paper
corresponds exactly to the steps taken to convert a numerical linear program from standard for to
tableau form for appropriate choices of $R$ and $B$. To verify that the selection of $R$ and $B$ are
appropriate, we observe that due to the requirement that $R \prec R'$ implies $|R| \le |R'|$, for
each value $\phi$, the corresponding values of $|R|$ and $|B|$ will correspond to a matrix of equal
rank to $A'_{\phi}$, ensuring that no program constraints are discarded.


\section{ADDITIONAL PARAMETRIC SIMPLE EXAMPLE}

\begin{figure*}[h!]
\vspace{-0.33in}
{\footnotesize
\begin{align*}
  \textbf{Ex. 3:}
  ~\texttt{optimize~}
  &\phi_1 x_1 + x_2&\\
  \texttt{subject to~}
  &\phi_2 = -x + x_2 + x_3 \\
  &1 = x_1 - \phi_3 x_2 + x_4 \\
  &\Phi = \{\phi \mid -5 \le \phi_i \le 5 \text{~ for all ~} i\}.\\
  % \vspace{0.25in}
  % \textbf{Ex. 4:}
  % ~\texttt{optimize~}
  % &(\phi_{0 \mid 1} - \phi_{0 \mid 0})(x_1 + x_5) +
  %   (\phi_{1 \mid 1} - \phi_{1 \mid 0})(x_2 + x_6)
  %  \\
  % \texttt{subject to~}
  % &\phi_{00} = x_0 +
  % \phi_{0 \mid 0}x_2 +
  % \phi_{1 \mid 0}x_1\\
  % &\phi_{01} = x_3 +
  %   \phi_{0 \mid 0}x_1 +
  %   \phi_{1 \mid 0}x_2\\
  % &\phi_{10} = x_4 +
  %   \phi_{0 \mid 1}x_6 +
  %   \phi_{1 \mid 1}x_5\\
  % &\phi_{11} = x_7 +
  %   \phi_{0 \mid 1}x_5 +
  %   \phi_{1 \mid 1}x_6\\
  % &\Phi = \{\phi \mid \sum_{ij} \phi_{ij} = 1,
  %   \sum_{i}\phi_{i \mid j} = 1 ~~\forall j, ~~\phi > 0\}.
\end{align*}
}
\vspace{-0.45in}
\end{figure*}

In this section, we present an additional example of the output of the parametric simplex algorithm.
Unlike the two examples explored in the main body of the paper, this solution includes regions of
the parameter space for which the program is unbounded and infeasible. This example was first
introduced by \cite{li2007plp}, in which an approximate solution was provided, and also addressed in
\cite{charitopoulos2017plp}, in which an exact solution was provided for critical regions in which
the program is feasible and bounded. Our solution matches their solution, while also characterizing
regions for which the program is infeasible or unbounded. This program appears as Example {1} in the
table detailing algorithm running times.
\newpage
\dirtree{%
.1 /.
.2 $\phi_2 \ge 0$.
.3 $\phi_1 \ge 0$.
.4 \texttt{opt:}$0$.
.3 $\phi_1 < 0$.
.4 $\phi_1\phi_3 \ge -1$.
.5 \texttt{opt:}$\phi_1$.
.4 $\phi_1\phi_3 < -1$.
.5 $\phi_3 \ge 1$.
.6 \texttt{unbounded}.
.5 $\phi_3 < 1$.
.6 \texttt{opt:}$\frac{\phi_1\phi_2\phi_3 \phi1 + \phi_2 + 1}{1 - \phi_3}$.
.2 $\phi_2 < -1$.
.3 $\phi_3 > 1$.
.4 $\phi_1\phi_3 < -1$.
.5 \texttt{unbounded}.
.4 $\phi_1\phi_3 \ge -1$.
.5 \texttt{opt:}$\frac{\phi_1\phi_2\phi_3 \phi1 + \phi_2 + 1}{1 - \phi_3}$.
.3 $\phi_3 \le 1$.
.4 \texttt{infeasible}.
.2 $-1 \le \phi_2 < 0$.
.3 $\phi_1 \ge 0$.
.4 \texttt{opt:}$-\phi_1\phi_2$.
.3 $\phi_1 < 0$.
.4 $\phi_1\phi_3 \ge 1$.
.5 \texttt{opt:}$\phi_1$.
.4 $\phi_1\phi_3 < 1$.
.5 $\phi_3 \ge 1$.
.6 \texttt{unbounded}.
.5 $\phi_3 < 1$.
.6 \texttt{opt:}$\frac{\phi_1\phi_2\phi_3 \phi1 + \phi_2 + 1}{1 - \phi_3}$.
}

\bibliographystyle{plainnat}
\bibliography{references}

\end{document}
