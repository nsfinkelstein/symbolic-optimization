import z3
import copy
import numpy as np
from functools import partial
from datatypes import Operation, Tableau, AuxStatus
from pivot_operations import pivot

def auxiliary_ops(program, status):
    # import IPython; IPython.embed(colors='neutral')
    feasible_constraints = z3.And(*[element >= 0 for element in program.b])
    feasible_op = Operation(
        alteration=lambda program, status: (copy.deepcopy(program), 'incomplete'),
        constraints=feasible_constraints,
        name='skip_auxiliary_already_feasible',
    )

    construction_ops = [
        Operation(
            alteration=partial(make_auxiliary, leaving=leaving),
            constraints=z3.And(
                z3.Not(feasible_constraints),
                auxiliary_constraints(copy.deepcopy(program), leaving)
            ),
            name=f'create_auxiliary_with_var_{leaving}_leaving',
        )
        for leaving in program.basis
    ]

    return [feasible_op] + construction_ops


def make_auxiliary(program, status, leaving):
    a, b, c, nu, basis, nonbasis = program
    assert len(nonbasis) == (max(nonbasis.values()) + 1)
    tableau = Tableau(
        a=np.hstack((a, -np.ones((a.shape[0], 1)))),
        b=b,
        c=np.array([0 for _ in c] + [-1], dtype=np.object_),
        nu=nu,
        basis=basis,
        nonbasis=nonbasis | {-1: max(nonbasis.values()) + 1},
    )

    program, _ = pivot(program=copy.deepcopy(tableau), status=status, entering=-1, leaving=leaving)
    return program, AuxStatus(
        c=c, nu=nu, complete=False, original_nonbasis=nonbasis.copy()
    )


def auxiliary_constraints(program, leaving):
    a, b, _, _, basis, nonbasis = program
    i = basis[leaving]
    return z3.And(
        b[i] < 0,  # unneeded bcause of exclusion of feasilbe constraints
        *[b[i] < b[k] for var, k in basis.items() if var < leaving],
        *[b[i] <= b[k] for var, k in basis.items() if var > leaving],
    )
