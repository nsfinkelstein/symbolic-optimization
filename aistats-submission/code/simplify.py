import sympy
from sympy.core.relational import Eq, LessThan, GreaterThan, StrictLessThan, StrictGreaterThan

import z3
# p0, p1, p2 = z3.Real('p0'), z3.Real('p1'), z3.Real('p2')
# parameter_space = z3.And(
#     z3.RealVal(0) <= p0, p0 <= z3.RealVal(6),
#     z3.RealVal(0) <= p1, p1 <= z3.RealVal(1.5),
#     z3.RealVal(-50000) <= p2, 2 <= z3.RealVal(50000),
# )
# ps = sympy.sympify(str(parameter_space))

# TODO: this module will be used to simplify critical regions overall, as well as the individual
# decisions made at each node to create a legible decision tree


# is it currently used to simplify the object value corresponding to each critical region
# as well as the critical regions themselves, though improvements can be made


def simplify_sympy(e):
    if not isinstance(e, sympy.Basic):
        return e

    # Tradeoff between speed and concision
    r = s(e.simplify().simplify()).simplify().simplify()

    unctions = {sympy.And, sympy.Or}
    if type(r) in unctions and not any(type(u) in unctions for a in r.args for u in a.args):
        r = sympy.to_cnf(r.simplify(), simplify=True).simplify()
    return r

    return e.simplify().simplify().simplify()
    return sympy.to_cnf(e.simplify(), simplify=True, force=True).simplify()


def s(e):
    print(e)
    if not isinstance(e, sympy.Basic):
        return e

    if len(e.args) == 0:
        return e.simplify()

    if len(e.args) == 1:
        return s(e.simplify()).simplify()

    rels = {Eq, LessThan, GreaterThan, StrictLessThan, StrictGreaterThan}
    # if type(e) in rels | {sympy.And, sympy.Or, sympy.Not}:
    #     simplified = sympy.And(ps, sympy.Not(e)).simplify().simplify().simplify()
    #     if not simplified:
    #         return sympy.logic.boolalg.BooleanTrue()
    #
    if type(e) in rels:
        if len(e.free_symbols) > 1:
            try:
                return sympy.solve(e, e.free_symbols.pop()).simplify()
            except NotImplementedError:
                pass

    return type(e)(*[s(a.simplify()).simplify() for a in e.args]).simplify()


def round_sympy(expr: sympy.Expr):
    #expr = r(expr)
    expr = (
        expr
        .simplify()
        .xreplace(
            sympy.core.rules.Transform(
                lambda x: x.evalf(),
                lambda x: hasattr(x, 'evalf'),
            )
        )
        .xreplace(
            sympy.core.rules.Transform(
                lambda x: x.round(7),
                lambda x: isinstance(x, sympy.Float),
            )
        )
        .xreplace(
            sympy.core.rules.Transform(
                lambda x: x.round(5),
                lambda x: isinstance(x, sympy.Float),
            )
        )
        .simplify()
    )
    print('\n\n', expr, '\n\n')
    return expr


