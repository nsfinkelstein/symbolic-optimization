import z3
import copy
import numpy as np
from conversion import z3_to_sympy
from functools import partial
from itertools import product
from datatypes import Operation, Tableau, AuxStatus


def pivot_ops(program, status):
    unbounded_constraints = z3.Or(*[
        z3.And(
            program.c[col] > 0,
            *[
                program.a[row, col] <= 0
                for row in range(program.a.shape[0])
            ]
        )
        for col in range(program.a.shape[1])
    ])

    unbounded_op = Operation(
        alteration=lambda program, status: (
            copy.deepcopy(program),
            update_complete_status(status, 'unbounded'),
        ),
        constraints=unbounded_constraints,
        name='mark_unbounded',
    )

    optimal_constraints = z3.And(*[element <= 0 for element in program.c.flatten()])
    optimal_op = Operation(
        alteration=lambda program, status: (
            copy.deepcopy(program),
            update_complete_status(status, 'optimal'),
        ),
        constraints=optimal_constraints,
        name='optimal: ' + str(z3_to_sympy(program.nu.item()))
    )

    execute_pivot_ops = [
        Operation(
            alteration=partial(pivot, entering=entering, leaving=leaving),
            constraints=z3.And(
                z3.Not(unbounded_constraints),
                pivot_constraints(program=program, entering=entering, leaving=leaving),
            ),
            name=f'pivot_{entering}_in_and_{leaving}_out',
        )
        for entering, leaving in product(program.nonbasis, program.basis)
    ]

    return [unbounded_op, optimal_op] + execute_pivot_ops


def update_complete_status(status, replacement):
    if isinstance(status, AuxStatus):
        return AuxStatus(
            c=status.c,
            nu=status.nu,
            complete=True,
            original_nonbasis=status.original_nonbasis
        )
    return replacement


def pivot(program: Tableau, status: str, entering: int, leaving: int):
    # basis and nonbasis are dictionaries that map from variable number to index
    a, b, c, nu, basis, nonbasis = copy.deepcopy(program)
    rows, cols = (set(range(size)) for size in a.shape)

    # see CLRS p.869, this is a line-by-line implementation
    a_hat, b_hat, c_hat, nu_hat = [np.zeros_like(array, dtype=np.object_) for array in program[:4]]

    # assign variables to new homes for easier reference
    basis[entering] = basis[leaving]
    nonbasis[leaving] = nonbasis[entering]

    for j in cols - {nonbasis[entering]}:
        a_hat[basis[entering], j] = a[basis[leaving], j] / a[basis[leaving], nonbasis[entering]]
    a_hat[basis[entering], nonbasis[leaving]] = 1 / a[basis[leaving], nonbasis[entering]]

    b_hat[basis[entering]] = b[basis[leaving]] / a[basis[leaving], nonbasis[entering]]
    for i in rows - {basis[leaving]}:
        b_hat[i] = b[i] - (a[i, nonbasis[entering]] * b_hat[basis[entering]])

        for j in cols - {nonbasis[entering]}:
            a_hat[i, j] = a[i, j] - (a[i, nonbasis[entering]] * a_hat[basis[entering], j])

        a_hat[i, nonbasis[leaving]] = -(
            a[i, nonbasis[entering]] * a_hat[basis[entering], nonbasis[leaving]]
        )

    nu_hat = np.array(nu + (c[nonbasis[entering]] * b_hat[basis[entering]]), dtype=np.object_)
    for j in cols - {nonbasis[entering]}:
        c_hat[j] = c[j] - (c[nonbasis[entering]] * a_hat[basis[entering], j])
    c_hat[nonbasis[leaving]] = -(c[nonbasis[entering]] * a_hat[basis[entering], nonbasis[leaving]])

    # remove variables from old homes
    basis.pop(leaving)
    nonbasis.pop(entering)

    return (Tableau(a_hat, b_hat, c_hat, nu_hat, basis.copy(), nonbasis.copy()), status)


def pivot_constraints(program: Tableau, entering: int, leaving: int):
    a, b, c, nu, basis, nonbasis = program
    i, j = basis[leaving], nonbasis[entering]

    if (not isinstance(a[i, j], z3.ArithRef)) and a[i, j] <= 0:
        return False

    return z3.And(
        # objective coefficient is positive
        c[j] > 0,

        # no objective coefficient for earlier variable is positive
        *[c[k] <= 0 for var, k in nonbasis.items() if var < entering],

        # coefficient is positive
        a[i, j] > 0,

        # ratio is less than earlier variables, weakly less than later variables, with positive coef
        *[
            (
                (not isinstance(a[k, j], z3.ArithRef) and a[k, j] <= 0)
                or (
                    z3.Or(a[k, j] <= 0, (b[i] / a[i, j]) < (b[k] / a[k, j]))
                )
            )
            for var, k in basis.items()
            if var < leaving
        ],
        *[
            (
                (not isinstance(a[k, j], z3.ArithRef) and a[k, j] <= 0)
                or (
                    z3.Or(a[k, j] <= 0, (b[i] / a[i, j]) <= (b[k] / a[k, j]))
                )
            )
            for var, k in basis.items()
            if var > leaving
        ],
    )
