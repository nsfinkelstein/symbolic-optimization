import z3
import copy
import numpy as np
from functools import partial
from datatypes import Operation, Tableau, AuxStatus
from pivot_operations import pivot


def recovery_ops(program, status):
    # if the auxiliary variable is already nonbasic, there is only one recovery operations
    if -1 in program.nonbasis:
        return [
            Operation(
                alteration=partial(recover_program),
                constraints=True,
                name='recover_auxiliary_already_nonbasic',
            )
        ]

    # otherwise pivot it out before recovering if feasible
    elif -1 in program.basis:
        print('\nAUXVAR:', program.b[program.basis[-1]], '\n')
        infeasible_op = Operation(
            alteration=lambda program, status: (program, 'infeasible'),
            constraints=(program.b[program.basis[-1]] != 0),
            name='mark_infeasible',
        )

        recover_program_ops = [
            Operation(
                alteration=partial(recover_program, entering=entering),
                constraints=z3.And(
                    program.b[program.basis[-1]] == 0,
                    program.a[program.basis[-1], program.nonbasis[entering]] != 0,
                    *[
                        program.a[program.basis[-1], k] == 0
                        for var, k in program.nonbasis.items()
                        if var < entering
                    ]
                ),
                name=f'recover_auxiliary_pivot_in_{entering}',
            )
            for entering in program.nonbasis
        ]

        return [infeasible_op] + recover_program_ops

    else:
        raise RuntimeError('Auxiliary variable should always be in basis or nonbasis')


def recover_program(program: Tableau, status: AuxStatus, entering: int = None):
    if entering is not None:
        program, status = pivot(program, status, entering, -1)

    a, b, c, nu, basis, nonbasis = copy.deepcopy(program)
    a = np.delete(a, nonbasis[-1], axis=1)
    nonbasis = {k: v - int(v > nonbasis[-1]) for k, v in nonbasis.items()}
    nonbasis.pop(-1)

    print(basis, nonbasis)
    print(status.original_nonbasis)

    c = np.zeros_like(status.c, dtype=np.object_)
    nu = status.nu
    for i in status.original_nonbasis:
        if i in nonbasis:
            # use same coefficients from original cost function
            c[nonbasis[i]] += status.c[status.original_nonbasis[i]]

        elif i in basis:
            # substitute in equation for basis variable
            for j in nonbasis:
                c[nonbasis[j]] -= (status.c[status.original_nonbasis[i]] * a[basis[i], nonbasis[j]])

            nu += (status.c[status.original_nonbasis[i]] * b[basis[i]])

    return (Tableau(a, b, c, nu, basis, nonbasis), 'incomplete')
