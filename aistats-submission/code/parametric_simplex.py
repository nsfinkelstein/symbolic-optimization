import sys
import multiprocessing  # imported to populate sys cache
for k in {k for k in sys.modules if 'multiprocessing' in k}:
    del sys.modules[k]
    sys.modules[k] = __import__(k.replace('multiprocessing', 'multiprocess'))
# load concurrent futures with multiprocess instead of multiprocessing
import concurrent.futures as cf
import multiprocess as mp

import os
import z3
import copy
import random
import resource
from treelib import Tree
from typing import Optional, Union

from datatypes import StandardForm, Operation, Tableau, Node, AuxStatus
from pivot_operations import pivot_ops
from tabularize_operations import tabularize_ops, simple_tabularize_ops
from auxiliary_operations import auxiliary_ops
from recovery_operations import recovery_ops
from conversion import sympy_to_z3, z3_to_sympy, convert_program

resource.setrlimit(resource.RLIMIT_STACK, (2**29, -1))
sys.setrecursionlimit(10**6)

from concurrent.futures import Future, Executor
from threading import Lock


class DummyExecutor(Executor):

    def __init__(self):
        self._shutdown = False
        self._shutdownLock = Lock()

    def submit(self, fn, *args, **kwargs):
        with self._shutdownLock:
            if self._shutdown:
                raise RuntimeError('cannot schedule new futures after shutdown')

            f = Future()
            try:
                result = fn(*args, **kwargs)
            except BaseException as e:
                f.set_exception(e)
            else:
                f.set_result(result)

            return f

    def shutdown(self, wait=True):
        with self._shutdownLock:
            self._shutdown = True


def apply(node: Node, operation: Operation) -> Optional[Node]:
    # add past and new constraints to a solver, run the solver
    solver = z3.SolverFor('QF_NRA')
    solver.add(z3.parse_smt2_string(node.constraints))
    solver.add(z3.parse_smt2_string(operation.constraints))
    solution = solver.check()

    # if the parameter subspace is empty, we do not add a node to the queue
    if str(solution) == 'unsat':
        return None

    if not isinstance(node.program, StandardForm):
        assert len(node.program.basis) == node.program.a.shape[0]
        assert len(node.program.nonbasis) == node.program.a.shape[1]
        assert set(node.program.basis.values()) == set(range(node.program.a.shape[0]))
        assert set(node.program.nonbasis.values()) == set(range(node.program.a.shape[1]))

    # import IPython; IPython.embed(colors='neutral')
    program, status = operation.alteration(node.program, node.status)

    child = Node(
        program=copy.deepcopy(program),
        status=copy.deepcopy(status),
        constraints=solver.to_smt2(),
        op_constraints=operation.constraints,  # todo
        parent=node.id,
        id=random.randbytes(20),
    )
    return child, operation.name


def parametric_simplex(program: Union[Tableau, StandardForm], constraints: str) -> Tree:
    """ constraints should be in smt2 format """
    # TODO: build the tree
    # TODO: keep track of original variable locations
    program = convert_program(z3_to_sympy, program)

    if isinstance(program, Tableau):
        status = 'tabularized'
    elif isinstance(program, StandardForm):
        status = 'root'

    root_id = random.randbytes(20)
    root = Node(
        program=program,
        status=status,
        constraints=constraints,
        op_constraints=None,
        id=root_id
    )
    assert isinstance(constraints, str)

    tree = Tree()
    tree.create_node('root', root.id, data=root)

    ## EXECUTOR PARADIGM
    ## create jobs
    executor = cf.ProcessPoolExecutor(os.cpu_count() - 1)
    executor = DummyExecutor()  # For debugging

    jobs = {executor.submit(apply, root, op) for op in get_operations_for_node(root)}
    while True:
        if len(jobs) == 0:
            break

        completed = next(cf.as_completed(jobs))
        jobs.remove(completed)
        node = completed.result()
        if node is None:
            continue

        node, op_name = node
        tree.create_node(op_name, node.id, parent=node.parent, data=node)
        print(op_name)

        completed_status = {'optimal', 'unbounded', 'infeasible'}
        if isinstance(node.status, AuxStatus) or (node.status not in completed_status):
            jobs.update({executor.submit(apply, node, op) for op in get_operations_for_node(node)})

    return tree


def get_operations_for_node(node):

    if isinstance(node.program, StandardForm):
        ops = simple_tabularize_ops  # simpler tabularization; check performance differences
        ops = tabularize_ops
        print('TABULARIZE')

    elif node.status == 'tabularized':
        print('AUX')
        ops = auxiliary_ops

    elif (
        (isinstance(node.status, AuxStatus) and not node.status.complete)
        or (node.status == 'incomplete')
    ):
        print('PIVOT')
        ops = pivot_ops

    elif isinstance(node.status, AuxStatus) and node.status.complete:
        print('RECOVER')
        ops = recovery_ops

    all_operations = set()
    for op in ops(convert_program(sympy_to_z3, node.program), node.status):
        if isinstance(op.constraints, bool) and not op.constraints:
            continue

        solver = z3.SolverFor('QF_NRA')
        if isinstance(op.constraints, bool) and op.constraints:
            solver.add(True)
        else:
            solver.add(op.constraints)

        constraints = solver.to_smt2()
        assert isinstance(constraints, str)
        all_operations.add(Operation(alteration=op.alteration, constraints=constraints, name=op.name))

    return all_operations
