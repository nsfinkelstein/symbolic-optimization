from functools import cache
import z3
import sympy
import numpy as np


def z3_regions_to_string(regions):
    rs = []
    for region in regions:
        s = z3.Solver()
        s.add(region)
        rs.append(s.to_smt2())
    return rs


#@cache
def z3_to_sympy(z3_obj, simplify=True):
    # If the string representation of the z3 object evaluates, it must be numeric

    if isinstance(z3_obj, str):
        z3_obj = z3.And(*list(z3.parse_smt2_string(z3_obj)))

    if not isinstance(z3_obj, z3.AstRef):
        return z3_obj

    if str(z3_obj.decl()) == 'And' and len(z3_obj.children()) == 1:
        z3_obj = z3_obj.children()[0]

    z3.set_option(max_args=100000000, max_lines=10000000, max_depth=100000000, max_visited=10000000)
    z3_obj = str(z3.simplify(z3_obj))
    try:
        result = sympy.sympify(str(z3_obj))
        if simplify:
            return sympy.simplify(sympy.simplify(result))
        return result
    except SyntaxError:
        import IPython; IPython.embed(colors='neutral')

@cache
def sympy_to_z3(expression: sympy.core.Expr):
    if not isinstance(expression, sympy.Basic):
        return expression
    parameters = {p: z3.Real(p) for p in (str(u) for u in expression.free_symbols)}
    return eval(str(expression).replace('\n', ' '), parameters)


def convert_array(func, array):
    flat = array.flatten()
    result = np.zeros_like(flat, dtype=np.object_)
    for i in range(flat.size):
        result[i] = func(flat[i])
    return np.reshape(result, array.shape)


def convert_program(func, program):
    props = [convert_array(func, a) if isinstance(a, np.ndarray) else a for a in program]
    return type(program)(*props)
