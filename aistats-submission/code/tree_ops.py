# A module that defines eunctions on the parameteric simplex tree

import os
import z3
import sympy
import numpy as np
import polars as pl
import multiprocess as mp

from treelib import Tree
from itertools import chain
from functools import partial
from conversion import z3_regions_to_string, z3_to_sympy

from datatypes import StandardForm, Tableau
from scipy.optimize import linprog


def simplify_critical_regions(cr):
    print(cr)

    regions, answers = zip(*cr)
    with mp.Pool(os.cpu_count() - 1) as p:
        simplified = p.map(sympy.to_dnf, (z3_to_sympy(r) for r in regions))
    new_cr = list(zip(simplified, answers))

    for i, a in new_cr:
        print('\n\nANSWER:', a, '\n')

        if not type(i) == sympy.Or:
            simplified = sympy.simplify(i)
            if repr(simplified) not in {'True', 'False'}:
                for s in sorted(str(simplified).split('&')):
                    print(s.strip())
            continue

        for c2 in i.args:
            simplified = sympy.simplify(c2)
            if repr(simplified) not in {'True', 'False'}:
                for s in sorted(str(simplified).split('&')):
                    print(s.strip())
                print('\n')
        print('\n')

    return new_cr

def get_critical_regions(tree: Tree):
    regions = []
    answers = []

    for leaf_path in tree.paths_to_leaves():
        leaf = tree.get_node(leaf_path[-1])
        if leaf.data.status == 'optimal':
            leaf_answer = sympy.simplify(sympy.simplify((leaf.data.program.nu.item())))
        else:
            leaf_answer = leaf.data.status

        # import IPython; IPython.embed(colors='neutral')
        path_constraints = list(chain.from_iterable(
            list(z3.parse_smt2_string(tree.get_node(p).data.op_constraints))
            for p in leaf_path[1:]
        ))

        leaf_constraints = z3.And(*path_constraints)

        already_seen = False
        for i in range(len(answers)):
            same = (
                leaf_answer == answers[i]
                or (
                    not isinstance(leaf_answer, str)
                    and not isinstance(answers[i], str)
                    and sympy.Eq(leaf_answer, answers[i]).simplify().simplify()
                )
            )
            if isinstance(same, sympy.logic.boolalg.BooleanTrue) or (same == True):
                regions[i] = z3.Or(regions[i], leaf_constraints)
                already_seen = True
                break

        if not already_seen:
            answers.append(leaf_answer)
            regions.append(leaf_constraints)

    return list(zip(z3_regions_to_string(regions), answers))

#
#
# def get_critical_regions(tree):
#     regions = []
#     answers = []
#     for leaf in tree.leaves():
#
#         leaf_constraints = z3.And(*list(z3.parse_smt2_string(leaf.data.constraints)))
#
#         if leaf.data.status == 'optimal':
#             leaf_answer = (leaf.data.program.nu.item()).simplify().simplify().simplify()
#         else:
#             leaf_answer = leaf.data.status
#
#         already_seen = False
#         for i in range(len(answers)):
#             same = (
#                 leaf_answer == answers[i]
#                 or (
#                     not isinstance(leaf_answer, str)
#                     and not isinstance(answers[i], str)
#                     and sympy.Eq(leaf_answer, answers[i]).simplify().simplify()
#                 )
#             )
#             if isinstance(same, sympy.logic.boolalg.BooleanTrue) or (same == True):
#                 regions[i] = z3.Or(regions[i], leaf_constraints)
#                 already_seen = True
#                 break
#
#         if not already_seen:
#             answers.append(leaf_answer)
#             regions.append(leaf_constraints)
#
#     return list(zip(z3_regions_to_string(regions), answers))
#

def print_decision_tree(tree):
    # TODO: print the decision corresponding to each node. Use the `op_constraints` property of the
    # node. Each decision should be all and only the part of the constraint that would still be
    # relevant at that node, i.e. constraints that are satisfied by construction due to earlier
    # decisions should not be mentioned
    pass

# TODO: check their result of Example 6 -- ours does not seem to agree on some points
# find specific parameter values for a CR that leads to one of our solutions that they don't
# have an equivalent to, see who is right

def check_against_numeric_program(run_num, params, program, critical_regions, min=True):
    # TODO: use sympy substitutions rather than eval for greater precision
    context = params | {'Tableau': Tableau, 'StandardForm': StandardForm, 'array': np.array}
    program = type(program)(*[x if len(x) > 0 else None for x in program])
    subbed = eval(str(program), context)

    coef = 1 if min else -1

    args = [-subbed.c, subbed.a, subbed.b]
    if isinstance(program, StandardForm) and program.aprime is not None:
        args += [subbed.aprime, subbed.bprime]

    num_result = linprog(*args)

    # import IPython; IPython.embed()
    # stop

    sols = {
        0: coef * num_result.fun if num_result.status == 0 else None,
        #4: -num_result.fun if num_result.status == 4 else None,
        2: 'infeasible',
        3: 'unbounded',
    }

    result = params | {
        # keep track of specific run for comparison between methods
        'run_num': run_num,

        # solution
        'num_solution': sols.get(num_result.status, 'error'),
    }

    for i, (region, answer) in enumerate(critical_regions):
        region_constraints = z3.parse_smt2_string(region)
        params_equality_constraints = [z3.Real(k) == v for k, v in params.items()]

        solver = z3.SolverFor('QF_NRA')
        solver.add(z3.And(*region_constraints, *params_equality_constraints))
        solution = solver.check()

        if str(solution) == 'sat':
            result.update({
                # keep track of critical region to facilitate analysis by region
                'cr': i,

                # add symbolic solution
                'cr_solution': answer if isinstance(answer, str) else str(coef * answer),
            })

            # track difference, for purposes of average
            result['diff'] = None

            # both are optimal
            if (
                not isinstance(result['num_solution'], str)
                and result['cr_solution'] not in {'infeasible', 'unbounded'}
            ):
                result['diff'] = abs(result['num_solution'] + coef * answer.subs(params))

            else:
                if (
                    isinstance(result['num_solution'], str)
                    and result['cr_solution'] not in {'infeasible, unbounded'}
                ):
                    result['diff_sols'] = True
                else:
                    result['different_result_types'] = (
                        result['num_solution'] != result['cr_solution']
                    )

            return result

    return result


def test_sampled_numeric_programs(program, params, critical_regions, min=True):
    mapper = partial(
        check_against_numeric_program,
        program=program,
        critical_regions=critical_regions,
        min=min,
    )

    with mp.Pool(os.cpu_count() - 1) as p:
        return pl.from_dicts(p.starmap(mapper, enumerate(params)), infer_schema_length=100000)

    # from itertools import starmap
    # list(starmap(mapper, enumerate(params)))
