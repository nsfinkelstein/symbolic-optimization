from collections import namedtuple

StandardForm = namedtuple('StandardForm', 'a aprime b bprime c')
Operation = namedtuple('Operation', 'alteration constraints name')
Tableau = namedtuple('Tableau', 'a b c nu basis nonbasis', defaults=[None, None])
Node = namedtuple('Node', 'program status constraints op_constraints id parent', defaults=[''])
AuxStatus = namedtuple('AuxStatus', 'c nu complete original_nonbasis')
