import z3
from datetime import datetime
import sympy
import random
import numpy as np
import polars as pl
import parametric_simplex as ps
import tree_ops as to
from conversion import z3_regions_to_string, z3_to_sympy
from simplify import round_sympy


# NOTE:
# objective vector c
# and Chatopolous answers
# have to be negated

def example1(n=1000):
    # define the parameter space
    p1, p2, p3 = z3.Reals('p1 p2 p3')
    parameter_space = z3.And(*[z3.And(x >= -5, x <= 5) for x in [p1, p2, p3]])
    solver = z3.SolverFor('QF_NRA')
    solver.add(parameter_space)
    solver.check()
    constraints = solver.to_smt2()

    # define the program
    sp1, sp2, sp3 = sympy.symbols(['p1', 'p2', 'p3'])
    aprime = np.array([
        [-1, 1, 1, 0],
        [1, -sp3, 0, 1],
    ], dtype=np.object_)
    bprime = np.array([sp2, 1], np.object_)
    c = -np.array([sp1, 1, 0, 0], np.object_)
    program = ps.StandardForm(np.zeros(((0, 4))), aprime, np.zeros((0)), bprime, c)

    print('completed parametric simplex, testing against numeric program')

    def sample_params():
        return {f'p{i}': (random.random() * 10) - 5 for i in (1, 2, 3)}

    chart_regions = [
        z3.And(p1 >= 0, p2 >= 0),
        z3.Or(
            z3.And(p1 <= -1 / 5, p2 >= -1, p3 <= -1 / p1),
            z3.And(p1 >= -1 / 5, p1 <= 0, p2 >= -1),
        ),
        z3.And(p1 >= 0, p2 >= -1, p2 <= 0),
        z3.Or(
            z3.And(p1 <= -1, p2 >= -1, p3 < 1, p3 >= -1 / p1),
            z3.And(p1 >= -1, p1 <= -1 / 5, p2 <= -1, p3 > 1, p3 <= -1 / p1),
            z3.And(p1 >= -1 / 5, p2 <= -1, p3 > 1),
        )
    ]

    chart_answers = [
        -sympy.Integer(0),
        -sp1,
        sp1 * sp2,
        -((1 + sp1 + sp2 + sp1 * sp2 * sp3) / (1 - sp3)),
    ]

    c_critical_regions = list(zip(z3_regions_to_string(chart_regions), chart_answers))
    print('completed numeric tests')
    return run_tests(program, constraints, sample_params, c_critical_regions, n=n)


# TODO: add at least one or two more examples, maybe just example 5
# and one other (that disagrees with Chart)
def example6(n=1000):
    # NOTE: seems to be actually a maximization!!!!!
    p1, p2, p3 = z3.Real('p1'), z3.Real('p2'), z3.Real('p3')
    parameter_space = z3.And(
        0 <= p1,
        6 >= p1,
        0 <= p2,
        1.5 >= p2,
        -50000 <= p3,
        50000 >= p3,
    )
    solver = z3.SolverFor('QF_NRA')
    solver.add(parameter_space)
    constraints = solver.to_smt2()

    sp1, sp2, sp3 = sympy.symbols(['p1', 'p2', 'p3'])
    a = np.array([
        [1.1, 0.9, 0.9, sp2, 1.1, 0.9, 0],
        [0.5, 0.35, 0.25, 0.25, 0.5, 0.35, 0],
        [0.01, 0.15, 0.15, 0.18, 0.01, 0.15, 0],
    ], dtype=np.object_)
    b = np.array([200000, 100000 + sp3, 20000], dtype=np.object_)

    aprime = np.array([
        [0.4, 0.06, 0.04, 0.05, -0.6, 0.06, 0],
        [0, 0.1, 0.01, 0.01, 0, -0.9, 0],
        [-6857.6, 364, 2032, -1145, -6857.6, 346, 21520]
    ], dtype=np.object_)
    bprime = np.array([0, 0, 20000000], dtype=np.object_)
    c = np.array([2.84, -sp1, -3.33, 1.09, 9.39, 9.51, 0], dtype=np.object_)

    program = ps.StandardForm(a, aprime, b, bprime, c)

    def draw_params():
        # meets conditions of subspace spec
        return {
            'p1': random.random() * 6,
            'p2': random.random() * 1.5,
            'p3': (random.random() * 100000) - 50000,
        }

    chart_regions = [
        z3.And(p2 >= 0.2977, p3 >= -9090),
        p3 <= -9090,
        z3.And(p3 == -9090, p2 <= 0.368 + (0.0201 * p1)),
        z3.And(
            p2 <= 0.2977,
            p3 >= -9090,
            p3 <= (-7.14e7 + (2.03e8 * p2)) / ((-4.49e3 + (225 * p2)))
        )
    ]
    chart_answers = [
        sympy.Integer(993000),  # must be sympy int so sympy.substitute will work
        (109000 + 10.9 * sp3),
        -(
            (
                (sp1 * (0.22 * sp3 + 2000))
                + (sp2 * (-10.92 * sp3 - 1.092e6))
                + 4.0148 * sp3
                + 595404
            )
            / (0.54856 - sp2)
        ),
        -(
            (
                sp2 * (-10.92 * sp3 - 1.092e6)
                + 3.25140 * sp3
                + 574124
            )
            / (sp2 - 0.54856)
        ),
    ]

    c_critical_regions = list(zip(z3_regions_to_string(chart_regions), chart_answers))

    # return to.test_sampled_numeric_programs(program, [draw_params() for _ in range(n)],
    #                                         c_critical_regions, min=False)

    df, tree, cr = run_tests(program, constraints, draw_params, c_critical_regions, n=n, min=False)
    print(
        df.group_by('cr_chart', 'cr')
        .agg(pl.count())
        .sort('cr_chart', 'cr')
    )
    #
    # for i in range(len(cr)):
    #     print('optimum at critical region', i, ':', )
    #     round_sympy(z3_to_sympy(cr[i][1]))
    #
    # TODO: figure out how to nicely sipmlify critical regions and answers
    # point out issues with chart critical regions, based in part on above output

    # NOTE: we do match chart CR region 3. There's one that they are missing
    return df, tree, cr


def run_tests(program, constraints, draw_params, c_critical_regions=None, n=1000, min=True):
    start = datetime.now()
    tree = ps.parametric_simplex(program, constraints)
    print('Parameteric simplex run time:', datetime.now() - start)
    critical_regions = to.get_critical_regions(tree)
    params = [draw_params() for _ in range(n)]
    df = to.test_sampled_numeric_programs(program, params, critical_regions, min)

    if c_critical_regions is not None:
        df2 = to.test_sampled_numeric_programs(program, params, c_critical_regions, min)
        df = df.join(
            df2,
            on=['run_num', 'num_solution'] + list(params[0].keys()),
            how='inner',
            suffix='_chart',
        )

    # critical_regions = [(z3_to_sympy(k).simplify().simplify(), v) for k, v in critical_regions]
    # critical_regions = [(z3_to_sympy(k), v) for k, v in critical_regions]
    return df, tree, critical_regions
