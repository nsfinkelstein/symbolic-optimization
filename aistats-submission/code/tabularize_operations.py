import z3
import sympy
import numpy as np
from functools import partial
from itertools import combinations

from conversion import sympy_to_z3, z3_to_sympy
from datatypes import StandardForm, Operation, Tableau

# simpler op; replaces equality constraints w/ two inequality constraints
def simple_tabularize_ops(program: StandardForm, status: str):
    return [
        Operation(
            alteration=simple_tabularize_alteration,
            constraints=z3.Bool(True),
            name='simple_tabularize',
        )
    ]


def simple_tabularize_alteration(program, status):
    if len(program.aprime) == 0:
        a, b = program.a, program.b
    else:
        new_a = [] if program.a is None else [program.a]
        new_b = [] if program.b is None else [program.b]
        a = np.vstack(tuple(new_a + [program.aprime, -program.aprime]))
        b = np.vstack(tuple(new_b + [program.bprime, -program.bprime])).flatten()

    tableau = Tableau(
        a=a,
        b=b,
        c=program.c,
        nu=np.array(0, np.object_),
        basis={i: i for i in range(a.shape[0])},
        nonbasis={j: j - a.shape[0] for j in range(a.shape[0], a.shape[0] + a.shape[1])}
    )

    return tableau, 'tabularized'


# TODO: figure out the complex version later. Will need to add basis and non basis

# more complex; solves for variables where permitted in the parameter space
def tabularize_ops(program, status):
    # there are no equality constraints
    if len(program.aprime) == 0:
        return [
            Operation(
                alteration=lambda program, status: (
                    Tableau(program.a, program.b, program.c, 0),
                    'tabularized',
                ),
                constraints=z3.Bool(True),
                name='tabularize_no_equality_constraints',
            )
        ]

    index_sets = get_index_sets(len(program.aprime), len(program.aprime[0]))
    # import IPython; IPython.embed()
    constraints = [
        get_constraints(program.aprime, index_sets[:i + 1])
        for i in range(len(index_sets))
    ]
    return [
        Operation(
            alteration=partial(tabularize, R=R, B=B),
            constraints=constraint,
            name=f'tabularize_rows_{set(R)}_cols_{set(B)}',
        )
        for (R, B), constraint
        in zip(index_sets, constraints)
    ]


# TODO: this needs to be fixed
def tabularize(program, status, R, B):
    no_inequalities = program.a is None
    inverse = invert(tuple(tuple(r) for r in program.aprime), R, B)

    rows, cols = program.aprime.shape
    N = list(set(range(cols)) - set(B))

    # solve for x_B
    constant_term = np.matmul(
        inverse,
        program.bprime[list(R)].reshape((-1, 1)),
    ).flatten()

    variable_term = np.matmul(
        inverse,
        program.aprime[np.ix_(R, N)],
    )

    # Substitute result into a and c
    nu = np.array(0, dtype=np.object_)
    new_c = np.copy(program.c[N])
    new_a = np.copy(program.a[:, N])
    new_b = np.copy(program.b)

    # TODO: we have to keep all remaining rows of aprime, and substitute the
    # answers back into those rows

    for eliminated_idx, eliminated_var in enumerate(B):
        nu += program.c[eliminated_var] * constant_term[eliminated_idx]

        for kept_idx, kept_var in enumerate(N):
            new_c[kept_idx] -= (
                program.c[eliminated_var]
                * variable_term[eliminated_idx, kept_idx]
            )

        if no_inequalities:
            continue

        for row in range(program.a.shape[0]):
            new_b[row] -= (
                program.a[row, eliminated_var]
                * constant_term[eliminated_idx]
            )
            for kept_idx, kept_var in enumerate(N):
                new_a[row, kept_idx] -= (
                    program.a[row, eliminated_var]
                    * variable_term[eliminated_idx, kept_idx]
                )

    if no_inequalities:
        a = variable_term
        b = constant_term

    else:
        a = np.vstack([new_a, variable_term])
        b = np.concatenate([new_b, constant_term])

    tableau = Tableau(
        a=a,
        b=b,
        c=new_c,
        nu=nu,
        basis={i: i for i in range(a.shape[0])},
        nonbasis={j + a.shape[0]: j for j in range(a.shape[1])},
    )
    # import IPython; IPython.embed(colors='neutral')
    return (tableau, 'tabularized')


#@cache
def invert(matrix, R, B):
    matrix = get_sympy_matrix(matrix, R, B)
    inverse = matrix.inv()
    return np.array([[x for x in inverse.row(i)] for i in range(inverse.rows)])


#@cache
#@simplify
def get_constraints(ap, index_sets):
    constraints = nonsingular_constraints(ap, *index_sets[-1])

    if len(index_sets) == 1:
        return constraints

    return z3.And(constraints, z3.Not(constraints_disjunction(ap, index_sets[:-1])))


#@cache
#@simplify
def constraints_disjunction(ap, index_sets):
    constraints = nonsingular_constraints(ap, *index_sets[-1])

    if len(index_sets) == 1:
        return constraints

    return z3.Or(constraints, constraints_disjunction(ap, index_sets[:-1]))


#@cache
#@simplify
def nonsingular_constraints(aprime, R, B):
    # import IPython; IPython.embed(colors='neutral')
    matrix = get_sympy_matrix(aprime, R, B)
    # import IPython; IPython.embed()
    determinant = matrix.det()
    z3_determinant = sympy_to_z3(determinant)
    return z3_determinant != 0


#@cache
def get_sympy_matrix(matrix: np.array, R: tuple, B: tuple) -> sympy.Matrix:
    return sympy.Matrix([
        [z3_to_sympy(z3_obj) for j, z3_obj in enumerate(row) if j in B]
        for i, row in enumerate(matrix) if i in R
    ])



#@cache
def get_index_sets(num_rows, num_cols):
    square_size = min(num_rows, num_cols)
    return tuple(
        (R, B)
        for num_rows_subset in range(square_size, 0, -1)
        for R in combinations(range(num_rows), num_rows_subset)
        for B in combinations(range(num_cols), num_rows_subset)
    )
