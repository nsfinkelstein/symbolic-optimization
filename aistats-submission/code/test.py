import z3
import sympy
import numpy as np
from simplify_sympy import simplify_sympy
import parametric_simplex as ps

import IPython
from functools import partial
debug = partial(IPython.embed, colors='neutral')


def test_tabularize():
    aprime = np.array([
        [1, 2, 3, 4],
        [5, 6, 7, 9],
    ])
    bprime = 2
    c = np.array([1, 3, 0, 0])
    expected_c = 3


def test_index_sets():
    result = ps.get_index_sets(2, 3)
    expected = (
        ((0, 1), (0, 1)),
        ((0, 1), (0, 2)),
        ((0, 1), (1, 2)),
        ((0,), (0,)),
        ((0,), (1,)),
        ((0,), (2,)),
        ((1,), (0,)),
        ((1,), (1,)),
        ((1,), (2,)),
    )
    assert result == expected


def test_z3_to_sympy():
    assert ps.convert_z3_value_to_sympy(z3.RealVal('1/3')) == 1 / 3
    assert ps.convert_z3_value_to_sympy(z3.Real('x')) == sympy.Symbol('x')


def test_sympy_matrix():
    matrix = (
        (z3.RealVal(1), z3.Real('x1')),
        (z3.Real('x2'), z3.RealVal(1)),
    )

    result = ps.get_sympy_matrix(matrix, (0, 1), (0, 1))
    expected = sympy.Matrix([
        [1, sympy.Symbol('x1')],
        [sympy.Symbol('x2'), 1],
    ])

    assert result == expected


def test_convert_sympy_to_z3():
    expression = 1 + sympy.Symbol('x') / (sympy.Symbol('y')**2 + 3)
    result = ps.convert_sympy_to_z3(expression)
    expected = 1 + z3.Real('x') / (3 + z3.Real('y')**2)
    assert result == z3.simplify(expected)


def test_nonsingular_constraints():
    matrix = (
        (z3.RealVal(1), z3.Real('x1')),
        (z3.Real('x2'), z3.RealVal(1)),
    )
    result = ps.nonsingular_constraints(matrix, (0, 1), (0, 1))
    expected = 1 + -z3.Real('x1') * z3.Real('x2') != 0
    assert result == z3.simplify(expected)


def test_constraints():
    x1, x2, x3 = z3.Reals('x1 x2 x3')
    matrix = (
        (1, x1, 2),
        (x2, 1, 2),
        (x3, 1, 2),
    )
    index_sets = ps.get_index_sets(3, 3)
    result = ps.constraints_disjunction(matrix, index_sets[:3])

    expected = z3.simplify(
        z3.Or(
            ps.nonsingular_constraints(matrix, (0, 1, 2), (0, 1, 2)),
            ps.nonsingular_constraints(matrix, (0, 1), (0, 1)),  # x1 = x2 = 1
            ps.nonsingular_constraints(matrix, (0, 1), (0, 2)),  # x1 = x3 = 1
        )
    )
    assert result == expected

    # includes 1x1 matrices, i.e. scalars, which are always invertible
    assert ps.constraints_disjunction(matrix, index_sets)


def test_tabularize():
    a = np.array([[z3.Real('x'), 1, 1, 1], [1, 1, 1, 1]])
    aprime = np.array([[1, 0, 1, 1], [0, 1, z3.Real('w'), 1]])
    b = np.array([z3.Real('y'), 1]).reshape(-1, 1)
    bprime = np.array([z3.Real('z'), 1]).reshape(-1, 1)
    c = np.array([z3.Real('v'), 1, 1, 1])
    program = ps.StandardForm(a, aprime, b, bprime, c)

    result = ps.tabularize(program, (0, 1), (0, 1))

    assert [z3.simplify(x) for x in result.a[2]] == [z3.RealVal(1), z3.RealVal(1)]
    assert z3.simplify(result.b[2, 0]) == z3.Real('z')
    assert [z3.simplify(x) for x in result.a[3]] == [z3.Real('w'), z3.RealVal(1)]
    assert z3.simplify(result.b[3, 0]) == z3.RealVal(1)

    result2 = ps.tabularize(program, (1,), (1,))
    assert [z3.simplify(x) for x in result2.a[2]] == [z3.RealVal(0), z3.Real('w'), z3.RealVal(1)]
    assert z3.simplify(result2.b[2, 0]) == z3.RealVal(1)


def test_simplify_sympy():
    s = """
(p0 <= 6.0)
 & (p2 <= 50000.0)
 & (p2 > -9090.90909)
 & (p1 < 0.563)
 & (p0 >= 0)
 & (p1 >= 0)
 & (999999999999999.0*p0 - 4.96363636363633e+16*p1 > -1.82492727272727e+16)
 & ((-1.0e+15*p2 - 9.09090909090927e+18) /(454545454545453.0*p1 - 255909090909091.0) <= 0.0)
 & (
     (
         (100000381469726.0*p0 - 4.96365529840639e+15*p1 + 1.82493423427235e+15)
        /(454545454545453.0*p1 - 255909090909091.0)
    )
     <= 0
 )
 & (
 (-4.09090909090908e+15*p0*p1 + 2.24409090909091e+15*p0 - 1.12636363636363e+16*p1 + 6.91437272727269e+15)
    /(454545454545453.0*p1 - 255909090909091.0) <= 0
)
 & ((45454718849875.8*p0*p1 - 40909246964888.1*p0 - 2.47960036797957e+15*p1 + 1.54454066465984e+15)
/(454545454545453.0*p1 - 255909090909091.0)
 <= 0)
 & ((p1 >= 18.24)
 | (2.27272727272728e+20/(113636363636371.0*p1 - 2.07272727272728e+15)
 <= (1.0e+15*p2 + 9.09090909090927e+18)
/(454545454545453.0*p1 - 255909090909091.0)
)
)
 & ((p1 <= 0.00933)
 | (1.09090909090909e+22/(5.45454545454543e+16*p1 - 509090909090903.0)
 >= (-1.0e+15*p2 - 9.09090909090927e+18)
/(454545454545453.0*p1 - 255909090909091.0)
)
)
 & ((p1 <= 0.211)
 | (7.27272727272726e+20/(3.63636363636362e+15*p1 - 767272727272727.0)
 >= (-1.0e+15*p2 - 9.09090909090927e+18)
/(454545454545453.0*p1 - 255909090909091.0)
)
)
 & ((p1 <= 0.2795)
 | (1.96226202545905e+19/(96564154556719.0*p1 - 26990030415681.0)
 >= (-1.0e+15*p2 - 9.09090909090927e+18)
/(454545454545453.0*p1 - 255909090909091.0)
)
)
    """
    expr = sympy.sympify(s)
    simplify_sympy(expr)


